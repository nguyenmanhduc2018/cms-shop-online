<script>
    RV_MEDIA_URL = {!! json_encode(RvMedia::getUrls()) !!};
    RV_MEDIA_CONFIG = {!! json_encode([
        'mode' => config('media.mode'),
        'permissions' => RvMedia::getPermissions(),
        'translations' => trans('media::media.javascript'),
    ]) !!}
</script>