<div class="table-actions">

    <a class="btn btn-icon btn-sm btn-info restoreDialog tip" data-toggle="modal" data-section="{{ route($restore, $item->id) }}" role="button" data-original-title="{{ trans('bases::tables.restore') }}" >
        <i class="fa fa-undo"></i>
    </a>

    <a class="btn btn-icon btn-sm btn-danger deletePermanentlyDialog tip" data-toggle="modal" data-section="{{ route($delete_permanently, $item->id) }}" role="button" data-original-title="{{ trans('bases::tables.delete_permanently') }}" >
        <i class="fa fa-trash-o"></i>
    </a>
</div>