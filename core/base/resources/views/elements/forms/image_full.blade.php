<div class="image-box">
    <input type="hidden"
           name="{{ $name }}"
           value="{{ $value }}"
           class="image-data">
    <img src="{{ get_object_image($value) }}"
        alt="preview image" class="preview_image_full img-responsive">
    <div class="image-box-actions">
        <a class="btn_gallery" data-result="{{ $name }}" data-action="{{ $attributes['action'] or 'select-image-full' }}">
            {{ trans('bases::forms.choose_image') }}
        </a> |
        <a class="btn_remove_image">
            {{ trans('bases::forms.remove_image') }}
        </a>
    </div>
</div>