/**
 *
 * Fix bug Trash by Duc Nguyen
 *
 */

$(document).ready(function() {

    var dataLink;

    $('a.action-item span span').click(function() {
        var _this = $(this);
        var action = _this.attr('data-action');
        var href = _this.attr('data-href');
        if(action == 'trash' || action == 'lists'){
            window.location.href=''+href+'';
        }
    });
    
    $(document).on('click', 'a.restoreDialog', function() {
        dataLink = $(this).attr('data-section');
        $('#restore-crud-modal').modal('show');
    });
    $(document).on('click', 'a.deletePermanentlyDialog', function() {
        dataLink = $(this).attr('data-section');
        $('#delete_permanently-crud-modal').modal('show');
    });

    $(document).on('click','#restore-crud-entry',function() {
        $('#restore-crud-modal').modal('hide');
        $.get(dataLink, function(data){
            if(data.error == false){
                Botble.showNotice("success", data.message, Botble.languages.notices_msg.success);
                var a = $(document).data("section");
                window.LaravelDataTables.dataTableBuilder.row($('a[data-section="' + a + '"]').closest("tr")).remove().draw();
            }else{
                Botble.showNotice("error", data.message, Botble.languages.notices_msg.error);
            }
        });
    });

    $(document).on('click', '#delete_permanently-crud-entry', function() {
        $('#delete_permanently-crud-modal').modal('hide');
        $.get(dataLink, function(data){
            if(data.error == false){
                Botble.showNotice("success", data.message, Botble.languages.notices_msg.success);
                var a = $(document).data("section");
                window.LaravelDataTables.dataTableBuilder.row($('a[data-section="' + a + '"]').closest("tr")).remove().draw();
            }else{
                Botble.showNotice("error", data.message, Botble.languages.notices_msg.error);
            }
        });
    });

    $(document).on('click', '.row-actions .delete a', function() {
        var link = $(this).attr('data-href');
        $.get(link, function(data){
            if(data.error == false){
                Botble.showNotice("success", data.message, Botble.languages.notices_msg.success);
                window.location.href = data.link;
            }else{
                Botble.showNotice("error", data.message, Botble.languages.notices_msg.error);
            }
        });
    });  
});