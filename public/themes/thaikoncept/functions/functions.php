<?php

register_page_template([
    'default' => 'Default',
    'home' => 'Home Page',
    'contact-us' => 'Contact US',
    'about' => 'About Page',
    'project' => 'Project Page',
    'gallery' => 'Gallery Page',
    'product' => 'Product Page',
    'decorate' => 'Decorate Page'
]);

// register_sidebar([
//     'id' => 'second_sidebar',
//     'name' => 'Second sidebar',
//     'description' => 'This is a sample widget for thaikoncept theme',
// ]);


theme_option()->setSection([
    'title' => __('General'),
    'desc' => __('General settings'),
    'id' => 'opt-text-subsection-general',
    'subsection' => true,
    'icon' => 'fa fa-home',
]);

theme_option()->setSection([
    'title' => __('Logo'),
    'desc' => __('Change logo'),
    'id' => 'opt-text-subsection-logo',
    'subsection' => true,
    'icon' => 'fa fa-image',
    'fields' => [
        [
            'id' => 'logo',
            'type' => 'mediaImage',
            'label' => __('Logo'),
            'attributes' => [
                'name' => 'logo',
                'value' => null,
            ],
        ],
    ],
]);

theme_option()->setField([
    'id' => 'copyright',
    'section_id' => 'opt-text-subsection-general',
    'type' => 'text',
    'label' => __('Copyright'),
    'attributes' => [
        'name' => 'copyright',
        'value' => '© 2017 ThaiKoncept. All right reserved. Designed by Duc Nguyen',
        'options' => [
            'class' => 'form-control',
            'placeholder' => __('Change copyright'),
            'data-counter' => 120,
        ]
    ],
    'helper' => __('Copyright on footer of site'),
]);

theme_option()->setArgs(['debug' => true]);