<?php

/*
 * Home Sidebar
 */

register_sidebar([
    'id' => 'home_sidebar',
    'name' => __('Home Header Slider'),
    'description' => __('This is custom for Hompage'),
]);

register_sidebar([
    'id' => 'home_project_slide',
    'name' => __('Home Project Slider'),
    'description' => __('This is custom for Hompage'),
]);

register_sidebar([
    'id' => 'list_post_project',
    'name' => __('List Post Project'),
    'description' => __('This is custom for Project'),
]);

register_sidebar([
    'id' => 'slide_project',
    'name' => __('Slide Project'),
    'description' => __('This is custom for Project'),
]);

register_sidebar([
    'id' => 'slide_gallery',
    'name' => __('Slide Gallery'),
    'description' => __('This is custom for Gallery'),
]);


/*
 * Widget Product
 */
require_once __DIR__ . '/../widgets/slide_home/slide_home.php';
register_widget(SlideHomeWidget::class);

/*
 * Widget Project
 */
require_once __DIR__ . '/../widgets/project_home/project_home.php';
register_widget(ProjectHomeWidget::class);

/*
 * Widget Recent Post
 */
require_once __DIR__ . '/../widgets/recent_post/recent_post.php';
register_widget(RecentPostWidget::class);

/*
 * Widget Slide Project
 */
require_once __DIR__ . '/../widgets/slide_project/slide_project.php';
register_widget(SlideProjectWidget::class);

/*
 * Widget Slide Project
 */
require_once __DIR__ . '/../widgets/slide_gallery/slide_gallery.php';
register_widget(SlideGalleryWidget::class);



?>