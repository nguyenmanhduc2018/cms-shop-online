{!! Theme::partial('header') !!}
<!-- Your page here -->
<main>
{!! Theme::content() !!}
</main>
<!-- end main -->
{!! Theme::partial('footer') !!}