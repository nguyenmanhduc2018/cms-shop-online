<section class="container" >
		<div class="pageItem" id="pProduct">
			<div class="pageItem-inner">
				@php
					$infoPage = gallery_meta_data(21, 'page');
				@endphp
				<div class="boxNormalContent pIntro">
					<div class="content to-animate" data-direct="fadeInUp">
						{{ __('product.section_text') }}
					</div>
					<div class="imageHolder to-animate" data-direct="fadeInUp">
						<img class="lazyload" data-src="{{ get_object_image($infoPage[0]['img']) }}" src="{{ get_object_image($infoPage[0]['img'], 'small') }}" title="{{ $infoPage[0]['description'] }}" alt="{{ $infoPage[0]['description'] }}">
					</div>
				</div><!-- boxNormalContent -->

				<div class="boxNormalContent pCare">
					<div class="content to-animate" data-direct="fadeInUp">
						<div class="blockContent service noPhoto">
							<div class="boxContent noViewMore">
								<div class="core">
									<div class="title">
										<h1>{{ __('product.take_care') }}<span class="line-h"></span></h1>
										<h1>{{ __('product.product') }}</h1>
									</div>
									<div class="content">
										<div class="clo cl1 to-animate" data-direct="fadeInUp">
											<p class="con-title">{{ __('product.section_title_one') }}</p>
											<p class="con-des">{{ __('product.section_text_one') }}</p>
										</div>
										<div class="clo cl2 to-animate" data-direct="fadeInUp">
											<p class="con-title">{{ __('product.section_title_two') }}</p>
											<p class="con-des">{{ __('product.section_text_two') }}</p>
										</div>
										<div class="clo cl3 to-animate" data-direct="fadeInUp">
											<p class="con-title">{{ __('product.section_title_three') }}</p>
											<p class="con-des">{{ __('product.section_text_three') }}</p>
										</div>
									</div>
								</div>
							</div><!-- boxContent -->
						</div><!-- blockContent -->
					</div>
					<div class="imageHolder to-animate" data-direct="fadeInUp">
						<img class="lazyload" data-src="{{ get_object_image($infoPage[1]['img']) }}" src="{{ get_object_image($infoPage[1]['img'], 'small') }}" title="{{ $infoPage[1]['description'] }}" alt="{{ $infoPage[1]['description'] }}">
					</div>
				</div><!-- boxNormalContent -->

			</div><!-- pageItem-inner -->
		</div><!-- about-wapper -->

		

	</section> <!-- container -->
