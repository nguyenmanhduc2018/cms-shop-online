<section class="container" >
	@php
		$infoPage = get_page_by_slug('about');
		$infoFooter = gallery_meta_data(18, 'page');
	@endphp
	<div class="pageItem" id="pAbout">
		<div class="pageItem-inner">
			<div id="bannerHolder">
				<img src="{{ get_object_image($infoPage->image, 'small') }}" alt="" title="" class="to-animate lazyload" data-src="{{ get_object_image($infoPage->image) }}" data-direct="fadeInUp">
			</div><!-- slideShow -->
			<div class="block">
				<div class="blockContent intro noPhoto to-animate" data-direct="fadeInUp">
					<div class="boxContent noViewMore">
						<div class="core">
							<div class="title underline">
								<h1>{{ __('about.about') }}<span class="line-h"></span></h1>
							</div>
							<div class="content to-animate" data-direct="fadeInUp">
								{!! nl2br(__('about.about_desc')) !!}
							</div>
						</div>
					</div><!-- boxContent -->
				</div><!-- blockContent -->
			</div><!-- block -->

			<div class="block">
				<div class="blockContent product type2 type4 to-animate" data-direct="fadeInUp">
					<div class="imgHolder to-animate" data-direct="fadeInUp">
						<img class="lazyload" data-src="{{ get_object_image($infoFooter[0]['img']) }}" src="{{ get_object_image($infoFooter[0]['img'], 'small') }}" title="{{ $infoFooter[0]['description'] }}" alt="{{ $infoFooter[0]['description'] }}"></img>
					</div>
					
					<div class="boxContent noViewMore">
						<div class="core">
							<div class="title underline">
								<h1>{{ __('about.title_content') }}<span class="line-h"></span></h1>
							</div>
							<p class="content to-animate" data-direct="fadeInUp">
								{!! nl2br(__('about.desc_content')) !!}
							</p>
						</div>
					</div><!-- boxContent -->

					
				</div><!-- blockContent -->
			</div><!-- block -->

			<div class="block">
				<div class="blockContent service noPhoto to-animate" data-direct="fadeInUp">
					<div class="boxContent noViewMore">
						<div class="core">
							<div class="title">
								<h1>{{ __('about.service') }} &<span class="line-h"></span></h1>
								<h1>{{ __('about.customer_segment') }}</h1>
							</div>

							<div class="content">
								<div class="clo cl1 to-animate" data-direct="fadeInUp">
									<p class="con-title">{!! nl2br(__('about.section_title_one')) !!}</p>
									<p class="con-des">{{ __('about.section_text_one') }}</p>
								</div>
								<div class="clo cl2 to-animate" data-direct="fadeInUp">
									<p class="con-title">{!! nl2br(__('about.section_title_two')) !!}</p>
									<p class="con-des">{{ __('about.section_text_two') }}</p>
								</div>
								<div class="clo cl3 to-animate" data-direct="fadeInUp">
									<p class="con-title">{!! nl2br(__('about.section_title_three')) !!}</p>
									<p class="con-des">{{ __('about.section_text_three') }}</p>
								</div>
							</div>
						</div>
					</div><!-- boxContent -->
				</div><!-- blockContent -->
			</div><!-- block -->

			<div class="blockContent contact block">
				<div class="boxContent alignCenter">
					<div class="core to-animate" data-direct="fadeInUp">
						@php
							$localeCode = Language::getCurrentLocale();
						@endphp
						@if($localeCode == 'en')
							<style type="text/css">
								.boxContent .title.callUs::before{
									left: var(--left);
								}
							</style>
						@endif
						<div class="title noUnderline type2 type3 callUs" style="--left: -150px;">
							<h1>{{ __('about.call_us') }}<span class="line-h"></span></h1>
						</div>
						<p class="content size34 fullWidth to-animate" data-direct="fadeInUp">
							{{ $infoFooter[1]['description'] }}
						</p>
					</div>
					<a href="{{ route('public.blog.contact') }}" class="button circleEff viewmore to-animate" data-direct="fadeInUp">
						<p>{{ __('about.contact') }}</p>
						<span></span>
					</a>
				</div><!-- boxContent -->

				<div class="imgHolder to-animate" data-direct="fadeInUp">
					<img class="lazyload" data-src="{{ get_object_image($infoFooter[1]['img']) }}" src="{{ get_object_image($infoFooter[1]['img'], 'small') }}" title="{{ $infoFooter[1]['description'] }}" alt="{{ $infoFooter[1]['description'] }}"></img>
				</div>
			</div><!-- blockContent -->

			<div class="boxNormalContent block">
				<p class="to-animate" data-direct="fadeInUp">{{ __('about.footer_top_text_one') }}<p>
				<p class="to-animate" data-direct="fadeInUp">{{ __('about.footer_top_text_two') }}</p>
			</div>

		</div><!-- pageItem-inner -->
	</div><!-- about-wapper -->

	

</section> <!-- container -->
