<section class="container" >
		<div class="pageItem" id="pGallery">
			<div class="pageItem-inner">
				@php
					$infoPage = gallery_meta_data(20, 'page');
				@endphp
				<div class="clo cl1">
					<div class="boxNormalContent">
						<div class="content to-animate" data-direct="fadeInUp"><p class="">{{ __('gallery.section_text_one') }}</p></div>
						<div class="imageHolder to-animate" data-direct="fadeInUp">
							<img class="lazyload" data-src="{{ get_object_image($infoPage[0]['img']) }}" src="{{ get_object_image($infoPage[0]['img'], 'small') }}" title="{{ $infoPage[0]['description'] }}" alt="{{ $infoPage[0]['description'] }}">
						</div>
					</div><!-- boxNormalContent -->
				</div>
				
				<div class="clo cl2">
					<div class="boxNormalContent swapIndex">
						<div class="imageHolder to-animate" data-direct="fadeInUp">
							<img class="lazyload" data-src="{{ get_object_image($infoPage[1]['img']) }}" src="{{ get_object_image($infoPage[1]['img'], 'small') }}" title="{{ $infoPage[1]['description'] }}" alt="{{ $infoPage[1]['description'] }}">
						</div>
						<div class="content to-animate" data-direct="fadeInUp"><p class="align-middle">{{ __('gallery.section_text_two') }}</p></div>
					</div><!-- boxNormalContent -->
				</div>

				<div id="slideShow" auto-slide>
					<div class="slideshow-inner loading to-animate" data-direct="fadeInUp">
						{!! dynamic_sidebar('slide_gallery') !!}
					</div>
					<div class="controller">
						<div class="controlItem next to-animate" data-direct="fadeInLeft"></div>
						<div class="controlItem pre to-animate" data-direct="fadeInRight"></div>
					</div>
				</div><!-- slideShow -->

			</div><!-- pageItem-inner -->
		</div><!-- gallery-wapper -->

		

	</section> <!-- container -->
