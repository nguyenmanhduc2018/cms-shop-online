<section class="container" >
		<div class="pageItem pDecor" id="pGallery">
			<div class="pageItem-inner">
				@php
					$infoPage = gallery_meta_data(22, 'page');
				@endphp
				<div class="row1">
					<div class="clo cl1">
						<div class="boxNormalContent">
							<div class="content type2 to-animate" data-direct="fadeInUp"><p class="">{{ __('gallery.section_text_deco_one') }}</p></div>
							<div class="imageHolder to-animate" data-direct="fadeInUp">
								<img class="r1 to-animate lazyload" data-direct="fadeInUp" data-src="{{ get_object_image($infoPage[0]['img']) }}" src="{{ get_object_image($infoPage[0]['img'], 'small') }}" title="{{ $infoPage[0]['description'] }}" alt="{{ $infoPage[0]['description'] }}">
							</div>
						</div><!-- boxNormalContent -->
					</div>
					
					<div class="clo cl2 hide-mobile">
						<div class="boxNormalContent">
							<div class="imageHolder">
								<img class="r1 to-animate lazyload" data-direct="fadeInUp" data-src="{{ get_object_image($infoPage[1]['img']) }}" src="{{ get_object_image($infoPage[1]['img'], 'small') }}" title="{{ $infoPage[1]['description'] }}" alt="{{ $infoPage[1]['description'] }}">
								<img class="r2 to-animate lazyload" data-direct="fadeInUp" data-src="{{ get_object_image($infoPage[2]['img']) }}" src="{{ get_object_image($infoPage[2]['img'], 'small') }}" title="{{ $infoPage[2]['description'] }}" alt="{{ $infoPage[2]['description'] }}">
							</div>
						</div><!-- boxNormalContent -->
					</div>
				</div>

				<div class="row2">
					<div class="boxNormalContent to-animate" data-direct="fadeInUp">
						{{ __('gallery.section_text_deco_two') }}
					</div>
				</div>

				<div id="slideShow" auto-slide>
					<div class="slideshow-inner loading to-animate" data-direct="fadeInUp">
						{!! dynamic_sidebar('slide_gallery') !!}
					</div>
					<div class="controller">
						<div class="controlItem next to-animate" data-direct="fadeInLeft"></div>
						<div class="controlItem pre to-animate" data-direct="fadeInRight"></div>
					</div>
				</div><!-- slideShow -->

			</div><!-- pageItem-inner -->
		</div><!-- gallery-wapper -->
</section> <!-- container -->
