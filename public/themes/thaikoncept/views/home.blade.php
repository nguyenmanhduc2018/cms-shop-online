<section class="container" >
	<div class="pageItem" id="pHome">
		<div class="pageItem-inner">
			{!! dynamic_sidebar('home_sidebar') !!}
			<!-- slideShow -->
			<div class="content-wapper">
				{{-- {!! get_page_by_slug('index')->content !!} --}}
				{!! nl2br(__('homepage.text_intro')) !!}
			</div>
			@php
				$infoPage = gallery_meta_data(24, 'page');
				$DescPage  = get_galleries_by_id(8);
			@endphp
			<div class="blockContent secret block">
					<div class="imgHolder to-animate" data-direct="fadeInUp">
						<img class="lazyload" data-src="{{ get_object_image($infoPage[0]['img']) }}" src="{{ get_object_image($infoPage[0]['img'], 'small') }}" title="{{ $infoPage[0]['description'] }}" alt="{{ $infoPage[0]['description'] }}">
					</div>
					<div class="boxContent hasButton">
						<div class="core to-animate" data-direct="fadeInUp">
							<div class="title">
								<h1>{{ __('homepage.section_title_one') }} <span class="line-h"></span></h1>
								<h1>{{ __('homepage.section_title_one_b') }}</h1>
							</div>
							<p class="content to-animate" data-direct="fadeInUp">{{ __('homepage.section_desc_one') }}</p>
						</div>

						<a href="{{ __('homepage.section_url_one') }}" class="button circleEff viewmore to-animate" data-direct="fadeInUp">
							<p>{{ __('allpage.see_more') }}</p>
							<span></span>
						</a>
					</div><!-- boxContent -->

					
				</div><!-- blockContent -->

				<div class="blockContent decor type2 block">
					<div class="imgHolder to-animate" data-direct="fadeInUp">
						<img class="lazyload" data-src="{{ get_object_image($infoPage[1]['img']) }}" src="{{ get_object_image($infoPage[1]['img'], 'small') }}" title="{{ $infoPage[1]['description'] }}" alt="{{ $infoPage[1]['description'] }}">
					</div>

					<div class="boxContent hasButton">
						<div class="core to-animate" data-direct="fadeInUp">
							<div class="title underline">
								<h1>{{ __('homepage.section_title_two') }} <span class="line-h"></span></h1>
							</div>
							<p class="content to-animate" data-direct="fadeInUp">{{ __('homepage.section_desc_two') }}</p>
						</div>

						<a href="{{ __('homepage.section_url_two') }}" class="button circleEff viewmore to-animate" data-direct="fadeInUp">
							<p>{{ __('allpage.see_more') }}</p>
							<span></span>
						</a>
					</div><!-- boxContent -->

					
				</div><!-- blockContent -->

			<div class="block">
				<div class="blockContent recentProject type3 to-animate" data-direct="fadeInUp">
					<div class="boxContent">
						<div class="core">
							<div class="title">
								<h1>{{ __('allpage.recent_projects') }} <span class="line-h"></span></h1>
							</div>

							<div id="smallGalelry" auto-slide>
								<div class="col cl1 control to-animate" data-direct="fadeInUp">
									<p class="projectName">{{ $DescPage->description }}</p>
									<ul>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</div>
								{!! dynamic_sidebar('home_project_slide') !!}
							</div>
						</div>
					</div><!-- boxContent -->
				</div><!-- blockContent -->
			</div> <!-- block -->

		</div><!-- pageItem-inner -->
	</div><!-- home-wapper -->
</section> <!-- container -->