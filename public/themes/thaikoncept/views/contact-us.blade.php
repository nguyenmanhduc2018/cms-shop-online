<section class="container" >
	@php
		$infoPage = get_page_by_slug('contact');
	@endphp
		<div class="pageItem" id="pContact">
			<div class="pageItem-inner">
				<div class="boxNormalContent">
					<div class="to-animate" data-direct="fadeInUp">
						{!! nl2br(__('contact::contact.contact_page_des')) !!}
					</div>
				</div>

				<div id="bannerHolder">
					<img data-src="{{ get_object_image($infoPage->image) }}" src="{{ get_object_image($infoPage->image, 'small') }}" class="to-animate lazyload" data-direct="fadeInUp">
				</div><!-- slideShow -->

				<div id="contactForm">
					@if (session()->has('error_msg_phone'))
				        @php
				        	$fail_phone = 'fail';
				        	$mess_phone = trans('contact::contact.email.phone');
				        @endphp
				    @endif
				    @if (session()->has('error_msg_name'))
				        @php
				        	$fail_name = 'fail';
				        	$mess_name = trans('contact::contact.email.name');
				        @endphp
				    @endif
					{!! Form::open(['route' => 'public.send.contact', 'method' => 'POST']) !!}

						<div class="name tf to-animate {{ @$fail_name }}" data-direct="fadeInUp" message="{{ @$mess_name }}">
							<input type="text" placeholder="{{ __('allpage.full_name') }}" name="name" value="{{ old('name') }}">
						</div> 
						<div class="phone tf to-animate {{ @$fail_phone }}" data-direct="fadeInUp" message="{{ @$mess_phone }}">
							<input type="text" placeholder="{{ __('allpage.phone') }}" name="phone" value="{{ old('phone') }}"  onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
						</div>
						<div class="email tf to-animate" data-direct="fadeInUp">
							<input type="email" placeholder="Email" name="email" value="{{ old('email') }}">
						</div>
						<div class="message tf to-animate" data-direct="fadeInUp">
							<textarea rows="5" name="content" placeholder="{{ __('allpage.content') }}" minlength="20">{{ old('content') }}</textarea>
						</div>
						<button type="button" style="outline: none;border: none; cursor: pointer;" class="button circleEff submitBtn to-animate" data-direct="fadeInUp">
							<p>{{ __('allpage.send') }}</p>
							<span></span>
						</button>
					</form>
					@if(session()->has('success_msg') || session()->has('error_msg') || isset($errors))
					    @if (session()->has('success_msg'))
					        <div class="alert alert-success to-animate" data-direct="fadeInUp">
					            <p>{{ trans('contact::contact.email.success') }}</p>
					        </div>
					    @endif
					    @if (session()->has('error_msg'))
					        <div class="alert alert-danger to-animate" data-direct="fadeInUp">
					            <p>{{ trans('contact::contact.email.failed') }}</p>
					        </div>
					    @endif
					    @if (isset($errors) && count($errors))
					        <div class="alert alert-danger to-animate" data-direct="fadeInUp">
					            @foreach ($errors->all() as $error)
					                <p>{{ $error }}</p>
					            @endforeach
					        </div>
					    @endif
					@endif
				</div><!-- contactForm -->


				<div class="animateMap loading">
					<div id="googleMap" class=" to-animate" data-direct="fadeInUp">
						
					</div><!-- mapHolder -->
				</div>


			</div><!-- pageItem-inner -->
		</div><!-- about-wapper -->
	</section> <!-- container -->