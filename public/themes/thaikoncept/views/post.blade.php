<section class="container">
    <div class="pageItem pDetail" id="pProject">
        <div class="pageItem-inner">

            <div id="detailBox">
                <div class="animtionIn imgHolder">
                    <img class="to-animate lazyload" data-direct="fadeInUp" alt="" title="{{ $post->name }}" data-src="{{ get_object_image($post->image) }}" src="{{ get_object_image($post->image, 'small') }}">
                </div>
                <div class="info animtionIn">
                    <h2 class="title to-animate" data-direct="fadeInUp">{{ $post->name }}</h2>
                    <p class="date to-animate" data-direct="fadeInUp">{{ date_from_database($post->created_at, 'd/m/Y') }}</p>
                    <div class="line_h to-animate" data-direct="fadeInUp"></div>
                </div>
                <div class="content animtionIn">
                    {!! $post->content !!}
                </div>
            </div>

            <div class="animtionIn">
                <div id="anotherProject" class="to-animate" data-direct="fadeInUp">
                    <h3 class="title to-animate" data-direct="fadeInUp">{{ __('allpage.other_projects') }}:</h3>
                    <div class="projectHolder">
                        @php 
                            try {
                                $related = get_related_posts_by_category($post->slug, 'projects', 3);
                            } catch (Exception $e) {
                                $related = get_related_posts_by_category($post->slug, 'project', 3);
                            }
                        @endphp
                        @if(count($related) === 0)
                            <div style="font-size: initial; text-align: center; margin-top: 20px" data-direct="fadeInUp">
                                <span>{{ __('allpage.no_related_projects') }}</span>
                            </div>
                        @endif
                        @foreach($related as $related_item)
                        <a href="{{ route('public.single.detail', $post->slug) }}" class="boxProject to-animate" data-direct="fadeInUp">
                            <div class="core">
                                <div class="thumb" style="background-image: url({{ get_object_image($related_item->image, 'thumb') }});"></div>
                                <div class="info">
                                    <h6>{{ $related_item->name }}</h6>
                                    <p class="des">{{ $related_item->description }}</p>
                                </div>
                            </div>
                        </a><!-- boxProject -->
                        @endforeach
                    </div> <!-- projectHolder -->
                </div><!-- anotherProject -->
            </div><!-- animtionIn -->
            
        </div><!-- pageItem-inner -->
    </div><!-- home-wapper -->
</section> <!-- container -->