<section class="container" >
	<div class="pageItem" id="pProject">
		<div class="pageItem-inner">
			<div id="slideShow" auto-slide>
				<div class="slideshow-inner loading to-animate" data-direct="fadeInUp">
					{!! dynamic_sidebar('slide_project') !!}
				</div>
				<div class="controller">
					<div class="controlItem next to-animate" data-direct="fadeInLeft"></div>
					<div class="controlItem pre to-animate" data-direct="fadeInRight"></div>
				</div>
			</div><!-- slideShow -->

			<!-- projectHolder -->
				{!! dynamic_sidebar('list_post_project') !!}
			<!-- projectHolder -->
		</div><!-- pageItem-inner -->
	</div><!-- home-wapper -->

	

</section> <!-- container -->
