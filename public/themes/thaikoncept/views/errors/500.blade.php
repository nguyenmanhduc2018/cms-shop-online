<section class="container" >
	<div class="pageItem" id="p404">
		<div class="pageItem-inner animationIn">

			<p class="title to-animate" data-direct="fadeInUp">500</p>
			<p class="des to-animate" data-direct="fadeInUp">{{ __('allpage.404_text') }}:</p>
			<a href="{{ url('/') }}" class="button circleEff viewmore to-animate" data-direct="fadeInUp">
				<p>{{ __('allpage.404_home') }}</p>
				<span></span>
			</a>

		</div><!-- pageItem-inner -->
	</div><!-- 500-wapper -->
</section> <!-- container -->
