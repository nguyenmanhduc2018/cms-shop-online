<section class="container" >
	<div class="pageItem" id="pSearch">
		<div class="pageItem-inner">

			<div class="animtionIn">
				<div class="searchHolder to-animate" data-direct="fadeInUp">
					<input id="searchTxt"  type="text" name="" placeholder="{{ __('allpage.enter_keywords') }}">
				</div>
			</div>

			<div class="animtionIn">
				<div class="keywordSearched to-animate" data-direct="fadeInUp">{{ __('allpage.you_are_looking') }} ”{{ Request::get('q') != '' || !empty(Request::get('q')) ? Request::get('q') : 'N/A' }}”</div>
			</div>
			@if(count($posts) === 0)
			<div class="animtionIn">
				<div class="keywordSearched to-animate" data-direct="fadeInUp">- {{ __('allpage.no_results_found') }}</div>
			</div>
			@else
			<div class="projectHolder">
				@foreach($posts as $post)
				<a href="{{ route('public.single.detail', $post->slug) }}" class="boxProject to-animate" data-direct="fadeInUp">
					<div class="core">
						<div class="thumb" style="background-image: url({{ get_object_image($post->image, 'thumb') }});"></div>
						<div class="info">
							<h6>{{ $post->name }}</h6>
							<p class="day">{{ date_from_database($post->created_at, 'd/m/Y') }}</p>
							<p class="des">{{ $post->description }}... <span class="readmore">{{ __('allpage.read_more') }}</span></p>
						</div>
					</div>
				</a><!-- boxProject -->
				@endforeach
			</div>

			@php
				// code by Duc Nguyen
				$link_limit = 4;
				$paginator = $posts;
				$totalLastPage = $paginator->lastPage();
				$currentPage   = $paginator->currentPage();
				$getQ = Request::get('q');
			@endphp
			<div class="numeOfPagesAnimation">
				<div id="numeOfPages" class="to-animate" data-direct="fadeInUp">
					@if ($totalLastPage > 1)
					    <a href="{{ $paginator->url($currentPage - 1).'&q='.$getQ }}" class="{{ ($currentPage == 1) ? 'disable' : '' }} col control pre"><span class="arrow"></span></a>
					    <div class="col list ">
						@for ($i = 1; $i <= $totalLastPage; $i++)
						    @php
						    $half_total_links = floor($link_limit / 2);
						    $from = $currentPage - $half_total_links;
						    $to = $currentPage + $half_total_links;
						    if ($currentPage < $half_total_links) {
						       $to += $half_total_links - $currentPage;
						    }
						    if ($totalLastPage - $currentPage < $half_total_links) {
						        $from -= $half_total_links - ($totalLastPage - $currentPage) - 1;
						    }
						    @endphp
						    @if ($from < $i && $i < $to)
						    	<a href="{{ $paginator->url($i).'&q='.$getQ }}" class="{{ ($currentPage == $i) ? 'active' : '' }}">{{ $i }}</a>
						    @endif
						@endfor
							@if($totalLastPage >= 4)
					    		<a href="#" class="disable">...</a>
					    		<a href="{{ $paginator->url($totalLastPage).'&q='.$getQ }}" class="{{ ($currentPage == $totalLastPage) ? 'disable ' : '' }}">{{ $totalLastPage }}</a>
					    	@endif
						</div>
						<a href="{{ $paginator->url(($currentPage < $totalLastPage) ? ($currentPage + 1) : $totalLastPage).'&q='.$getQ }}" class="{{ ($currentPage == $totalLastPage) ? 'disable ' : '' }} col control next"><span class="arrow"></span></a>
						@else
					@endif
				</div><!-- pageWapper -->
			</div>
			@endif
		</div><!-- pageItem-inner -->
	</div><!-- home-wapper -->

	

</section> <!-- container -->
