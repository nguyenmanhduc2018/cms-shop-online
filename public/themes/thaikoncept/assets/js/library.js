// version 1.0
// frontend: TANDAO
// company: redixel
// website: redixel.com


//================== start general  ===========================

var appLibrary = {
	
	//verify email address
	validateEmail:function(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	},//validateEmail

	//scroll to top of page.
	scrollTop:function(dy,dur){
	 	$("html, body").animate({scrollTop: dy}, dur, "easeOutSine");
	},//scrollTop

	//checking scrollbar for an object
	checkScrollBar:function(target) {
	    var divnode = target.get(0);
	    if(divnode.scrollHeight > divnode.clientHeight) return true;
	    return false;
	},//checkScrollBar

	//rotation an object by css
	rotate:function(target,degrees) {
		target.css({'-webkit-transform' : 'rotate('+ degrees +'deg)',
				  '-moz-transform' : 'rotate('+ degrees +'deg)',
				  '-ms-transform' : 'rotate('+ degrees +'deg)',
				  'transform' : 'rotate('+ degrees +'deg)'});
	},//rotate

	//button hover effect
	buttonCircleEff:function(target){
	 	target.on('mouseenter', function(e) {
			var parentOffset = $(this).offset(),
				relX = e.pageX - parentOffset.left,
				relY = e.pageY - parentOffset.top;
			$(this).find('span').css({top:relY, left:relX})
		}) .on('mouseout', function(e) {
			var parentOffset = $(this).offset(),
				relX = e.pageX - parentOffset.left,
				relY = e.pageY - parentOffset.top;
			$(this).find('span').css({top:relY, left:relX})
		});
	},//buttonCircleEff


	//to make the slideshow with this effect, pls following as below.
	//1. all slide items need to make css with absolute, width: 100%, left:0
	//2. there are 2 next and pre button for control slideshow.
	//3. slideshow is looping infinity.
	slideshow:function(target,slideItem,clickEvent){
		var len = slideItem.length; // nume of slide
		var slideArr = []; // slide array
		var next = target.find('.controlItem.next'); // next button
		var pre = target.find('.controlItem.pre'); // previous button
		var isNext = false; //direction for transition
		var curID = 0; //hold current slide ID
		var listItem = target.find('ul li');
		var listItemArr = [];
		var timeout = 0;
		var autoSlide = target[0].hasAttribute("auto-slide");

		//init list dot
		listItem.each(function(index){

			listItemArr.push($(this));

			if(index==curID) $(this).addClass('active');
			else $(this).removeClass('active');

			$(this).on(clickEvent,function(e){

				if(index > curID) isNext = false;
				else isNext = true;
				//
				changeItem(index);
			});
		});


		//init slide
		slideItem.each(function(index){
			if(index==curID) $(this).addClass('show');
			else $(this).removeClass('show');

			slideArr.push($(this));
		});

		//controll slide show
		next.on(clickEvent,nextFn);
		pre.on(clickEvent,preFn);

		//auto slide
		if(autoSlide) autoRun();

		//next item
		function nextFn(){
			var id = curID;
			id++
			if(id >= len) id = 0;
			//
			isNext = true;
			changeItem(id);
		}//nextFn

		//pre item
		function preFn(){
			var id = curID;
			id--;
			if(id < 0) id = len-1;
			//
			isNext = false;
			changeItem(id);
		}//preFn

		function changeItem(id){
			var curItem = slideArr[curID];
			var nextItem = slideArr[id];

			hideFn(curItem,0,.5);
			showFn(nextItem,0,.5);

			//auto change slide
			if(autoSlide) autoRun();

			//active dot
			if(listItemArr.length > 0){
				listItem.removeClass('active');
				listItemArr[id].addClass('active');
			}

			//set curID
			curID = id;
		}//changeItem

		function hideFn(item,delay,dur){
			var dx = 0;
			if(!isNext) dx = '100%';

			TweenMax.killTweensOf(item);
			TweenMax.to(item,dur,{delay:delay,css:{'width':0,'left':dx}, ease:Sine.easeInOut,onComplete:function(){
				item.removeClass('show');
			}});
		}//hideFn, 

		function showFn(item,delay,dur){
			var dx = 0;
			if(isNext) dx = '100%';

			TweenMax.set(item, {clearProps:"width,left"}); //reset item before show.
			TweenMax.killTweensOf(item);
			TweenMax.from(item,dur,{delay:delay,css:{'width':0,'left':dx}, ease:Sine.easeInOut,onStart:function(){
				item.addClass('show');
			}});
		}//showFn,

		function autoRun(){
			clearTimeout(timeout);
			timeout = setTimeout(function(){
				if(isNext) nextFn();
				else preFn();
			},3000);
		}
	},//slideshow


	//scroll to show animation in for an object
	// only show one time.
	// need waypoint plugin to check when the object is in screen.
	animationIn:function(object) {
		var target = object.ob;
		var easing = object.easing;
		var delay = object.delay;

		if(easing == undefined) easing = "easeOut";
		if(delay == undefined) delay = 100;

		$(target).waypoint( function( direction ) {
									
			if( direction === 'down' && !$(this.element).hasClass('animated') ) {

				setTimeout(function() {
					$(target).find('.to-animate').each(function( k ) {
						var el = $(this);
						var typeAnimate = el.attr('data-direct');
							if(typeAnimate == undefined) typeAnimate = "fadeIn";
						setTimeout ( function () {
							el.addClass(typeAnimate + " animated");
						},  k * delay, easing );
						
					});
				}, 100);
				
				$(this.element).addClass('animated');
					
			}
		} , { offset: '90%' } );
	},//animationIn

	lazyLoading:function(){
		var ll = new LazyLoad({
			elements_selector: '.lazyload',
			callback_load: function (element) {
			  // trace("LOADED");
			},
			callback_set: function (element) {
				//trace("set");
			},
			callback_error: function(element) {
				//trace("error");
			}
		});
	},//lazyLoading




	//================== start deepling ==========================
	//set hash
	 hashTag_setValue:function(txt){
		window.location.hash = txt;
	},//hashTag_setValue

	//get value
	 hashTag_getValue:function(){
		return window.location.hash.substr(2);
	},//hashTag_getValue

	//get page name
	hashTag_getPagename:function(){
		var path = htmlAddress_getValue();
		var index = path.indexOf("?");
		
		if(index == -1) return path;
		else return path.substr(0,index);
	},//hashTag_getValue

	//get parameter
	hashTag_getParameter:function(txt){
		var path = htmlAddress_getValue();
		var index = path.indexOf("?");
		if(index == -1) return -1;

		var str = path.substr(index + 1);
		var arr = str.split("&");

		for(var i=0; i<arr.length; i++){
			str = arr[i];
			index = str.indexOf(txt + "=");
			if(index != -1){
				return str.substr(index + 1 + txt.length,str.length)
			}
		}
		return -1;
	},//hashTag_getParameter
	//================== end deepling =============================



}//library
//================== end general  ===========================




//================== start debug  ===========================

 //trace your message in console log of brower
 function trace(str){
	 console.log(str);
 }

//================== end debug  =============================
