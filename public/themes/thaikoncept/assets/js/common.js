// version 1.0
// frontend: TANDAO
// company: redixel
// website: redixel.com

 // your variables here ========================================================en
 var lang = "vi";
 var mobile = false; // is mobile
 var sw = window.innerWidth; // get window with.
 var sh = window.innerHeight; // get window height.
 var touchScreen = false; // is touchscreen.
 var clickEvent = "click"; // click event
 var mouseDown = "mousedown"
 var mouseUp = "mouseup";


 //auto run after the page is full load.
 $(document).ready(function(){

	// detect touchscreen
	touchScreen = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
	if(touchScreen){
		clickEvent = "tap";
		mouseDown = "touchstart";
		mouseUp = "touchend";
		$('body').addClass("disableHover");
	}

	//checking mobile.
	if(sw<=767 && touchScreen) mobile = true;

	// page array
	var pages = [
			['#pHome',homePage.init],
			['#pProject',projectPage.init],
			['#pContact',contactPage.init],
			['#pGallery',pageGallery.init],
			['#pProduct',pageProduct.init],
			['#pSearch',pageSearch.init],
			['#pAbout',pageAbout.init],
			['#p404',page404.init]
		]

	//get current page
	for (var i = 0; i < pages.length; i++){
		if ($(pages[i][0]).length == 1)
		pages[i][1]();
	}

	//call this function first time.
	ui.init();
	mobileUI.init();//init mobile
	 
	 //deny click event with href="#"
	$('a[href="#"]').click(function(e){ 
		e.preventDefault(); 
	})
 });


var ui = {
	init:function(){
		var _this = this;

		//show page after fully loaded page.
		$('body').addClass('show');

		//add effect to buttons.
		$('.button.circleEff').each(function(e){
			appLibrary.buttonCircleEff($(this));
		});

		//get lang from backend
		try{
			lang = langFromBackend;
		}catch(err){
			
		}
		

		//search 
		var timeout = 0;
		var iconSearch = $('header .searchBtn .icon');
		
		iconSearch.on(clickEvent,function(){
			clearTimeout(timeout);

			var searchBox = $(this).parent().find('.searchBox')
			if(searchBox.hasClass('show')) {
				_this.hideSearchBox();
			} else {
				iconSearch.addClass('show');
				searchBox.addClass('show');
				timeout=setTimeout(function(){
					$(document).on(clickEvent,function(e) {
					    if (!$(e.target).hasClass('searchTxt')){
					        _this.hideSearchBox();
					    }
					});
				},100)
			}
		});

		$('.searchTxt').on('keydown',function(e){
			if(e.keyCode==13) _this.hideSearchBox();
		})

		//scrolltop
		$('.moveTopBtn').on(clickEvent,function(e){
			appLibrary.scrollTop(0,500);
		});

		//using layzy plugin to loading images from blur to clean.
		appLibrary.lazyLoading();

		//resize window
		setTimeout(_this.resize,100);
		$( window ).resize(function() { _this.resize(); });
	},//init

	hideSearchBox:function (){
		$('header .searchBox').removeClass('show');
		$(document).off(clickEvent);
		$('header .searchBtn .icon').removeClass('show');
	},//closeSearchBox

	resize:function(){
		sw = window.innerWidth;
 		sh = window.innerHeight;
	},//resize
}


//this function for only mobile
var mobileUI = {
	init:function(){
		var _this = mobileUI;
		
		//hamburger button
		$('.hamburger').on(clickEvent,function(e){
			if(!$(this).hasClass('is-active')){
				_this.showMenu($(this));
			}else{
				_this.hideMenu($(this));
			}
		});

		//checking scroll
		var currentScrollTop = $(document).scrollTop();
		var curDy = 0;
		var dy = 0;

		$(document).on(mouseDown, function(){
			currentScrollTop = $(document).scrollTop();
		});

		$(document).on(mouseUp, function(){
			curDy = dy;
		});

		$(document).on( 'scroll', function(){

			if($('body').hasClass('disableScroll')) return;

			var scr = $(document).scrollTop();
			var headerH = $('header').outerHeight(true);
			dy = curDy + (currentScrollTop - scr);

			if(dy>0) dy = 0;
			if(dy< -headerH) dy = -headerH;

			$('header').addClass('disalbeEasing');
			$('header').css({
				'-webkit-transform': 'translateY(' + dy + 'px)',
				'transform': 'translateY(' + dy + 'px)'
			});
		});
	},//mobile

	showMenu:function(target){
		target.addClass('is-active');
		$('header').removeClass('disalbeEasing').addClass('open');
		$('body').addClass('disableScroll');
	},//showMenu

	hideMenu:function(target){
		target.removeClass('is-active');
		$('header').removeClass('disalbeEasing').removeClass('open');
		$('body').removeClass('disableScroll');
	},//showMenu
}//mobile



//home page
var homePage = {
	init:function(){

		var _this = homePage;

		//initBanner
		appLibrary.slideshow($('#slideShow'),$('#slideShow .slideItem'),clickEvent);

		//init smallGallery
		appLibrary.slideshow($('#smallGalelry'),$('#smallGalelry .galleryItem'),clickEvent);

		//show animation in
		_this.animationIn();
	},//init

	animationIn:function(){
		appLibrary.animationIn({ob:$('#slideShow'),easing:"easeInOutExpo"});
		appLibrary.animationIn({ob:$('.content-wapper'),easing:"easeInOutExpo"});
		appLibrary.animationIn({ob:$('footer'),easing:"easeInOutExpo"});

		$('.block').each(function(e){
			appLibrary.animationIn({ob:$(this),easing:"easeInOutExpo"});
		});
		
	},//animationIn
}

//home page
var pageSearch = {
	init:function(){

		var _this = pageSearch;

		//show animation in
		_this.animationIn();
	},//init

	animationIn:function(){
		appLibrary.animationIn({ob:$('footer'),easing:"easeInOutExpo"});
		appLibrary.animationIn({ob:$('.projectHolder'),easing:"easeInOutExpo"});
		appLibrary.animationIn({ob:$('.numeOfPagesAnimation'),easing:"easeInOutExpo"});
		
		//animation in
		$('.animtionIn').each(function(e){
			appLibrary.animationIn({ob:$(this),easing:"easeInOutExpo"});
		});


	},//animationIn
}

//about page
var pageAbout = {
	init:function(){

		var _this = pageAbout;

		//show animation in
		_this.animationIn();
	},//init

	animationIn:function(){
		appLibrary.animationIn({ob:$('#bannerHolder'),easing:"easeInOutExpo"});

		$('.block').each(function(e){
			appLibrary.animationIn({ob:$(this),easing:"easeInOutExpo"});
		});
		
		appLibrary.animationIn({ob:$('footer'),easing:"easeInOutExpo"});
	},//animationIn
}

//404 page
var page404 = {
	init:function(){

		var _this = page404;

		//show animation in
		_this.animationIn();
	},//init

	animationIn:function(){
		//animation in
		$('.animationIn').each(function(e){
			appLibrary.animationIn({ob:$(this),easing:"easeInOutExpo"});
		});
		appLibrary.animationIn({ob:$('footer'),easing:"easeInOutExpo"});
	},//animationIn
}

// pageGallery
var pageGallery = {
	init:function(){
		var _this = pageGallery;

		//initBanner
		appLibrary.slideshow($('#slideShow'),$('#slideShow .slideItem'),clickEvent);
		
		//show animation in
		_this.animationIn();
	},//init

	animationIn:function(){
		trace($('#pGallery').hasClass('pDecor'))
		if($('#pGallery').hasClass('pDecor')){
			appLibrary.animationIn({ob:$('.row1'),easing:"easeInOutExpo"});
			appLibrary.animationIn({ob:$('.row2'),easing:"easeInOutExpo"});
		}else{
			appLibrary.animationIn({ob:$('.cl1'),easing:"easeInOutExpo"});
			appLibrary.animationIn({ob:$('.cl2'),easing:"easeInOutExpo"});
		}

		appLibrary.animationIn({ob:$('#slideShow'),easing:"easeInOutExpo"});
		appLibrary.animationIn({ob:$('footer'),easing:"easeInOutExpo"});
	},//animationIn
}

// pageProduct
var pageProduct = {
	init:function(){
		var _this = pageProduct;

		//show animation in
		_this.animationIn();
	},//init

	animationIn:function(){
		//animation in
		$('.boxNormalContent').each(function(e){
			appLibrary.animationIn({ob:$(this),easing:"easeInOutExpo"});
		});

		appLibrary.animationIn({ob:$('footer'),easing:"easeInOutExpo"});
	},//animationIn
}


			

//contact page
var contactPage = {
	init:function(){
		var _this = contactPage;
		console.log("contact page");
		_this.initForm();
		//show animation in
		_this.animationIn();
	},//init

	animationIn:function(){
		appLibrary.animationIn({ob:$('.boxNormalContent'),easing:"easeInOutExpo"});
		appLibrary.animationIn({ob:$('#bannerHolder'),easing:"easeInOutExpo"});
		appLibrary.animationIn({ob:$('#contactForm'),easing:"easeInOutExpo"});
		appLibrary.animationIn({ob:$('.animateMap'),easing:"easeInOutExpo"});
		appLibrary.animationIn({ob:$('footer'),easing:"easeInOutExpo"});
	},//animationIn

	//init google map
	initMap:function() {
		var myLatLng = {lat: 10.776754, lng: 106.671989};

		var map = new google.maps.Map(document.getElementById('googleMap'), {
		  zoom: 18,
		  center: myLatLng,
		  styles: [
				    {
				        "featureType": "administrative",
				        "elementType": "all",
				        "stylers": [
				            {
				                "saturation": "-100"
				            }
				        ]
				    },
				    {
				        "featureType": "administrative.province",
				        "elementType": "all",
				        "stylers": [
				            {
				                "visibility": "off"
				            }
				        ]
				    },
				    {
				        "featureType": "landscape",
				        "elementType": "all",
				        "stylers": [
				            {
				                "saturation": -100
				            },
				            {
				                "lightness": 65
				            },
				            {
				                "visibility": "on"
				            }
				        ]
				    },
				    {
				        "featureType": "poi",
				        "elementType": "all",
				        "stylers": [
				            {
				                "saturation": -100
				            },
				            {
				                "lightness": "50"
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "road",
				        "elementType": "all",
				        "stylers": [
				            {
				                "saturation": "-100"
				            }
				        ]
				    },
				    {
				        "featureType": "road.highway",
				        "elementType": "all",
				        "stylers": [
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "road.arterial",
				        "elementType": "all",
				        "stylers": [
				            {
				                "lightness": "30"
				            }
				        ]
				    },
				    {
				        "featureType": "road.local",
				        "elementType": "all",
				        "stylers": [
				            {
				                "lightness": "40"
				            }
				        ]
				    },
				    {
				        "featureType": "transit",
				        "elementType": "all",
				        "stylers": [
				            {
				                "saturation": -100
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "water",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "hue": "#ffff00"
				            },
				            {
				                "lightness": -25
				            },
				            {
				                "saturation": -97
				            }
				        ]
				    },
				    {
				        "featureType": "water",
				        "elementType": "labels",
				        "stylers": [
				            {
				                "lightness": -25
				            },
				            {
				                "saturation": -100
				            }
				        ]
				    }
				]
		});

		var logo = new google.maps.MarkerImage("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMMAAABSCAYAAADpRWBMAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4JpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpBNTg4QjI3OThCMThFMzExQkNDRUQ5RTc4RjM4N0M5NCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFQzZCNDNGNEMyQTMxMUU3QTE4RDhDNUMyMjJFMERCRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFQzZCNDNGM0MyQTMxMUU3QTE4RDhDNUMyMjJFMERCRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNyAoTWFjaW50b3NoKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjU2YWViZWYyLWYxMDAtNDE2NC1iNjQ5LTFhYjdiYzI3NzY3NCIgc3RSZWY6ZG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjY3YWEyNDQxLWYxMTItMTE3OS1iMTFiLThmNzY2Yzg0ODBiOSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PksalrQAAEEySURBVHjatH13/N3z9f8573s/iRhB7ZHELEXEjF2lVFt7VRVFSMyqojG+tWcQe7RVhJo1QgeNrS1qU1Riz6oiRBIk+dz3+b3Oa57X6/16v+/1x+8+XPl87ue+12uc+TzPg8sNGUZ97RYMHDgQAAGICFD/C9GLPyuphE6n1L+XHQIqCQYM6IOiKED9Zr+o3hQfx+fslCWU6vsIhfoM9bvVKuy3kov5g+2xnY66Fn9gjuXPWwXqS4E/A5kDkPxt8Ku/v19f13yirq6Oa6n71TdWd1374uuWZI4tsNDPaS4fX4u/guJ8pG6Wr0vE46K+rZ693WpBW41zdHP++hieQp2M77fUz13qc/P88PEUHUPyRH5++uf0q7Emda8t9VmhjjP3He6N/AFy/MqSj+uoa5I6R0efe9CguaDVbvNf1YHV8SL9dDxO/TB79hz1U0tfV4+xuudSPffsWbP0s5h7MIOlj0N3G4X+rNTrDu3nGEYlnmR9G4Uda4qXClQWbvKM/qJiDOTxbf69zQ+sT1RG340GXP+Z7GCivnleVKg3QnzDuV/dcWY1YbR46jaCPq4s/bFo1rMeDPfc6J/T/+SHicj85LcCgt+I1GUj8IKmzIILo0KVz0E8KxH6TYS8IFuYbIS665K4NzOZfA4jZOrvmY/hTUT2AiysiqIE5A1oh9ofjlVBUup7BnsOtQHVPbdarWTz5aaIrIB081mq+23rxc/zREpY8kbhjWGECUKQm/Zn/UYgSsbHfZeCoEX1vdILIPEMdkFQcm/xSqZYcLrRsudp84IOEhoaJTSVQrrwZmgXXvJnD7PPogeYzMPb9QxFl0u6R3CThHYTVRZfsinMAxqprTWK/lvhNwwiQm9XhjARbjFmFn6qDt2i9IvOajCn0cyzUCIszD2bYSU790Zz6jErMNa2YRojjaI3MFlpq7VnYRVgWNDx85NTKXZOrWXAMr7VDtqTsqOjr1F2Sj//bl3o+wW3wNpAfQRz1IbQ66cw8+julwUxb7r8GgoLHGu2pD9Oaw2y6wvFog+C0n0XIf9cbb5ZK/aze99NM5X+1Fpa8wWKphWNUkq6BYpBwvfwKq1WQKcSKJ5Mt9ly59PzIjZh2AzQoBXIq9qgVcxWKoQ0pYqAD4ufrDmoj0Oz/d3tJ2LKLmgpecMYm3FCaxLE90xQ87yEVvu17IIu4u9jfrFpU5DcMirNRmoVkJgJVUmnN36YI30GqxHAbQxkM6+tvzNHmXBUktcQbnEGy4TE4GLGlKSs0MKa8Wj+JlW+29YPLW4sv6jtgNndxA/U7mtHEqBWVnq1HxaGllw9mEhawvrVbuWhGCPyizdVr05iiY1gF1ezcYZeOgYpkpphUiYnaslqT7LaxJuCQjIZoZAKHHOtUmgFbz9j3rdIH9qYk2ZsC6+N8uYURnqU9LHOftdmnTq+sCYSNVh22r/R2qhkPaYXeeSPYRg7tyE6/R39r/6e9Sv6++eoDdEX7ifRnqkulKOBuY1ulXm0EgmrJkXyUxuL5oXpzAXj2IGXAsaerDeR3CBGE+ylM2VNjKrdLbYRhgUdXDe7AFHqMLcoSz8R3tJBbJKtseolKfCx6sxBPOHOz9CSUmoSDEq7OnNigsUGlNsEo8AEZSeTP+2UZsKdgHJmWex9QuUzP85kgxrtPr2Z3DhiZSsJM4SftbT+kf2ithZQ2pn2czUBA/qU/6A+6reBibb6bqm+O6e/36+p/BxJY7IqFqS8oMp0CsFBzX5iUacK5eSXdnGgNZeKllOFzcK9dCoY6mx1rNPfZjFT8h3EREpnnCVELa1KKq3LhN5XwIpLVb0f7UQmG7hw9r6USRm/kryTWvh7xMjgzG9BvxEoPDKmG4dqj9YbEKyJgtI0SxYLIiRaITjMaDWZ1goN5q88urQmoTNpzfFFNYhiNwaPI5tEOvqovt+xx/D358yZE56jy5W7WdlYeWPms+q7qJWOfhDDYJN1KFt29zdFZIzNbkJyeqC9iYSVyax4h85fEAsfk4gk1tmH3tFHL03dgqboWpiPjRDFUoVPU2DmCKqsUa8VChR+itgQhJGElUZckF/xsd0dffu8FKa8Ep6EesPaO+wozckuW0GEy50A4HeRhKwp8e20L9IuTIgZQUehOnZt8djNUeYSaSFGtRufMiFV722hsKixsrK67RizGRoDdlT6cB8R2ThysvsxZ3oTROvKGrH10RhxzbKsOJJSWsrhwGRDkNdGQfHpiE5PXnswzdxoahsa8wvCuwNeG7nlXNj7xTilgPHdowgFE0FGljXnYHyUz92zMOcQ62bf70yjPZ0f4+6mgObrAla1gs0jFEkoNjqvGDYtLOyFjAAxA8nmU7wh6t58TCm2RhzNAh/6MG+dD0L7ufw5NZOoJo7spItbWHx/fOOxXZdX4SikjpRQBUJvjjOVdqGFQBlm1HRkqWFqe2MUTm3OK5DYCCFxpqVFgfXmXOI4m82DcRi2IqmqmtFo0Di3ANmxqiZxXMSscKsY602yeNOHY8FteMSMr1ETUClLITjArIuiiJ4jm2/iY9Wi15Ey3jzWp3MCoF852DphWeY2RGIaoD6b3hzo9UMpBEQQ4hVzIrJMzM/tJkf2kcefiD756quv4MUXXoBPPv4YZqmf+bXlVlvB3HPPrX+eNm0aPPTAA/pUAwYMhEUXWwyGDx8OfQMGRPex0Xrrwdhjj9U/L7zwwrDRxhvrn6fPmAHf23yzYMcCivApxQm1XGIRRJjQhuqcHQ1N4VSbl5Aqf4MNN4TxF14AfcrpO/n4E+DeeybVSkyftErMuvr9jnFEzGk2dKZk03qkyKQrbfbJaD8QUaS6MGTY7JT4c932QfDn7GYoQ2zKZ9drruc2OHnTqDBZcha4nA+CkMXvF+ZTFCxMMsaUZp9FkMBH7QiiiF4IiCQCUv3YbkqWzTvffP6zW266Gc4fPx5mqAUrE22bbbGF/977778PZ591ph4oY08iLLzIInD0McfANtttG13j7DPP1P+uM3IkbPWDH8SZX5uRlBIWc9rLfo1EsEX7N/ZhpanSGE5Fcay9ztHHHQff+MY39G/HHv8rsRkysITSaAaEWJOlZh1mNIuUrigjT4ANAWsymVitkaQKAp9HwQZDNHL2MZhlRRfLLCRCw0ZwYdJW0crPT6xQ7LqwuQYw5hKxQinFhqB4XGRYUyMefA4DM9CW+t8d1KgSnrUftuscJCjDyX7761/DOWeN0/glzksQdYMymEnii0z9eCocM3YszJw5A3bbffceEm02cmWdbsD6kChlIlDmgQsbzjMTnIvS5xxJH1FS9zBr1lf+z0YLUmIuiYwzlZUoV5OLImP93qyTZg52XZERIqAaueoG+YAQehZ4r5DOqfNTzNw47FTI3LaNDUz5Bel9I+poLRDlnAqDnZrTb8LSXmJjnCwlEdxwPplQqVKuGVGQhHadP5Xmj+TfK2mzsHPMYP3r+efhgnPP0zZhkQmb5RMxJGKzZuJZE7zx+us9qWDp72HV9YukaJi/JIwb2Qx24aJNtGZi5+TMK2tfnnriyfDSiy/Cq6+8Asf/3/81ZC+ddC4qfkrsumIlFEwicxwnlrAp62MXltA3GL+bNqGfo1KMFYnrNgk6CrkUd/P8XHozNOKXnJ8hoCps53OuQQnXvr62TsqhDUTI3FbH+hcDBw7QwphN7r4BfRogOmBAW//Lpqz+vd2n74WBjfqc6s2/ty3Qkf/ts2/+veXeHBTi58g7hMFkGH/2OcqG69cXNAmsstG5Kj38oojWIztFF194IZx/0UXdo8RiUlOcSQTwEgvd5QYKp1EgYyJRjU3onUH0WetXpkyGn/xot67qN2ScRaKqQOEWUCZFJp396rM3D4959tIuSixaIqBQdBlZ9OaCyzh7M7SX4L0D8pUgHOcYFdvNvHJ3oYF77eDP8fri+5o9Z46A4IRji1YL/r+9SDjQqcYxA03w7rvvwmOPPipwSDFSMydEyD6w17nCrHvowQfh06lTm+8JbdgtBrlK4KINjaHAHoYknVuILltNXSdJJtnswFs0bg72EK6IwgkVfrjYyE1GnYxy+IRXjW+UYokCGBCjY6GHaB35vFGsPXsJPfN1Oy7sbSHdfQyjEIjnOsRQmaACgBLUgNUwfA2NYSKzAdqF0YL9apMwBCjjifjXCiuuCNM//xyGLbMMfPbpp/CK0uo9vawV0w4LKjgkjOvhQb/p+us9nr5owi+B1CgyPxCHGtlevOnGG7veGfoUPlYhHxkQFy/Kjp1gjPIC3SJIIarCl1hEOfubfHtjEzMXWdgP/vMfeOLxx2sku8BHORgGCkcXmoB1ZWQEYqN5JECTqZ/h4SrU4H6T1/jm2FYmudfFT3HOLx/b4sWNGo2Qg06kvlXJtRIi48wavNWKsCw6jK0xTCXpnINztE19yBx9n612u1ZTnzHuLFhppZX0z7847DC1Gab06EVZzRCkLiZJHITf/fYK44Rm0vN1+8LBrRHy+P1fX3ppFytAgvIwIwfS8JpEThbeMCmwS7Ac0yQbwBczZ8Lkl6coexJh6+22g7333Vd//uADD4jNIBJPFCcHQ25ErD9Mx0YuaHkvTY5zrC8cgrioaBSogTvHGCif1EPoQRvFNr9Dner8S4tMfoPyMSRfKOaPtcU5FsMUm3U2T6POy/Y/zDYYphA1JY9h0iYTgS+uqkcU9AiPdpopKvqRksM+TKtoRZIuVW7ZPCeCUP89GWyZtYrNoUE3wdaOdRNclkbCVKKYaRGUl84+xggzZs6AKZMnK3VdwLoffthlYZQiawwi0y2iKNZMq2ST7TVJ4IV6iSBFeCLCKCkJPdQOyZCoS6oi9OYvGGh6J3LYPSivEfiIOmyqrQ00kTq9IVpFJSMfMtSFNolMZWXH13VoyIYyl/oQrMYAAT+hhoRab5uhqCSPypCd1DikVuFDW91kiEurBxzS198ImExQJOmw3t6XSRSpmQYMGADzzTefTgImpncknV3YTiI2c/nTyDcS6FYJ6IMsiqZ634MGDYK55porq/W6mSrGpwljhY2oDRTOfsA/YcXsDNlkM2YDXFjHRoIg9m8wHRvznmfeefWxiEKjkEA0CGsjMunEbXDuYv7559fnksfz5uANYWpqUqj31wAiVeKUlESTBHDLAK9a1bLOpmROAhbrCWiWRKdc7pkTXj/efXeYf4EFtEQaMnQIrLTyt+CZp5+CaydMgMn/ftmbSF4KWzOaB3Hf/UbBBhttpMO5/3n/fVhAnWftddeF5559Vpl/v4X333vXo3Hdu+UKUyoPhtUYvx2j/UaP1pl2joZw+O/LL7+EadM+g19fdllVk6nTrLHWmrDd9jvq+/70s8+05uVnnT17lvbRXs+En4/mbD2a+eANxNf9bNo0OHfcOfqcg9XC3X3PPWGBBRfQftlSQ4bAt1ZZBZ5Xz8pjxSFirwWtXxVC10bPL62O2X2PPWCppZeG9959Vz/HUkstCcssuyw88o9/wDVXXQWffDLVbsI4AsVj/8NttoXv//CH8NH//qe+97HyvxbVDu0D990H10y4GmbNnu6r6MYceAAsseSS+trzzjevf87TTjlZa5C99tkHllR///zzaVpeL7/iCvDxxx/DlVdcAa++8qoG+M2GOdrh3nq7bWH48NX1eRdbdFF/rh123gnWWXedaBwfuP8+eOrJJ2vntS3xPaUoHfR4kyyUgSpJJmkjBahwNxhEBtekDh45ciRsv+MOcNnFF8F///uh//5W3/8+nHP++fCDH24NJ/7q/+CO228PYTgb3RgydChcc911sNjii8GO22xjFpe9jRXV5NwycaI+z0FjRquN9bQxOxyytShqhHNcbcWb0znOV17xO1hETcKpZ5wGl150kTazqkBCgvkHD4aTTj0Nll1uOTj0oAPhrbfe1qYOayLegMNHrA5XXXstTPrrX9UiH6c2x2x/jnGcrVcnYufwlol36M/+o5z6c8edDauuNhz22GsPuOySS/Smd69NNt0ULrn8cp3d50X2hxtvSuAXZoYYRfoz5Wz+VPlHvNgOP/TQ6MmHLTsMbp14J+zyo93gwP1Hw4svvBQ9GW+es845R23EBWHMqH11sMFJ++XUs954662w8667woFKaPxbCTB+1muuulqbSfMqqf/IE0/4a5137jlw+BFHwg1q/iZP/rfGKbncxN777gN/+stf4MILLoTL1bPyhijL2TDx1tvgjttu02uOhR3fh0ZM3HwT3HfvPfUFVZl5LryJIUNuLjJSYD4q0aDSkZrxMdkEnXh985vfhJ123RlOOuEEtRH+G8S2OuE9kybBG2+8oRftr044ERZc6BtRAQAP9Ilq4nkjcNbYS1l7iVdffVUnERk+cvZ55+kJ8VlNxGAiVZ4vLuQpRUh1ebXBTj39NDjt5FP0RsjpzsUWWxyuv/kP8N0tt4Rjj/4lvK03gjCN1H8v/usFOOuMM+DHP/kJXHnNNREUxtnWnTLWoiw9d91tVyUYfhVtBH79/eGH9f3wcx197HFqwy7iQ9fulMxgcc748TBKLVQel0svvrhy92+/9ZbGnLH2uvjyy7Rz67TCUCV4rr/pJlh9xAg44bjj1D38J7IuX3/jdfjjHXdorXON0nrfWGihEKnLLI29frq33ghTpkz2SAAnICdcNQGeUBvniKOOhONOOD6QF1gTTvva4pydfuVv9JvPDeNHqd/6M/e3fhKfUfAZKDKRLPNFbUEO1gbSBA1Fd6eMqkXZrELPVBK0lJEEccU3XntN/ztQ2dobb7xJVHk1aO65Yf0NNvB/n3eeeSqX/dA6xksssSSsM3I9P3m8MVzdAtUKAIrCoeuvvwH87Oc/g6OP+iV8qDdutcaDHcGLf305DB02TJto2mSBUM6KwlSZdPfd8NFHH8GINdaA0y12C0SGncr43KPVWJ1/7nhtR+fU7JtKcDi/abPNN/e1JW4K9x+9P2zxve/p79x0ww1Z8bb00kN0yJlfiylzcMSaa+h75mzweUoTLqg2yWtKyLCW9XBp4Zg9++wz+reF1EYYe/TRod45M8qsZZxAMewkpRfQTFBw+y236b/tO2oU/GDrH1pfz7yr9f1oUh+lg/gY68Ggw82/Dinufi6MInAp9lB0zxfvVjzvobFJ2Ka7ViAPjJPfYqeNB3bGjOkQ6p3jJNYXX3wRTZSZXFeMjgJsZzZHOsOzZs3yPy+r7GFn/MvwMTbmUcyob7P99tpGPurwIyrgRYkW3nvfUcq8WVn//Ogjj8QRqKSsku/98cce0z9v9t3vegBjbiMw2neqsqM/n/55rQ0sTS220WU+g+3r0Qcc4P/+j7//vfqw2jRbuRr6Vm/WYCsqLc6vJ7WpE/BETjjxWE2fFu5vp1121r5bXW31C//6V5y/SZDXL730YuRHuay1yX0UUTidNx0LAdZkA/W/5mcuPXXwDwPd6LOQjbbxGbT9RaFqyVOM9JCRzNUyNB9LsYmUHK8h4FlkS3WDsSkhoRezZ30Fx44dC2uutRZMfvllLWXTFzuZ7tXXN0CfkXEqRS8ZWGsejTnoICUZB8GJxx+vr9/X15ZICX/bg5R22m///f3xb7z+mt18KYYpPNebb77pfz5Y2e+sLSRE3L14ch9+6CGRD29+DRg4l48E8Zl23nUXrT1dUnGmFkDJhlLXfPLJJ+Bv6jrLKPv/8X8+Dk8/+bSOMP5U2fDu9e6773gIMfkwdim0aFig7PdwECT3ess/O/miH41qZc2t/v+/D/8nBOHSyi/6tnbQc3KX59TNS6+vNlBgViCfCTQmA3ULiWYyb0WvvEQuTp98/80336jkEqhryCz4N5Puugv+qt651yJKmq6z7sgkcUiG4MuvqjrSLBPOPOmkU5Sdvhv85c9/DtetUfsjlRnFoUH3Mj6QqLXIDBVHY7zmUguQ37yJfCmreDmTEXrcEBLduulm3/GfszOeAyLyxucozuhR+6kx6rPEYACrrLaaN534teaaa2mtHiOgSh0ZWjHRLAtb3yV3rxyFijigylCxZpCtc9SmneHHlM3kB++/32PmJFIh+r0naLraDKWvHwA/4AZrnq2IjmR0WVKXWGu9Vsj5C2xuMKCvUnjRdGqESgF8kgjV9u3mW2yh1PQu8O4778BKK68sNm8RqsS6jNaee/1UbwR+bb3NNkr7TIarf3dl7fdX/OaK0e8crvQBCoSsT8ZjIF8ckXnttVftITES4CsLM8cetgQmycGllloqnOerr7Lj6ko7sZCgS4Rlllkm+io7yewzSL/RUFWWmtlv7JFHaWtj0FwD1T3PCi5FcqvOrPM4M4E/KwpMoPbGgdd1JAVCBX+NQmd2J2Nx0aQyVJJqlryWSLLVLEMEn0H9WntBRpFKqkKOKVPbnAFgSEmHgqxLvtgEYLv79LPO0khZhu8ePGY0vP/eewkeJlA3erMvU2XIZsoN11/n7VqNfznyCPiOlrBVTJDL01TgQTaMVKdBS+8MO/NmQGAK6Qla2rCpBYrYhSBT3yL1jyiqLTEDw3kc+WI/bvr06eatzC3OD0z7bJr6d4b6bCbMmD5DSfSZ6m8zdcKs7g6//PKrCCvmfcEo2y3mWAk5B/vOUO1BbZluTZa68Nk9yYeE+YWZy/xWQ5CNejogHysw3XxmOuacy+iLpMaYEYtnjBsHf3vkUZ1E+v2ECXCwchT/dOedMCuddJvWx4g5lWrNOpaghxx4IHxsfRE+dty552hTplKVpc79ceKzcGQp8L7mAw3zJBGw/37wQQI7gC532iD+PPMd6oWbmk9xpM+YKTKb7+75008/jZ+r3c4KOxb9hbbdjZMquHSyD8BEx2VC4OZD3nZNDhg4MDKr+NxOWKXzFcaXfCZd8IZWNkXhCjVcLYAsLIdssBAq6iq/2zJvoqRgv6g/A2a2WWIXSoedP+e65VsnToRttttOZ00P2H8/mCxi/zzBheCVLWxuggQrL9VsOldi+dH/PoJDDjrYSzi2Xy+69FIYbDOpMgvz7xdfjM7BdnWo6c4LgoFzhcnmkOmUKVPsQuyJnDaZBaoIDXdZNhfdy5W3yvktPUK1CgZ8Szj5bgyisSolqzYaRpWE6jIXaBmsNI4LrHjrwSZ/3VMZiIh5PfvMM6Zgx0WVxLk067sPz8dVYlgT+te6xyUvDA4pJ+Vjp9JHgXITmlKIUyLZZWEJ9gZbQgjUM5lIrpfSJ556qo+Q/Oayy8S1AofwAgssGJtZaZlj5hqB8tBokOeffQ5OPuHEYLsOGwZnjz8vFKDY602eMlnnFdxryJChUCVUiy/IWsa9OJLECa86EjbsFutLnkv6Ro898khI3gn/wVkYofxWkAVYc4UjdexYu9eiiy4SxtkuYkdTGR0rCa4z48x5DOdL6kSaBkMVHgIyeL7BUYj8wfvvM1yufW3DDUux8GLUq/Zdyn4f3SLqV3/r1/+qK6ifO7oclR3+gkSFmJ/MGva2CPAFOfw91ZiwFAG+iOoloyymT8erTDKwMmG16KKL6oxsCGO+noBEjaSTUZAIZo7SqK4zLdEHGe68fSJc//vf+69suPHGcMQRR1ZgGGefdYb/jKETTiNVTSRzkW+tsqqf7AvGnytg3vg1AMmBuqbiM9iT3HLzzd5X4JzFEkssEesUx7ohhNZGG28EO+y0kx7LCVeFwAHfM6ZE07YM1hPHqc8YtsFrrM6zWW34cM+67Wl2ixDpG6KEjnv94aablJn0iX8uR0wmX7wZ+BlnzZqtx5PfTI/Pv/O//OaADUPD+Xv+SQurFagOT+9pTYSERvxaqluWhEINpUq0AUhGcavQDY//cLiiiuoOXDq8CYcsPQQW95MOXSgvaww/GxLlSTr37LOjOgfG92y7/fbRgU8/9bTG/PBr8y2+q5M+Fd4p+zNv1LXWXlv/xnii97yzjz3lb0gYRyU1R+M+UL7IJRde6H//wdZbg6SrNKDHVlRrscNOO2tIDH9484036gSpSxAygNCYdtWQOf+40sor6RCuNl1q4ObLL7+Czv04S8UUALW8nc9msIZ5vPYaXHrRhRVTmavc5Py7dgT8L1/2xJMZ2WBqzsvSFKI5SAb/XLjJbdlwamyLCNEqmLHTTjW50Y59JUxIqyRlZA0HaURrSoGRu+b7HMOXqnvjTTaJHDqNfdlnH7jsklBcNLdyVulrYN6l46tLp/s7cNTPf65Rnu514smnwOqrrx7BN84791y46ndXatucoQTVWJ95pkN/friRoKefpgFonuCgaKKbzLELZnyrTCj3mquv1gEGfjH6dokll6iSBYCJWo458ECN+pxhHW/uyHPQmDHafxg8eDD8XGlFx8oty0hZCzIU46BDD4XfXn45RHIgeZ7bbv0DjD7gQH/vsr56HovMfUf5Oj87+CAdeYocdrXa//63h/1n37QVb65Edbnllg9RP1ukRIK7jX9vLTh4gZNatpii1l0QKnDHnXeGdUeup/Ez/N5ok41NCyxwBSClWgwjYMSIERpByuwSKTcnlw3+dN+9YQ11/FrrrA3DVx/uL/XlF19o4Ndiiy/u2TRW/ta3NPRhxBprwgYbbKAh0/ziBhjslPL3p06dCk8/+aSWUuxkrbf++lqNMsKUocT7jzkA7r7rbrhv0j2w9bbb6glkaMBLL7yo0Y4rrPhNmD79c9hRSb/VR6wB64xc19vvHaVGByiJxc/Lk8mZbZ6ktdddR5tHnD9gyDRYZ+/bm35H26rzzDMvvPP223pRs0P/r+ee15lb5pJiABxX1rl4+WG/+IW63rJw5OGH66yqHivLurGPOmbEGiNgzbXXgjXWXNNPDd8vP/vii4Wx4klnOPUI9QzrbbChtsP9WA02Y8V16J9bKcoQkeefew5WVSbKbj/eXTnWb8N/3v9AO+/8zOuosTngkIPgheef14hauSCY/ufOiRO16cP5lyFDhsArU17RIVXeDKzpGH283Q7bw5mnn+6v6TYE15jwJnSvc886C9oD+mDrbbaDd99+B2Z99SX0DRwAq666qo4QcuZ67BFH+Chd4Msq9Xi/9MJLMHK99bS5zIDPvz30sJ6r5VdYHo745VFKyJyux4HniPuSsDXkOJ90EdsKw5alAZZSI0vDJ5pH8K5nuHK7aAM49gsLQY6qS+wxbIe5G2eGDcPCZpjUOJ3OhRkYIfGCOmBoxccff6KvMWjuQTDvvPMZyDPX32KVYn6qWvgc+uT4+fe22koNxkowl1LdvFBfevEF+KtyRj+fNl1vRnb4dtl1Fw0x4H4BT6tBvvsvf9aDtJCyn2OJU/qCFL49Top9qRYxPzdvqGCOSbPAqHmOrXMNQEkhwcfOHm9+ZhGcb/Bgn3VmbNCrtmZXF8D3d7ypwhisoshR45OV0mqcP/5I/87myuDB8ws28DBPaNG/bqziTD8qobOyFk5Dhy6jn3uOmrM3X3sd7rv3XqMRMJKMgSgeUUNj1lUbZ9XVVoeB6h54LD9Xzv/jjz0Kzz79TASncdPGcyoh3Ft8Z1N4W22CRRZdDHZQm2jokKH63jjy9fCDDyqz8d2MjCa9tnQfOt3Drg82+c53YM211oBFFl5E5zwYJXzrLX9QgvZLSyszAFyDnoigeqXllie2YyvkUZmsV2BdRu8EuqZ/RJSvvbVJkY5u+Ie+EN1sorTKibIJJJfNZOo154BFxeQ1DrxjqObjuXjdmXea4MCyLkCWoCzULfT3B6IBLflt268qR63ouYA2vGiBiJyFRWv/Gph4CU3cSB1dJkm23qFlqd5lzweKW3dhSObp59XF9Hxs29+PYU6vC144to+ObQhpxxnj8DNiHoWDNlttws0cRm37VmWyGWF6Wdbq/3g8bIbNN91ELdx39bMwB9LAQQN9OD4tyPMBHDLs3UxJaZ7X5EU4ecwkAnxPso5fj4XlSwoncn4zFlVqk0wejQS5Lkahybg5hozWI4Zwaul5OTEO2mTbr2Akncl1CS2KjOOba5SQZHwpxucURZcomP2oU1JcA2D7D3QFihAI4luAuCUVNSf3ACo08RAV/IfMv3TqQxNDi+iSx0a8+CDKVSnC73j4hfTpJNsepRshbJBSdA4K0cdKDKY5hC17yjnu1spGiAMvpShG04k2C93gbrAs5BmwV1qUhStW62grpZPAWDTDfOF/x2iFViEU6RQWmGn/EXEHSaqXZCPU8vzECyY8bKjcxQq1INXtBYv2DIujKNKEC+US5b6AByGm0slTvWeiZk6lliIf0q1e18FCSpGYw5gPNISUk0YohHH7LUcQJljzIBJAFdWSgV8UVSxHRljKwjCJYYKkjh3zcQORNQ6RQRNFoi7a3zGUlN530BAbsZl5Q7A/0LE5B7cHTRVdGd1DEXo4U8MCiVtKuX7KUJsICvE4Z3cDFsmEYjfkhh0gQZCLNVYN1R0bmKZdjXPL0qZTlqI8RLcC3gW9RiuKLhdHFzkTjN7ZSA5mgrbx5o/qNDButZt02bKAN4rpZwDFteXKxepYOa1A5PKwEYEbZOx9OYcht5Bj9CbBWtmNvgU9vAMrzHF5jJvseISyF56dY17frle53wB24zhuJpGBhrR3bD0OiTBOpCSLAsUkSLvbqN4CQgMQhG5UHjFZFkbtOzDtVYZVjFrcI41smyXIgBAzaE1PqQKe3a++H1zany0uTHF4oMC6Ub2gpzAmqJAJVxtP5ePQMVU/VsKpOTQvZeAXaEPsnu1DfJPyLp31jVB3GY3Wh9sYBD3x3WvYBjpG74ZwuwhbS2HnupRGDerB0B1xZIznQMM0SjNbmi1emUwep0bZPjhV9eUtEqyr6sRUQFriWIprNyvZoxr7nShuZZVUvdXerkfUlv7JOGKUVrM1JtnKnOmEXWs1pL0vx9QTbVEXARB1/5H9ozHrz6UyoZTsFxDzLiNUJW2luSLZe62hm4y4p4gqoDwoiqrwx9jCkh+mVhBvBi3FE5Y+yjjtJLSo2xChyxIlYtpAjbiqzdX7u8+1+cQbgmwNdBO9YGiRJHEqRXeEquXW9Bln4WfU458SlmmKE3PRJkRqzhgL+ITpOCQIcqnexpJAQhTtdqvEZHlUqG/BRIJdEBv4poiiMknTsVM2Ya9268llo30bAEHVjCnEpJLxCg3KO9aUjRvP5xZkPH6SzVDqsniOsQZGWE0M+nJj2bciRz0kAjqO6Q8zUH75MhuibYmPQxK3Y2EbRS8MFoEkFkX72e6vThnaUWFk/3an/isjglw7uZGpkaeioUhqBNBY1Kq3puufCQOHfgtOAPpqNmpGFUrTzE1lUQRplXafkaQAJBszAibITorp95OeEyEKI32FYJpR01iTDT27BoVA+QZUGUofM74dkAVh4Z6byRXqICN5nU2RlJcte0tHcMZOtG2wWMv2qLuNtjwrSqBIMjCSdkrtXo0SiNg0D1gLGuj80Ettk2CLU/Opr4HC4UyCh77JYaBNz/VThpyzEEwV27falCsWtc004o3vGmYErRA4XykbKiB7hKtTJmtqmTaymSIEpMj5IeuwBxMpFuQRoae0+WVPuURRxh0/64t9/CbMOb9ZXxIh7h8do2TSOo1ciGWZZZeBgQMGwsBBc0V/WWGFFWBBJkKjDrw65ZXo2SQdJolSgEAt2lRyTJG/qsOt1peFIvTrbqfruBorl3F26wgjdSG/cA4ZehMJIVATurpWP00pu7CNbASuVvQJJ8rwNqU8qmRx8A4YaJrvNbOII4SOndL29xupeZhFw5PQU65Ikaa5SisrYams4oBD+S2BKDRLek4IACOGxd9rKSuJsQ5mLFbqSZIgqU4cGgcWhdks/l4blCEYtsyyMHj+wRpPdOzYo70vx9n/RRZdWCfK/GZArGxm33yegvhkVAQWRSURGlpXkdcaJgnZMp2EuE+E3f3tMMqQzd4CpJVOdd1dUCRvgvPrHDLZnokSFehUf+jLJiIqVpKipF+HTAInUqPgzR1NYiuKQ+KsY5IjEbXgVqn50CPVaUKMne7Q8EQw9FEV2hJd2yW7fKOWhDmDQPbuSwCRgTXdj1fRPXTtick6pdX4Lf88aaSQEu/dLAH081RI7Y057R2/HnrwfuO49pd6Xl2mvGWz3rmIXRCdMb+XB/W5cmUqs2H2KDdROr7XlokuaecZoWjqo0cCx4895AbiCYLIv8BoEceBQKpECShy8kJKP58giyzKkkQo2EiAQlDV18TpACD1Fcgn6Zo61kcRGZH8ix1n0XklDSm78KDceQBxogxFNyOMqIRDbkEohwJ7oYMgrxVA+nQQ3yNR/qmdvR6FULE3miDneENqBmOT3gYfgQrmGfpanDhJhxmoDGrLxrMh2lyOCePa89Qt5rICAcYeWsgam9uzG2CALniG6hpTxZ05FPDIhVHURtulhVCK5u2u+2bLUp9TV1dDHGsH3JfBYpfEoD8Wok0Yhqu+9yaRqByM/AWsD6NmAhxQSWhS14BfiMKEUt8oH0L1GkUuqAC/6Y2wQPafdmZiFSKTRmLRa2hfGlo6qHfKe1Xft4FEwtWVpPLxBI1FyLLxSNPOpci5K0kQXkXHYmavJ/TEwilz1y0KzBYdpeHFlGKeJ5pRpY7yPXJAM4KztCFRslGwiBwhtwkYTTtobhEGpih+EqgqjX/ESMl5bL/sNDriEnzuk8I63q5gJmfCpmFvJvFtWRAjYjXBF9Up2wHvuO44Nizqm9ZTs17x8AuRzyh6bCJNNhELwtfA2pB1igykwPFFgTDaVLlhxp/ENP1UYYHUGWqGbHDoPZ9bgDjCg2Jyk9BhpXeXB3sVEAHUavuyBnNDMm7IBoluMBjleOEll0RDxnTszF5NAny21lprw2133gHnX3wRXHnNBDj1zDM0DNyN+vY77Ah7/nRvf3nG7B9y6M8sbydoMt37//YQ/HnS3ZrJ2j3zaWeeCSuvvLI3AY8/+SRYesjSEfzizLPP0fT5zn7nzXjiqafAFROuhnHnjYcbb7kV1t9gwyTBh35BgsXT/E59f/yFF8Jee+9dE7MzG3DVVVeD6268Ec678Hy4+toJcNJpp1TmkwtdDj3ssCR0zdTv+8Kk+++H6266wfTpxlDR+M9nntFkwfz8C1rCAP7znydN0uN9zfU36IIf/nButcknXHctTLrvPrj9j3/UxzSb0FTxQbC2nl5ILoojSS530Mq20qph/CASNd6kAX3sb2hQX/3NCnBbxgmpdKOxzktZxpBn8BSBopE1ZIDPLmGVQowRaolqI9Pb3jcXFP3qxBNg9Kj9Yfq0z/TO51LMrb7/A7hj4sQs4EwyiPDi3GO3H8N+Y/bXhUZ3Wgp4eYy7PWbXvuDiS+CA/UfBBx98KPyMQMJ2/Eknwb+eZwKBE/TfmIn6l0cfA08+8Tj0z+k3DqxrxmjNhW8svLCu1x13xhlw+BFHwO+vuabqUKrvLrTQwnDqGWfCIQcepCll+LXOOmtXhmmPvfbSG/7iCy4wXLWim+sVv/kt3PWXu+GWibfC3Xf9xY8rFyXtvccemk5+x513gquu+J2+Lh/Pn3e0wDP+GH82SgkXpsOcMvlleEBtsHpkgND86DRhDTKApCp24frYRzLmaNEY6pfoaS9bNfTEJjV1oAWrZhIBZUoGMWpMkV+P5PsDu2wkdnWMKHKpqZTppV78FHG0NRl22mVXuEwtUCbldY7zH++YqDbC7dFxQ4cNhXVHjlTvdXVRi9yQGgLsNjPVpazMlc8ff47SVpfpkk5vuluVzWYakxtzbwT34sKaY8f+0gLjSt8ONjh5CP/78ENdPXj2+PFwSdIqWEJduDj/6quu1PXMzi977rnnou+zVGdK+BuVlGcKHSchSwt3ZqG26CILw5zZs6OooQseLLX0UjBjxkw/hwyi42rCLbbYEpZffnmf4OulL5w3CTOsGz1k3syYAfie28YnbCVlxDUeFoWcistcF0UI9bPp1k673TsHQ2JjqjeLsYbwpFNlRKmCiUOG0iZFYw78cJttKgkUZ0cypfqLL7zQvBEwjmAxSwbXyTq2hBNOOUUv1KlTP4npXYYO0+WrfNySSy2pi++dv9NXYGLz15vDzz7zLFygFi1zJ40ZtV/AT3H30EUXgbfeessfvOlmm8NOO++sf2Z2CWavloEB4ysUmh2bSb64Eo41BFeRzRCkX472hOPyTz3xVIjyZlC1u/7oRzDxttt0pdhvrrwSbvnDzdAREayjjhmrezucceppibAYpkzMa3U/bBYm3p9RC3GppZbWx34y9dOeooxc1bew2nDaDNaVg4HAgavP7r/v3qpfRxBD/ImiXt9U2AYzOiRMaUQj4zjHvQplQtStv3ZksEQEXwn+n6gGD0Qh1CYXM2Ac4sMqZx03wOYaWg+n1UIyOIGxLdiQ8aaQ9Joy+RW9yN+2RFfjx52tWyVxxx95Hi6zvE7ZviwRuN55nXXX87F+wxBXTw4cIycJHn30ERg41yC45NeX+zpf/tv7778Hq6y6qpZe7Kw+9sg/4JmnnoIjx47VUQwfybGahCeZN9D6G24A+++zD6y62mow7txz9fFjRo0y57aFcrww3n7zLX3vvCEBi4oW5RLH7XfcUZfecu0615WzNnzkH3/3Lceuv/Y6Jek304wT8th33nlbmT57VZxJxvBcc/UEQ53pLIAuzvMj6rk17IPLM7VZaEp/0dr8ehNTYh4l9OK+vsQKZ1M1WGTzXr7/N1XhPR2RG3Hwk9LWORTpxJJM8GTNFKwB5VFSRikamsv6N0qzXCAoBUNqPA611cEJSGwE82Y+nV1+tAt8e9NN9XX7BvTB2OOOU6bKjdWIkKNNFyfQGWeHY6q5gyp/CMJ999wLd9x2u66/dmFNrjO+U0nV0848S5tMnFndYKMNNdHAU089CSUkDQd1N8xSOd0DNUEDL7wFF1xQU75zLW9gJTQa+PbbJ8IWW24JW37ve9oRZEeWGUDca0t1L3ffdRfsveeesJ9yxJlVgik3HfyCr8x09DfecCP88pija4xfqOCC3MSih6v0YCJF/kIR51Bqa3hEuECCPi2fU1ED9ZaZichxhpDQbdmiNmeuWs2QOM4R4avEtFEGgCDT4253YdINsurcy80cmPIE43J22ZlQIJsOHM24U/kArFXm9M+Gl1/+t9dlzNhw8JgD4NCfHwb7HTBGLyjeIPdOmuS5vZlShp1jZxZOUxL3vffe139zrX4//O8HMNM2RnH3ynT5fJyDbrz88su6ttvd4p/++CcNM5CvCco02Xa77WH8BRfq5ils9v3s4AN1fa7IqXoYBDN63HDd9XCZcmxZExysnoH7qTEBGRMb+F4N6sCvvvwSxuw3Wjmuh8A+o/bVm+9Pd97hr73KKqto7edeTHfJhMBMkfPFzK80vc4XX34B/3z0URg+fDVYTvkAmv6e+LsvVZKUvHBef/0N+P0N1+t7Z+16leaEMpufeZFSHla5sE1NSRGXCmdFDWbhPehRthY20oVx05ekkix8gspm8Ht8+Morkat0Mql59NEgbg6ORQHdCnF4I8zp7/jjUKtBkb3FGKNGYoNFTBA2yeWIBnzNZFPxfMd1ni8shBlMU20B+6Bs/NqobbewmF2B75a5TnX9bdJVCDHW267jPQj4CJslhYME5BQbhk1tkKJm6xeapKBtbf5A1FZ11VDjeUpboOKztwWDz7qtDFvdpQv+QUeCHF1ky5MU1JUuGc3fr68doBueb6uHrDM/s2ly3vJz3PZ5nBz1nwzMdHTkTYtajSkyhBAme1x2cdpNAQ+vbSZ34Lni62qS49LMA/gkK4Ige4WQGyjSkGamIMf+3bFmmOAUWkdLIOtdAg+qdbzevJIYKPFT06439w2+X5dPlHUxsBATh6oMgL5QFprDtwRYiCw3dE4d5hI/SQIrZLktiNHVWaNA7FIV6Z2iAwI4DntuEAOyVtn7deA3n2QKwijnBB4qHcdssAeWfNE80xUPuectMhODmFDhBASzy4N4yEkPNeUyL+Hm2zGCm3uyNREd4ko3iFLr5CM/mBT95OP8ZQ41KWwkyuZOJRQhxNn98QVk6rJrEoMiJ2LYK+qCfFhZUA7bUjqcis1k5hxnmbmUWHgfMy8SmHXEE1KTdPLPW++AkgibUVl6/BQJGDz21nA7hL5FvgRzFHcZv84B4AKGSUavmq8blWdKJuxu8Ac3Pw7HBEVcU049XLssk97mJjBTivp4t34Ll2jytbf2hoseOn16jIqjcoGYyYEq0Ns4Ne6SKAihj7NxnLv0fEAZBpbJQUExD019SUPcufQ1D2i0Aog6ARQ4+rTKym0IuxncsZBE42Ltkq/TMCZhXhdiggcyazIu4sEe+9G5aJnPA9Uoe0JR1QZx4ZG093vTSJKxA32hWBxBgiT7GQq6HKIXQFT/Qfca+miNCFPWOd0dJ1RcYRQ/TxnV7IqN0I0SxdrGur4YCp9BjXILXeDDMi2PWQLsBnFJEt0KYiPlU3pR2E1kyt1ka8e5iDH5rsgnK9nFh5VKOMqLDxK1EuiKTYqYLCAGFMYI1ZKiQsgq80WXBWmifUWMjI1g7VSJI/kqPN82t4n3tRrtI1/LHnO3YtaPSyoZRVJSJukK7F4p6eEbFrJdChqaksgXDbnyTx2q1TcrXE2j/opmQJ7ICLoySikxIsBazW2T7KmMvSJj42y31DRFA34YE23mkbFWjRYYZzIlhUtFF/pFJRJlWMQAMMzsCJBkzHIDF1XkIGLeLCxLqJaF9ta9z4Eno3lKw+ARc03Me0XOWE94merXCQUzuoRY6BVYk6/KbGAXgbJ5n6LnNSKKtYRmQgg949zAOtOp8MwGQuU3S3aZEQzRnjqVTVBtZOcryXx5JWagHpTdgLKqLJB8BZWdM5FIaAXftktgVDTxbKuGCS6ZqFL2HIsSTpmFnBTFyN4FFJlI5J32OgQnUVnN/vsah2Yt7KhvXKRQmm0A+bbC7jul0MCxr1GXCK1Ce3xwBePwSO3xSBGOKfKvKgC+Zh8pnqsE32XzLYaEAA1VjLzFoqHmIBjStkiiBBsSsE5nEdvsuTFCWeZI1So6SgoyZHDUA64EjJeiOmXK5qgxWZAgGjoSuIo07PLcIl6d4ra6moRx0ilk94uKf5Wr1QCbLIpqjSWpGVFXc7QjSvdiPqUQyqW6ZKrIlEMjIUQ+PxAAVTmWEUi4RoUGF8ER3/Cw6EETouCRKnPDE0qAWzrJaktehYi0+JYiiYbk8eUm3i2qpBABAXvqdhBsdohqHrqtKEoal+hsMRRiI8VOJ2ZCsd7pFszYrgAoP8Rpb2ERRcouDKpV+aU42LdzxTS8jnGsPaJNCePfu0kpW8mmxzZLWB9pFHgrhG795TI8ua6WHavSOR5iioRH6RC9YFk7EHp+Zp0E9jRFGJvLAjnrakD4jMxDj7ymW1bSpBfL+dGOi8jz1JCgpm/qQSm6SIriYssw3YtDBr6sM2w+sjiZDB0hRrhAobZDqM1RhziBkCfMpAoOP0LkUi1kirnCH1LHbSzpKqMoEoVFgDlfh0SpopnY+9WHW2JvckdvBFeshEVRgchj1ZqLHH6XNQYI+Kme4BdCsiPKQi7M625KTPCyTMwjbCRmiB9IRL8Kbf74ZKGzTCzRsGFjd/6bKf0rPS8lVCxnrLXFsvZrYzwIY3OB0B8L2A0NBIIcl2LWjSLlGYKouSKKov3Uz3U1sPm5zbGBQyXMGOn6uBSDQ217qWM/j8gVXLbT1XZHTmuqjWT0Cqeq/+2to58Fdg0tGibxUpRn5uuUUWglhMQ0kxu4qwYXdeo6kVVGEjlrIuW0KMQJvogztqfURhhTF/Zut1oeauPg267cE3zLeW8uhMQRNQDjCKqhstghq7fbSZaEQj6qUR9WpVAAJGHPwvFtSk+EFksmq2eijMFeBGw2dmRhSKjfR6i4OBV7n95Sxx7sGf7sYEkKRawzH0Q9t/3sAAbDSl+jGxQhELEF3qAc+DLXW6OprqUusIJJoAGiIi8p9Gqif7KarXLtbok215Mu6VHuzCGZI3GdbQVytHAbISV7xWhJBGdW8oGGOHsKRcgMk+/NVkY94Xpj3cAk85uGJvOx6rhGWqbejbRst1v+PlLKKy+TUIQWRQQqKmfFPLlYgAOU16tfbnAnaEXQY2oIMwZ4sTpggvr/rRGzYJM9CoKFXLJuFHUEB/EGcb2cZdF+vZ+Ske4C9uGy7PFiluxjcccon5iMaIqgBsdUjRAFoVdlJtROc8tg4FJnoOBF0nJNqxEzcfl6bEt3JrN4sKgso0wk9JrST7tQAkQNu7MD5GgZRYNE8mZaqedYCgBsiIjEfoaLmhX1Wz/KtPvQ3iHq/bbXZmnJaU2izDrdb6hfDwuh76JrWBF8U3GHgeomXWM+q1JwOeHXgX2AZFeJ0QHNprDwNVJ/oVf4BcrmNIL5VdIVWQvIawWRtNS4Ur8RiDLtmYIJUlIpOkEWIRoky4ozUBcfThU36uPsvThFOTwQJE22M3kJ2cnRA+soFHc0b0T0TqPsDoNWi2KFA4kymjBi7PiM7X1lN3eKZGK7OO0d9c+e6uvTvd0NXRYVShbyOHzdKF0TOn8JmQ75J2pMtcmNJEOxlcWMOa5cintqeKe9hygSyoiflPkEgsRd8M+mAQNiB7sVSRrMGEqVlL7w/NMIFFGs+VJH0Ic1qcrL2WAgBW4hCmq3yPdWSrKogRk7Tb9Dj0Up7roktVktbQ5GwYIyqk2Fh9VYj4MM32x1MYPD8J+uPngs2M1FZgtVHZ4y9jV6SKZCVFUWInaQZI3zGzD1C/Fr1rLLNlxxcIICM2H30FnINlfyL5hMd0pmbDv3uJqF+p7I6HMLklsf6wB9lHHl0hg9Qm9AL0+OW8YUIZK6MSMsKc0tEPgNUTjHmboFB2NOo0Cn2LQJKeP8hmdR1z5J/ftkfQbWaTD9vI+rg04lwWFVdLOdo8QkJdGY5sQiRlEkkQTMgvLyya86UF/UwA+TUGou/9Q1wFIPOQn7IIGbNKCDTRKzy65zKFaflo+yyUV97wGBgizLZFG4eocCujYlT2st/HlTc4EkzIMyx4ekl2RF6CppgERtOEFjJEfONQUfh0TETh3PJW57qvfMegmi73WGOpP6Hva7cfQ1Jt1u22kkEcnJ95dIAgMQ92eDKNHWHX4RY8bqKCfrGRklbaSPckZo4G65hVJAv8m3P0jbVMR5NBSh36IQDl9NYwl3oSQz48gCqAtbXdwcMa476OY0u3EoxT22igK6jy9VIBRlKTpJ1tZqUJQ08rAPKhOSAsq1KxWWKlXmL7RbpVfUX3/RJfN7uPr3NUnP2RN0WTj8MdSlyA6YbMjiNXhSxFN8LY7dGK5dZDVSpuMKhX4NshlOz0x9aW+MCIYf0BH1EU+CopagSyxwnwShRK00RnKEmSJivwjQY8MTzDykyC1UMFQEMXEU2oktQwKOtJNkFrQsDILmeLfbkI6evlvbL/DmVQwdTxzQK9SnE3MNV5R0nag+ujIk6SChgoFGE8lxIoXQNyYxdSlsk4KrhATu65gp1fwAJQGShLKeAjAv3oRxXXgvuXaiMqnWq+etTbMrofa+zhHMtSqCQADcKkT8WcYsYuaZ4N0nEaevNcARn1KIQNW4cNHEeuIL28Gn3W7VKBSqOFVyI8QhzSZzAX2CLwXlAUouJv3zaPX+IDnDB2rMRpMw/XStMRZfS0JGLYqLVBMGbioSTpzH/Qv+IXHb2egZQbpOQra7KJIKwGpGEiLGD6gm2qAnx7sMAEzpr2RRvdFqFTkhhKI5M+ByC53IFsckgyq7VUYHo2NLFtlXCvSLPUBMolBu2vOgFjtZCeWGaqZ8J8kc6VToy+ZwWN5pr63SwMhxln5Sq1Xk8r2fqP/twwMVmhjRPuq4T6QP5M1K6j5gAZRXxGHNrDaLVLhouxs3vgesFzguukc+7C79hQJ6iK5ENeUQ9bfoEYhoN5NLtkWFYoi1OZUYBcNmUl0Hygr0OLDtuXCqjOcSQo2vQaLRpU149PCQrvhHFpUYLFE3uzmEU6V5RbYPtDQ1spxQiIlWCBGorO2bCWeZRt2SpjF2upM7v0e9z7c2zkVK8NwjOJQ9l1MvDr9fEImJ5OoDpNsstX8EYhQ8uiFsnkmgCgol16AmxquloVzsGr6WehkL7L3OWfh2aV6lyPYqr+Y3WOC1G6NIgj9VhlNbRUL0itWMPv9oeu6WmpaEJGwZehBy0vG2m6rPNcsGiNssUS48KKjLbSfIYCJR8+TIsKTXCq2G+xaU/J3Sc/yQtRVl4VA1g6Ovc5z63+LqmseQKISXnKe9ZOmDSVmE8HWSrc52PUL7zKUp75ShWGzsTYGRmRNhoFp1tKSU9a9cbsKNUKvRJMUomRWQCQFBXdWIFI08ipCzi6D9PwEGALYqGRLiX8lhAAAAAElFTkSuQmCC", null, null, null, new google.maps.Size(120,50)); 

		var marker = new google.maps.Marker({
		  position: myLatLng,
		  map: map,
		  icon:logo
		});
	},//initMap

	initForm:function(){
		var submitBtn = $('#contactForm .submitBtn');
		var name = $('#contactForm .name input');
		var phone = $('#contactForm .phone input');
		var email = $('#contactForm .email input');
		var message = $('#contactForm .message textarea');
		var tf = $('#contactForm .tf');

		submitBtn.off(clickEvent);
		submitBtn.on(clickEvent,function(e){
			verifyTextfiled();
		});

		//verify text filed
		function verifyTextfiled(){
			//reset error
			tf.each(function(e){
				$(this).removeClass('fail');
			});

			//
			if(name.val() ==""){
				var txt = "Vui lòng nhập Họ & Tên";
				if(lang=="en") txt = "Please input your Full name"
				showError(name,txt);
			}

			if(phone.val() ==""){
				var txt = "Vui lòng nhập Điện thoại";
				if(lang=="en") txt = "Please input your number phone"
				showError(phone,txt);
			}else{
				if(phone.val().length < 10){
					var txt = "Số điện thoại không hợp lệ";
					if(lang=="en") txt = "Your number phone is invalid"
					showError(phone,txt);
				}
			}

			if(email.val() ==""){
				var txt = "Vui lòng nhập Email";
				if(lang=="en") txt = "Please input your Email address"
				showError(email,txt);
			}else{
				if(!appLibrary.validateEmail(email.val())){
					var txt = "Email không hợp lệ";
					if(lang=="en") txt = "Your email address is invalid";
					showError(email,txt);
				}
			}

			if(message.val() ==""){
				var txt = "Vui lòng nhập Nội dung";
				if(lang=="en") txt = "Please input your Message"
				showError(message,txt);
			}
		}

		//show error for each 
		function showError(target,txt){
			target.parent().addClass('fail').attr('message',txt);
		}
	},//initForm
}


//project page
var projectPage = {
	init:function(){
		var _this = projectPage;
		if($('#pProject.pDetail').length >=1){
			_this.zoomPhoto($('#detailBox .imageHolder .clo'));

			//animation in
			$('.animtionIn').each(function(e){
				appLibrary.animationIn({ob:$(this),easing:"easeInOutExpo"});
			});
		}else{
			//initBanner
			appLibrary.slideshow($('#slideShow'),$('#slideShow .slideItem'),clickEvent);
			
			//animation in
			appLibrary.animationIn({ob:$('.projectHolder'),easing:"easeInOutExpo"});
			appLibrary.animationIn({ob:$('#slideShow'),easing:"easeInOutExpo"});
			appLibrary.animationIn({ob:$('.numeOfPagesAnimation'),easing:"easeInOutExpo"});
		}

		
		appLibrary.animationIn({ob:$('footer'),easing:"easeInOutExpo"});
	},//init



	//zoom photo
	zoomPhoto:function(target){

		target.each(function(e){
			setUp($(this));
		});

		//setup photo
		function setUp(parent){
			var closeBtn = parent.find('.icon-close');
			var zoomBtn = parent.find('.icon-zoom');

			zoomBtn.off(clickEvent);	
			zoomBtn.on(clickEvent,function(index){
				expand(parent);
			})

			closeBtn.off(clickEvent);
			closeBtn.on(clickEvent,function(index){
				target.css('z-index',0);
				collapse(parent);
			})
		}


		//expading photo
		function expand(ob){
			ob.addClass('expand');
			target.parent().addClass('overlay');
		}

		//collapse photo;
		function collapse(ob){
			ob.removeClass('expand').css('z-index',1);
			target.parent().removeClass('overlay');
		}
	},//zoomPhoto
}

