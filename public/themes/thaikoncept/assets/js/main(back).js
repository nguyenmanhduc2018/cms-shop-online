/**
 *
 * JS by Duc Nguyen
 * 
 */



$(document).ready(function() {
	$('input#searchTxt').keypress(function(event) {
		if ( event.which == 13 ) {
			var key = $(this).val();
			var link = $('link[rel=canonical]').attr('href');
			window.location.href= link + '/search?q=' + key;
		}
	});

	$('button.circleEff.submitBtn').click(function() {
		var name = $('#contactForm .name input');
		var phone = $('#contactForm .phone input');
		var email = $('#contactForm .email input');
		var message = $('#contactForm .message textarea');

		if(message.val() != ''){
			if(message.val().length < 20){
				var txt = "Nội dung tin nhắn tối thiểu 20 ký tự";
				if(lang=="en") txt = "Message content of 20 characters minimum"
				showError(message,txt);
			}
		}
		if(name.val() != '' && phone.val().length >= 10 && email.val() != '' && message.val() != '' && message.val().length >= 20){
			$('#contactForm form').submit();
		}
	});

	function showError(target,txt){
		target.parent().addClass('fail').attr('message',txt);
		return false;
	}
});

var html = [];

var count = $('#detailBox .content img').length;
var i =0;
var text = '<div class="imageHolder animtionIn">';
$('#detailBox .content img').each(function() {
	i++;
	if(i != 1){num = 2}else{num = 1}
	if(i == 2){positon = 'top'}else if(i == 3){positon = 'bottom'}else{ positon = '' }
    var _this = $(this);
    var link  = _this.attr('src');
    text = text + '<div class="clo cl'+num+' to-animate '+positon+' lazyload" data-src="'+link+'" data-direct="fadeInUp" style="background-image: url('+link+');">';
    text = text + '<a href="#" class="icon-zoom"></a>';
    text = text + '<a href="#" class="icon-close"></a>';
    text = text + '</div>';
    _this.remove();
    html.push(text);
    if(i == count){
    	appendContent();
    }
});


function appendContent(){
	$('#detailBox .content p').each(function(){
    	if($(this).text() == ''){
    		$(this).append(html.join('')).css({'margin-bottom':'25px'});
    	}else{
    		$(this).addClass('to-animate').attr('data-direct', 'fadeInUp');
    	}
    });
}   

