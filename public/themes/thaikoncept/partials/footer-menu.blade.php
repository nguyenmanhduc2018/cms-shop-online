@foreach ($menu_nodes as $key => $row)
	@if (!$row->hasChild())
	<a href="{{ $row->getRelated(true)->url }}" target="{{ $row->target }}" class="{{ $row->css_class }}">{{ $row->getRelated(true)->name }}</a>
	@endif
@endforeach  
