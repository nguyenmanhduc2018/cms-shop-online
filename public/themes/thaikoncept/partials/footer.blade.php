		<footer >
			<div class="container">
				<div class="row r1 to-animate" data-direct="fadeInUp">
					<h1 class="title">{{ __('allpage.contact') }}<br/>{{ __('allpage.contact_us') }}</h1>
					<div class="moveTopBtn easing"></div>
				</div>

				<div class="row r2">
					<div class="col cl1 to-animate" data-direct="fadeInUp">
						<div class="address">
							<h6><span>{{ __('allpage.address') }}</span></h6>
							<p>{{ setting('contact_address') }}</p>
						</div>
						<div class="email to-animate" data-direct="fadeInUp">
							<h6><span>Email</span></h6>
							<p><a href="mailto:{{ setting('email_support') }}">{{ setting('email_support') }}</a></p>
						</div>
						<div class="Điện thoại to-animate" data-direct="fadeInUp">
							<h6><span>{{ __('allpage.phone') }}</span></h6>
							<p>{{ setting('contact_phone') }}</p>
						</div>
						<div class="address to-animate" data-direct="fadeInUp">
							<h6><span>{{ __('allpage.follow') }}</span></h6>
							<p>
								<a href="{{ setting('facebook') }}" class="social">Facebook</a>
								<br/>
								<a href="{{ setting('instagram') }}" class="social">Instagram</a>
							</p>
						</div>
					</div>
					<div class="col cl2 to-animate" data-direct="fadeInUp">
						<nav>
							{!!
				                Menu::generateMenu([
				                    'slug' => 'footer-menu',
				                    'view' => 'footer-menu'
				                ])
				            !!}
						</nav>

						<div class="lang" style="text-transform: capitalize;">
							@php
								$__currentLoopData = Language::getSupportedLocales();
								foreach($__currentLoopData as $localeCode => $properties):
							@endphp
								@php 
									$name = $properties['flag'];
									if($name == 'us') $text = 'English';
									if($name == 'vn') $text = 'Vietnamese';
									$code = e($localeCode);
									$link = e(Language::getLocalizedURL($localeCode));
									if($localeCode == Language::getCurrentLocale()): 
								@endphp
									<a rel="alternate" href="javascript:;" class="active">
										<?php echo $text; ?>
									</a>
								@php
									else:
								@endphp
									@php if($name == 'vn'): @endphp
										<a href="javascript:;" class="disable">/</a>
									@php endif; @endphp
									@php if($name != 'vn' && $name != 'us'): @endphp
										<a href="javascript:;" class="disable">/</a>
									@php endif; @endphp
									<a rel="alternate" href="<?php echo $link; ?>" hreflang="<?php echo $code; ?>"><?php echo $text; ?></a>
									@php if($name == 'us'): @endphp
										<a href="javascript:;" class="disable">/</a>
									@php endif; @endphp
								@php		
									endif
								@endphp
							@php
								endforeach;
							@endphp
						</div>
					</div>
				</div>
				
			</div>
		</footer>
		{!! Theme::footer() !!}
	</body>
</html>
