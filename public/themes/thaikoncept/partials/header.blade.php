<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <META NAME="ROBOTS" CONTENT="index">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="Cache-control" content="public">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="target-densitydpi=medium-dpi, width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="icon" href="{{ url(theme_option('favicon', '/themes/thaikoncept/favicon.gif')) }}" type="image/gif"/>
        <meta name="HandheldFriendly" content="true">
    	<meta name="MobileOptimized" content="320">
        <script type="text/javascript">var langFromBackend = '{{ Language::getCurrentLocale() }}';</script>
        {!! Theme::header() !!}
    </head>
	<body>
		<header class="">
			<div class="container">
				<div class="hamburger hamburger--spring-r showMobile">
			        <div class="hamburger-box">
			          <div class="hamburger-inner"></div>
			        </div>
			    </div>
			    @php
			    	$urlRe = Request::url();
			    	$urlRe = str_replace(array('/en', '/vi'), '', $urlRe);
			    @endphp
				<a href="{{ url('/') }}" class="@if(url('/') === $urlRe || url('/index.html') === $urlRe) active @endif logo-thaikoncept"><img src="{{ url(theme_option('logo-header', '/themes/thaikoncept/assets/images/logo_thaikoncept.svg')) }}" alt="Login Thaikoncept" title="Login Thaikoncept"/></a>
				<nav>
    				{!!
		                Menu::generateMenu([
		                    'slug' => 'main-menu',
		                    'options' => ['class' => 'nav navbar-nav'],
		                    'view' => 'main-menu'
		                ])
		            !!}
					<a href="#" class="navItem searchBtn">
						<img src="{{ url(theme_option('search', '/themes/thaikoncept/assets/images/icon_search.svg')) }}" alt="Search Thaikoncept" title="Search Thaikoncept" class="easing icon" />
						<div class="searchBox">
							<input id="searchTxt" class="searchTxt" type="text" name="" placeholder="{{ __('allpage.enter_keywords') }}"></input>
						</div><!-- searchBox -->
					</a>
				</nav>
				<div class="lang" style="text-transform: uppercase;">
					@php
						$__currentLoopData = Language::getSupportedLocales();
						foreach($__currentLoopData as $localeCode => $properties):
					@endphp
						@php 
							$name = $properties['flag'];
							if($name == 'us') $name = 'en';
							$code = e($localeCode);
							$link = e(Language::getLocalizedURL($localeCode));
							if($localeCode == Language::getCurrentLocale()): 
						@endphp
							<a rel="alternate" href="javascript:;" class="active">
								<?php echo $name; ?>
							</a>
						@php
							else:
						@endphp
							@php if($name == 'vn'): @endphp
								<a href="javascript:;" class="disable">/</a>
							@php endif; @endphp
							@php if($name != 'vn' && $name != 'en'): @endphp
								<a href="javascript:;" class="disable">/</a>
							@php endif; @endphp
							<a rel="alternate" href="<?php echo $link; ?>" hreflang="<?php echo $code; ?>"><?php echo $name; ?></a>
							@php if($name == 'en'): @endphp
								<a href="javascript:;" class="disable">/</a>
							@php endif; @endphp
						@php		
							endif
						@endphp
					@php
						endforeach;
					@endphp
				</div>
			</div>
		</header>