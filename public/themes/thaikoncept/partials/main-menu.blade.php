
@foreach ($menu_nodes as $key => $row)
	@if (!$row->hasChild())
		@if($row->css_class === '')
		<li>
			<a class="{{ $row->css_class }} @if ($row->getRelated(true)->url == Request::url()) active @endif" href="{{ $row->getRelated(true)->url }}" target="{{ $row->target }}">
				{{ $row->getRelated(true)->name }}
		    </a>
		</li>
	    @else
	    	<a class="{{ $row->css_class }} @if ($row->getRelated(true)->url == Request::url()) active @endif" href="{{ $row->getRelated(true)->url }}" target="{{ $row->target }}">
				{{ $row->getRelated(true)->name }}
		    </a>
	    @endif
	@else
	<div class="{{ $row->css_class }} hasSubmenu">
		{{ $row->getRelated(true)->name }}
		<ul class="submenu">
			@if ($row->hasChild())
		        {!!
		            Menu::generateMenu([
		                'slug' => $menu->slug,
		                'view' => 'main-menu',
		                'parent_id' => $row->id
		            ])
		        !!}
	    	@endif
		</ul>
    </div>
	@endif
@endforeach  
