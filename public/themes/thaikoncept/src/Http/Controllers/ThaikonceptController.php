<?php

namespace Theme\Thaikoncept\Http\Controllers;

use Illuminate\Routing\Controller;
use Theme;

class ThaikonceptController extends Controller
{

    /**
     * @return \Response
     */
    public function test()
    {
        return Theme::scope('test')->render();
    }
}