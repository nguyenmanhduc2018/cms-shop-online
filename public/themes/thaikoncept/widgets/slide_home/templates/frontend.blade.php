<div id="slideShow" auto-slide>
	<div class="slideshow-inner to-animate" data-direct="fadeInUp">
		@php 
			$listImgSlide = gallery_meta_data(7, 'gallery');
		@endphp
		@foreach($listImgSlide as $item)
		<div class="slideItem lazyload" data-src="{{ get_object_image($item['img']) }}" style="background-image: url({{ get_object_image($item['img'], 'small') }});" title="{{ $item['description'] }}" alt="{{ $item['description'] }}"></div>
		@endforeach
	</div>
	<div class="controller">
		<div class="controlItem next to-animate" data-direct="fadeInLeft"></div>
		<div class="controlItem pre to-animate" data-direct="fadeInRight"></div>
	</div>
</div>