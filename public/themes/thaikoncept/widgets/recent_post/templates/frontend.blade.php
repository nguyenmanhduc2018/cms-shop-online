<div class="projectHolder">	
	@php
		$slugProject = get_category_by_slug('projects');
		if(empty($slugProject)){
			$slugProject = get_category_by_slug('project');
		}
	    $cateID = $slugProject->id;
	    $listProject = get_posts_by_category($cateID, $config['number_display'], $config['number_display']);
	@endphp
	@foreach($listProject as $post)
	<a href="{{ route('public.single.detail', $post->slug) }}" class="boxProject to-animate" data-direct="fadeInUp">
		<div class="core">
			<div class="thumb" style="background-image: url({{ get_object_image($post->image, 'thumb') }});"></div>
			<div class="info">
				<h6>{{ $post->name }}</h6>
				<p class="des">{{ $post->description }}</p>
			</div>
		</div>
	</a><!-- boxProject -->
	@endforeach
</div> <!-- projectHolder -->
@php
	// code by Duc Nguyen
	$link_limit = 4;
	$paginator = $listProject;
	$totalLastPage = $paginator->lastPage();
	$currentPage   = $paginator->currentPage();
@endphp
<div class="numeOfPagesAnimation">
	<div id="numeOfPages" class="to-animate" data-direct="fadeInUp">
		@if ($totalLastPage > 1)
		    <a href="{{ $paginator->url($currentPage - 1) }}" class="{{ ($currentPage == 1) ? 'disable' : '' }} col control pre"><span class="arrow"></span></a>
		    <div class="col list ">
			@for ($i = 1; $i <= $totalLastPage; $i++)
			    @php
			    $half_total_links = floor($link_limit / 2);
			    $from = $currentPage - $half_total_links;
			    $to = $currentPage + $half_total_links;
			    if ($currentPage < $half_total_links) {
			       $to += $half_total_links - $currentPage;
			    }
			    if ($totalLastPage - $currentPage < $half_total_links) {
			        $from -= $half_total_links - ($totalLastPage - $currentPage) - 1;
			    }
			    @endphp
			    @if ($from < $i && $i < $to)
			    	<a href="{{ $paginator->url($i) }}" class="{{ ($currentPage == $i) ? 'active' : '' }}">{{ $i }}</a>
			    @endif
			@endfor
				@if($totalLastPage >= 4)
		    		<a href="#" class="disable">...</a>
		    		<a href="{{ $paginator->url($totalLastPage) }}" class="{{ ($currentPage == $totalLastPage) ? 'disable ' : '' }}">{{ $totalLastPage }}</a>
		    	@endif
			</div>
			<a href="{{ $paginator->url(($currentPage < $totalLastPage) ? $currentPage + 1 : $totalLastPage) }}" class="{{ ($currentPage == $totalLastPage) ? 'disable ' : '' }} col control next"><span class="arrow"></span></a>
			@else
		@endif
	</div><!-- pageWapper -->
</div>