<div class="col cl2 galleryHolder to-animate" data-direct="fadeInUp">
	@php 
		$listImgSlide = gallery_meta_data(8, 'gallery');
	@endphp
	@foreach($listImgSlide as $item)
	<div class="galleryItem lazyload" data-src="{{ get_object_image($item['img']) }}" style="background-image: url({{ get_object_image($item['img'], 'small') }});" title="{{ $item['description'] }}" alt="{{ $item['description'] }}"></div>
	@endforeach
</div>