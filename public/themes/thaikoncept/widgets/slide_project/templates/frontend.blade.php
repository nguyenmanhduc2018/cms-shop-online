@php
	$slugProject = get_category_by_slug('projects');

	if(empty($slugProject)){
		$slugProject = get_category_by_slug('project');
	}
    $cateID = $slugProject->id;
    $listSlide = get_posts_by_category($cateID, $config['number_display'], $config['number_display']);
@endphp
@foreach($listSlide as $post)
<a href="{{ route('public.single.detail', $post->slug) }}" class="slideItem lazyload" data-src="{{ get_object_image($post->image) }}" style="background-image: url({{ get_object_image($post->image, 'small') }});">
	<div class="info">
		<h6 class="title">{{ $post->name }}</h6>
		<p class="des">{{ $post->description }}</p>
	</div>
</a>
@endforeach