@php 
	$listImgSlide = gallery_meta_data(10, 'gallery');
@endphp
@foreach($listImgSlide as $item)
<div class="slideItem lazyload" data-src="{{ get_object_image($item['img']) }}" style="background-image: url({{ get_object_image($item['img'], 'small') }});" title="{{ $item['description'] }}" alt="{{ $item['description'] }}"></div>
@endforeach