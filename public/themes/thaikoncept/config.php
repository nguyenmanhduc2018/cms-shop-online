<?php



return [

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials" and "views"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => [

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
            // You can remove this line anytime.
            $theme->setTitle('Copyright ©  2016 - thaikoncept');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
            // You may use this event to set up your assets.
            // $theme->asset()->usePath()->add('core', 'core.js');
            // $theme->asset()->add('jquery', 'vendor/jquery/jquery.min.js');
            // $theme->asset()->add('jquery-ui', 'vendor/jqueryui/jquery-ui.min.js', array('jquery'));

            // Partial composer.
            // $theme->partialComposer('header', function($view)
            // {
            //     $view->with('auth', acl_get_current_user());
            // });

            $theme->asset()->container('footer')->usePath()->add('jquery-plugin', 'js/plugin.min.js');
            $theme->asset()->container('footer')->usePath()->add('jquery-library', 'js/library.min.js');
            $theme->asset()->container('footer')->usePath()->add('jquery-common', 'js/common.min.js');
            $theme->asset()->container('footer')->usePath()->add('jquery-main', 'js/main.js');

            //$theme->asset()->usePath()->add('library-css', 'css/library.min.css');
            $theme->asset()->usePath()->add('style-css', 'css/style.min.css');
            //$theme->asset()->usePath()->add('responsive-css', 'css/responsive.min.css');

            $theme->composer(['page', 'index', 'default' ,'post'], function($view) {
                $view->withShortcodes();
            });
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => [

            'contact-us' => function($theme)
            {
                $theme->asset()->container('footer')->add('jquery-map', 'https://maps.googleapis.com/maps/api/js?key='.env('GOOGLE_MAP_KEY').'&callback=contactPage.initMap');
            }
        ]
    ]
];
