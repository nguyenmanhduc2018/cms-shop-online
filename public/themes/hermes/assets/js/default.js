$(document).ready(function () {
	if($('.search-main')){
		var query = getParameterByName('query');
		if(query){
			$('.search-block input').val(query);
		}
	}
	function getParameterByName(name) {
    	var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    	return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
	}
	$(document).mouseup(function(e) {
		var container = $(".mobile-main-menu");

		// if the target of the click isn't the container nor a descendant of the container
		if (!container.is(e.target) && container.has(e.target).length === 0) 
		{
			container.hide();
		}
	});
	
	var swiper1 = new Swiper('.swiper-block-1 .swiper-container', {
		slidesPerView: 1,
		slidesPerColumn: 2,
		nextButton: '.swiper-block-1 .custom-next-button',
		prevButton: '.swiper-block-1 .custom-prev-button',
		spaceBetween: 10,
	});
	
	var swiper2 = new Swiper('.swiper-block-2 .swiper-container', {
		slidesPerView: 1,
		slidesPerColumn: 2,
		nextButton: '.swiper-block-2 .custom-next-button',
		prevButton: '.swiper-block-2 .custom-prev-button',
		spaceBetween: 10,
	});
	
	var swiper3 = new Swiper('.swiper-block-3 .swiper-container', {
		slidesPerView: 1,
		slidesPerColumn: 2,
		nextButton: '.swiper-block-3 .custom-next-button',
		prevButton: '.swiper-block-3 .custom-prev-button',
		spaceBetween: 10,
	});
	
	$('.banner .owl-carousel').owlCarousel({
		loop : true,
			nav: true,
			dots : true,
            navText: ["<i class='fa fa-long-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"],
            items:1,
			responsive: {
                0: {
					autoplay:false,
                },
                768: {
					autoplay:false,
                },
                992: {
                    autoplayTimeout:5000,
					autoplay:true,
                }
            }
        });
	
	$('.thumbnail-product').owlCarousel({
			nav: false,
			pagination : false,
			dots: false,
			items : 4,
			margin: 10,
        });
	
	$('.collection-slide .owl-carousel').owlCarousel({
            loop: true,
			nav: true,
            navText: ["<i class='fa fa-long-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"],
			pagination : false,
			dots: false,
            responsive: {
                0: {
                    items: 1, margin : 5
                },
				540 : { items : 2, margin: 20,
				},
                768: {
                    items: 2,
					margin: 20,
                }
            }
        });
	
	$('.daily-deal .owl-carousel').owlCarousel({
            loop: true,
			nav: true,
            navText: ["<i class='fa fa-long-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"],
            margin: 20,
			dots : false,
            responsive: {
                0: {
                    items: 1
                },
				540 : {
					items : 2,
					margin : 10
				},
                768: {
                    items: 2,
					margin: 30,
                },
                992: {
                    items: 2
                }
            }
        });
	
	$('.news-blog .owl-carousel').owlCarousel({
			nav: true,
            navText: ["<i class='fa fa-long-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"],
            margin: 30,
			dots : false,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                992: {
                    items: 3
                }
            }
        });
	
	if($('.details-product')){
		//alert(true);
		$('.details-product .form-product .quantity-left-minus').click(function(){
			var currValue = $(this).closest('.toogle-quantity').find('.qty').val();
			if(currValue > 1){
				currValue--;
			}
			$(this).closest('.toogle-quantity').find('.qty').val(currValue);
			var price = parseInt($('.details-product .form-product').find('.hidPrice').val());
			var totalprice = parseInt(currValue) * price;
			$('.details-product .form-product .totalprice span').html(totalprice.formatMoney(0)+'Ä‘');
		});
		
		$('.details-product .form-product .quantity-right-plus').click(function(){
			var currValue = $(this).closest('.toogle-quantity').find('.qty').val();
				currValue++;
			$(this).closest('.toogle-quantity').find('.qty').val(currValue);
			var price = parseInt($('.details-product .form-product').find('.hidPrice').val());
			var totalprice = parseInt(currValue) * price;
			$('.details-product .form-product .totalprice span').html(totalprice.formatMoney(0)+'Ä‘');
		});
		
		$('.related-product .owl-carousel').owlCarousel({
			nav: false,
			dots : false,
			margin: 10,
            responsive: {
                0: {
                    items: 1,
                },
                768: {
                    items: 2,
                },
				992 : {
					items: 3,
				},
                1200: {
                    items: 4,
                }
            }
        });
	}
	


	
	
	$('.all-product .tab').each(function(){
		$(this).find('.owl-carousel').owlCarousel({
			nav: true,
            navText: ["<i class='fa fa-long-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"],
            margin: 20,
			dots : false,
            responsive: {
                0: {
                    items: 1,
					margin: 20
                },
				540 : {
					items : 2,
					margin : 10
				},
                768: {
                    items: 2
                },
				992: {
                    items: 3
                },
                1200: {
                    items: 4
                }
            }
        });
	});
	
	
	
		
			
	$('.all-product .tab-header a').click(function (e) {
		e.stopPropagation();
		e.preventDefault();
		var clickId = $(this).attr('data-tab');
		$('.all-product .tab-header a').each(function () {
			$(this).removeClass('active');
		});
		$(this).addClass('active');

		$('.all-product .tab-content-product .tab').each(function () {
			$(this).removeClass('active');
		});

		$('.' + clickId).addClass('active');
	});
	$(document).on('scroll', function() {
		if($(this).scrollTop()>=$('.footer').position().top - 500){
			$('#btn-to-top').addClass('show-item');
		}
		else{
			$('#btn-to-top').removeClass('show-item');
		}
	});

	$('#btn-to-top').click(function(e){
		e.preventDefault();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	});
	$('.footer .link-column .fa-minus').each(function(){
		$(this).click(function(){
			$(this).closest('.link-column-item').find('.footer-column-text').addClass('has-line');
			$(this).toggle();
			$(this).closest('.link-column-item').find('.fa-plus').toggle();
			$(this).closest('.link-column-item').find('ul').toggle();
		});
	});
	$('.footer .link-column .fa-plus').each(function(){
		$(this).click(function(){
			$(this).closest('.link-column-item').find('.footer-column-text').removeClass('has-line');
			$(this).toggle();
			$(this).closest('.link-column-item').find('.fa-minus').toggle();
			$(this).closest('.link-column-item').find('ul').toggle();
		});
	});
	$('.main-menu .btn-cate-menu .fa').click(function(e){
		e.preventDefault();
		$('.mobile-main-menu').toggle();
	});
	/*$(function(){
		$('.cart-info .list-cart-item').slimScroll({
			height: '145px'
		});
	});*/
	$('.mega-menu li .item-open').each(function(){
		$(this).click(function(){
			$(this).closest('li').find('> ul').toggle();
			if($(this).hasClass('rotate-90')){
				$(this).removeClass('rotate-90');
			}
			else{
				$(this).addClass('rotate-90');
			}
		});
		
	});
	$('.mobile-main-menu  .has-mega-icon').each(function(){
		$(this).click(function(){
			if($(this).hasClass('rotate-90')){
				$(this).removeClass('rotate-90');
			}
			else{
				$(this).addClass('rotate-90');
			}
			$(this).closest('.has-mega').find('.mega-menu').toggle();
		});
		
	});
});