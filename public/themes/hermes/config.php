<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials" and "views"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => [

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
            // You can remove this line anytime.
            //$theme->setTitle('Copyright ©  2018 - shop.dev');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
            // $theme->asset()->container('footer')->usePath()->add('jquery-plugin', 'js/plugin.min.js');
            // $theme->asset()->container('footer')->usePath()->add('jquery-library', 'js/library.min.js');
            

            // $theme->asset()->usePath()->add('library-css', 'css/library.min.css');

            $theme->composer(['page', 'post'], function($view) {
                $view->withShortcodes();
            });
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => [

            'default' => function($theme)
            {
                // $theme->asset()->usePath()->add('ipad', 'css/layouts/ipad.css');
            },
            'product' => function($theme)
            {
                $theme->asset()->usePath()->add('jquery-ui-css', 'css/jquery-ui.min.css');
                $theme->asset()->container('footer')->usePath()->add('jquery-search', 'js/search_filter.js');
                $theme->asset()->container('footer')->usePath()->add('jquery-ui', 'js/jquery-ui.min.js');
            }
        ]
    ]
];
