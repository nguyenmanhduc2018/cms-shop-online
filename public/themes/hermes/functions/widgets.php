<?php

/*
 * Widget Slide Banner Home
 */

register_sidebar([
    'id' => 'slide_banner_home',
    'name' => __('Home Header Slider')
]);


/*
 * Widget Deal Hot
 */

register_sidebar([
    'id' => 'deal_hot',
    'name' => __('Deal Hot')
]);


require_once __DIR__ . '/../widgets/banner-home/banner-home.php';
register_widget(BannerHomeWidget::class);

require_once __DIR__ . '/../widgets/deal-hot/deal-hot.php';
register_widget(DealHotWidget::class);