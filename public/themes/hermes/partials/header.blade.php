<!DOCTYPE html>
<html lang="vi">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<!-- ================= Meta ================== -->
		<meta name="keywords" content="" />		
		<link rel="canonical" href="./index.html" />
		<meta name='revisit-after' content='1 days' />
		<meta name="robots" content="noodp,index,follow" />
		<!-- ================= Page description ================== -->
		
		<!-- ================= Favicon ================== -->
		
		<link rel="icon" href="{{ url(theme_option('favicon', '/themes/hermes/favicon.png')) }}" type="image/x-icon" />
		
		
		<!-- Plugin CSS -->			
		<link rel="stylesheet" href="{{ asset('themes/hermes') }}/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="{{ asset('themes/hermes') }}/assets/css/font-awesome.min.css" />
		<link href='{{ asset('themes/hermes') }}/assets/css/owl.carousel.min.css' rel='stylesheet' type='text/css' />
		<link href='{{ asset('themes/hermes') }}/assets/css/swiper.min.css' rel='stylesheet' type='text/css' />
		<!-- Build Main CSS -->								
		<link href='{{ asset('themes/hermes') }}/assets/css/base.css' rel='stylesheet' type='text/css' />		
		<link href='{{ asset('themes/hermes') }}/assets/css/style.css' rel='stylesheet' type='text/css' />		
		<link href='{{ asset('themes/hermes') }}/assets/css/update.css' rel='stylesheet' type='text/css' />		
		<link href='{{ asset('themes/hermes') }}/assets/css/module.css' rel='stylesheet' type='text/css' />
		<link href='{{ asset('themes/hermes') }}/assets/css/responsive.css' rel='stylesheet' type='text/css' />
		<link href='{{ asset('themes/hermes') }}/assets/css/owl.theme.default.css' rel='stylesheet' type='text/css' />
		<link href='{{ asset('themes/hermes') }}/assets/css/homepage.css' rel='stylesheet' type='text/css' />
		<link href='{{ asset('themes/hermes') }}/assets/css/bpr-products-module.css' rel='stylesheet' type='text/css' />
		{!! Theme::header() !!}
</head>
	<body>	
		<header class="index">
    <div class="sub-menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 left-col">
                    <p class="hotline">Hotline: <a href="tel:01684966474">01684966474</a>
                    </p>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7 right-col">
                    <div class="right-corner">
                        <ul class="social-icon hidden-xs">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                        <div class="login-and-register">
                            <a class="" href="./account/register.html">Đăng ký</a> / &nbsp;
                            <a class="" href="./account/login.html">Đăng nhập</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-menu">
        <div class="container">
            <div class="row">
                <div class="col-md-2 logo-col col-sm-5 col-xs-5">
                    <a href="{{ url('/') }}" class="logo"><img src="{{ url(theme_option('logo', '/themes/hermes/assets/images/logo.png')) }}" alt="Hermes" />
                    </a>
                </div>
                <div class="col-md-8 menu-mobile-col col-sm-5 col-xs-2">
                    <ul class="hidden-sm hidden-md hidden-xs">
                    	{!!
			                Menu::generateMenu([
			                    'slug' => 'main-menu',
			                    'options' => ['class' => 'nav navbar-nav'],
			                    'view' => 'main-menu'
			                ])
			            !!}
					</ul>
                    <div class="display-menu visible-sm visible-xs visible-md">
                        <span class="btn-cate-menu"><i class="fa fa-bars" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="col-md-2 cart-col col-sm-2 col-xs-5">

                    <ul class="right-menu">
                        <li class="search">
                            <i class="fa fa-search hidden-md hidden-sm hidden-xs" aria-hidden="true"></i>
                            <a href="javascript:;" class="visible-md visible-sm visible-xs search-icon"><i class="fa fa-search" aria-hidden="true"></i></a>
                            <div class="search-box hidden-md hidden-sm hidden-xs">
                                <div class="search-content">
                                    <form action="{{ route('public.search') }}" method="get" />
                                    <input type="text" name="q" placeholder="Tìm kiếm" autocomplete="off" />
                                    <button><i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                    </form>
                                </div>
                            </div>
                        </li>
                        <li class="cart">
                            <a href="./cart.html" class="go-cart-page">
                                <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                                <span class="item-number">0</span>
                            </a>
                            <div class="cart-info hidden-sm hidden-xs hidden-md">

                                <p class="no-item">Không có sản phẩm nào trong giỏ hàng</p>

                            </div>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
        <ul class="hidden-lg mobile-main-menu">
            <li>
                <a href="./index.html" class="link-menu">Trang chủ</a>
            </li>

            <li>
                <a href="./gioi-thieu.html" class="link-menu">Giới thiệu</a>
            </li>
            <li class="has-mega">
                <a href="./collections/all.html" class="link-menu">Sản phẩm</a>

                <span class="has-mega-icon"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                <div class="mega-menu">
                    <div class="row">

                        <ul class="ul-mega">

                            <li class="link2">
                                <a href="./nam.html">Đồ nam</a>

                                <span class="item-open"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                <ul class="sub-ul">


                                    <li class="link3"><a href="./ao-khoac-nam.html">Áo khoác nam</a>
                                    </li>



                                    <li class="link3"><a href="./ao-len-nam.html">Áo len nam</a>
                                    </li>



                                    <li class="link3"><a href="./ao-cotton.html">Áo Cotton</a>
                                    </li>



                                    <li class="link3"><a href="./phu-kien-nam.html">Phụ kiện nam</a>
                                    </li>


                                </ul>

                            </li>

                            <li class="link2">
                                <a href="./phu-kien.html">Phụ kiện</a>

                                <span class="item-open"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                <ul class="sub-ul">


                                    <li class="link3"><a href="./phu-kien-nam.html">Phụ kiện Nam</a>
                                    </li>



                                    <li class="link3"><a href="./phu-kien-nu.html">Phụ kiện Nữ</a>
                                    </li>


                                </ul>

                            </li>

                            <li class="link2">
                                <a href="./nu.html">Đồ nữ</a>

                                <span class="item-open"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                <ul class="sub-ul">


                                    <li class="link3"><a href="./quan-jeans.html">Quần Jeans</a>
                                    </li>



                                    <li class="link3"><a href="./chan-vay.html">Chân váy</a>
                                    </li>



                                    <li class="link3"><a href="./phu-kien-nu.html">Phụ kiện nữ</a>
                                    </li>


                                </ul>

                            </li>

                            <li class="link2">
                                <a href="./nam.html">Đồ nam</a>

                                <span class="item-open"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                <ul class="sub-ul">


                                    <li class="link3"><a href="./ao-khoac-nam.html">Áo khoác nam</a>
                                    </li>



                                    <li class="link3"><a href="./ao-len-nam.html">Áo len nam</a>
                                    </li>



                                    <li class="link3"><a href="./ao-cotton.html">Áo Cotton</a>
                                    </li>



                                    <li class="link3"><a href="./phu-kien-nam.html">Phụ kiện nam</a>
                                    </li>


                                </ul>

                            </li>

                            <li class="link2">
                                <a href="./nam.html">Đồ nam</a>

                                <span class="item-open"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                <ul class="sub-ul">
                                    <li class="link3"><a href="./ao-khoac-nam.html">Áo khoác nam</a>
                                    </li>
                                    <li class="link3"><a href="./ao-len-nam.html">Áo len nam</a>
                                    </li>
                                    <li class="link3"><a href="./ao-cotton.html">Áo Cotton</a>
                                    </li>
                                    <li class="link3"><a href="./phu-kien-nam.html">Phụ kiện nam</a>
                                    </li>
                                </ul>

                            </li>

                        </ul>

                    </div>
                </div>

            </li>
            <li>
                <a href="./lien-he.html" class="link-menu">Liên hệ</a>
            </li>
            <li>
                <a href="./thoi-trang-nam.html" class="link-menu">Tin tức</a>
            </li>
        </ul>
    </div>
</header>
<h1 class="hidden">Hermes - </h1>