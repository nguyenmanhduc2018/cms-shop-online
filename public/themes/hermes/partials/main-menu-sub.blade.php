@foreach ($menu_nodes as $key => $row)
		<li class="link2">
			<a href="{{ $row->getRelated(true)->url }}" class="line-bottom">{{ $row->getRelated(true)->name }}</a>
			@if($row->hasChild())
				{!!
		            Menu::generateMenu([
		                'slug' => $menu->slug,
		                'view' => 'main-menu-sub-2',
		                'parent_id' => $row->id
		            ])
		        !!}
			@endif
		</li>
@endforeach