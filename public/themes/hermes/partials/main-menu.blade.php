@foreach ($menu_nodes as $key => $row)
	@if (!$row->hasChild())
    	<li>
	    	<a class="link-menu" href="{{ $row->getRelated(true)->url }}" target="{{ $row->target }}">
				{{ $row->getRelated(true)->name }}
		    </a>
	    </li>
	@else
	<li class="has-mega">
		<a class="link-menu" href="{{ $row->getRelated(true)->url }}" target="{{ $row->target }}">
		{{ $row->getRelated(true)->name }}
		</a>
		<div class="mega-menu">
			<div class="row">
				<div class="menu-links">
					<ul>
						@if ($row->hasChild())
					        {!!
					            Menu::generateMenu([
					                'slug' => $menu->slug,
					                'view' => 'main-menu-sub',
					                'parent_id' => $row->id
					            ])
					        !!}
				    	@endif
					</ul>
				</div>
			</div>
		</div>
    </li>
	@endif
@endforeach  
