@foreach ($menu_nodes as $key => $row)
<li class="nav-item">
	<a class="nav-link" href="{{ $row->getRelated(true)->url }}">{{ $row->getRelated(true)->name }}</a>
</li>
@endforeach