<div class="customer-reviews">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <h3 class="title first-col-title">Khách hàng nói gì</h3>
                <div class="customer-image">
						<img src="{{ asset('themes/hermes') }}/assets/images/section_review_customercol_image.png" alt="customer-image" />
                </div>
                <div class="customer-info">
                    <p class="name">Pixel Team</p>
                    <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;16 November 2017</p>
                </div>
                <span class="comment">" Là một nhân viên văn phòng ưa thích thời trang tôi rất hài lòng về sản phẩm từ chất lượng cho tới kiểu dáng, bắt kịp xu hướng và mức giá hấp dẫn "</span>
            </div>
			
			
			
			
			
			
				
			
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="swiper-block swiper-block-1">
					<h3 class="title">Giảm giá</h3>
					<div class="swiper-container">
						<div class="swiper-wrapper">
							
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/13_1e1a21f.jpg" alt="Vest Cotton Blazer" />
											<div class="right-col">
												<p class="product-name"><a href="./bouble-cotton-blazer.html">Vest Cotton Blazer</a></p>
												
												
													
														<span class="price">1.200.000₫</span>
												<form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147581" enctype="multipart/form-data" />
													<input type="hidden" name="variantId" value="13235604" />
													<button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
												</form>
													
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							 
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/24_69a359ed.jpg" alt="Áo thun cố tròn" />
											<div class="right-col">
												<p class="product-name"><a href="./gillet.html">Áo thun cố tròn</a></p>
												
												
													<span class="price">Hết hàng</span>
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							 
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/17_4e00d45d.jpg" alt="Hoodie Nam bo ống" />
											<div class="right-col">
												<p class="product-name"><a href="./hoodie-nam-bo-ong.html">Hoodie Nam bo ống</a></p>
												
												
													<span class="price">Hết hàng</span>
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/22_96e89cf7.jpg" alt="Áo thun dài tay" />
											<div class="right-col">
												<p class="product-name"><a href="./ao-thun-dai-tay.html">Áo thun dài tay</a></p>
												
												
													<span class="price">Hết hàng</span>
												
											</div>
										</div>
									</div>
								</div>
							
						</div>
					</div>
					<div class="activity-icon custom-prev-button"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                    <div class="activity-icon custom-next-button"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
				</div>
			</div>
			
			
			
			
			
			
			
				
			
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="swiper-block swiper-block-2">
					<h3 class="title">Áo Cotton</h3>
					<div class="swiper-container">
						<div class="swiper-wrapper">
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/1a_219c2873.jpg" alt="Áo len cổ tròn" />
											<div class="right-col">
												<p class="product-name"><a href="./ao-len-co-tron.html">Áo len cổ tròn</a></p>
												
												
													<span class="price">Hết hàng</span>
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/13_1e1a21f.jpg" alt="Vest Cotton Blazer" />
											<div class="right-col">
												<p class="product-name"><a href="./bouble-cotton-blazer.html">Vest Cotton Blazer</a></p>
												
												
													
														<span class="price">1.200.000₫</span>
												<form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147581" enctype="multipart/form-data" />
													<input type="hidden" name="variantId" value="13235604" />
													<button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
												</form>
													
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/11_6ad79329.jpg" alt="Áo khoác lông vũ" />
											<div class="right-col">
												<p class="product-name"><a href="./winter-coat.html">Áo khoác lông vũ</a></p>
												
												
													<span class="price">Hết hàng</span>
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/14_432e5336.jpg" alt="Áo len nam body" />
											<div class="right-col">
												<p class="product-name"><a href="./sweeter-black.html">Áo len nam body</a></p>
												
												
													
														<span class="price">180.000₫</span>
												<form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147578" enctype="multipart/form-data" />
													<input type="hidden" name="variantId" value="13235658" />
													<button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
												</form>
													
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/6_63a1ee93.jpg" alt="Áo len khóa kéo" />
											<div class="right-col">
												<p class="product-name"><a href="./new-blazer.html">Áo len khóa kéo</a></p>
												
												
													
														<span class="price">Liên hệ</span>
													
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							 
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/24_69a359ed.jpg" alt="Áo thun cố tròn" />
											<div class="right-col">
												<p class="product-name"><a href="./gillet.html">Áo thun cố tròn</a></p>
												
												
													<span class="price">Hết hàng</span>
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/27_6ad79f8f.jpg" alt="Măng tô nam 2 lớp" />
											<div class="right-col">
												<p class="product-name"><a href="./mang-to-nam-2-lop.html">Măng tô nam 2 lớp</a></p>
												
												
													
														<span class="price">2.100.000₫</span>
												<form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147575" enctype="multipart/form-data" />
													<input type="hidden" name="variantId" value="13235640" />
													<button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
												</form>
													
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							 
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/17_4e00d45d.jpg" alt="Hoodie Nam bo ống" />
											<div class="right-col">
												<p class="product-name"><a href="./hoodie-nam-bo-ong.html">Hoodie Nam bo ống</a></p>
												
												
													<span class="price">Hết hàng</span>
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							
							 
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/12_fddf659b.jpg" alt="Áo khoác gió 2 lớp" />
											<div class="right-col">
												<p class="product-name"><a href="./ao-khoac-gio-2-lop.html">Áo khoác gió 2 lớp</a></p>
												
												
													
														<span class="price">495.000₫</span>
												<form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147573" enctype="multipart/form-data" />
													<input type="hidden" name="variantId" value="12944574" />
													<button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
												</form>
													
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/20_5c6c3740.jpg" alt="Áo len họa tiết" />
											<div class="right-col">
												<p class="product-name"><a href="./ao-len-hoa-tiet.html">Áo len họa tiết</a></p>
												
												
													
														<span class="price">325.000₫</span>
												<form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147572" enctype="multipart/form-data" />
													<input type="hidden" name="variantId" value="12944573" />
													<button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
												</form>
													
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/22_96e89cf7.jpg" alt="Áo thun dài tay" />
											<div class="right-col">
												<p class="product-name"><a href="./ao-thun-dai-tay.html">Áo thun dài tay</a></p>
												
												
													<span class="price">Hết hàng</span>
												
											</div>
										</div>
									</div>
								</div>
							
						</div>
					</div>
					<div class="activity-icon custom-prev-button"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                    <div class="activity-icon custom-next-button"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
				</div>
			</div>
			
			
			
			
			
			
			
				
			
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="swiper-block swiper-block-3">
					<h3 class="title">Áo len</h3>
					<div class="swiper-container">
						<div class="swiper-wrapper">
							
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/13_1e1a21f.jpg" alt="Vest Cotton Blazer" />
											<div class="right-col">
												<p class="product-name"><a href="./bouble-cotton-blazer.html">Vest Cotton Blazer</a></p>
												
												
													
														<span class="price">1.200.000₫</span>
												<form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147581" enctype="multipart/form-data" />
													<input type="hidden" name="variantId" value="13235604" />
													<button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
												</form>
													
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/11_6ad79329.jpg" alt="Áo khoác lông vũ" />
											<div class="right-col">
												<p class="product-name"><a href="./winter-coat.html">Áo khoác lông vũ</a></p>
												
												
													<span class="price">Hết hàng</span>
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							
							 
							
							
							
							
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/27_6ad79f8f.jpg" alt="Măng tô nam 2 lớp" />
											<div class="right-col">
												<p class="product-name"><a href="./mang-to-nam-2-lop.html">Măng tô nam 2 lớp</a></p>
												
												
													
														<span class="price">2.100.000₫</span>
												<form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147575" enctype="multipart/form-data" />
													<input type="hidden" name="variantId" value="13235640" />
													<button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
												</form>
													
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							 
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/17_4e00d45d.jpg" alt="Hoodie Nam bo ống" />
											<div class="right-col">
												<p class="product-name"><a href="./hoodie-nam-bo-ong.html">Hoodie Nam bo ống</a></p>
												
												
													<span class="price">Hết hàng</span>
												
											</div>
										</div>
									</div>
								</div>
							
							
							
							
							
							
							 
							
							
							
							
							
							
							
								<div class="swiper-slide">
									<div class="item">
										<div class="item-content">
											<img class="product-image" src="{{ asset('themes/hermes') }}/assets/images/products/12_fddf659b.jpg" alt="Áo khoác gió 2 lớp" />
											<div class="right-col">
												<p class="product-name"><a href="./ao-khoac-gio-2-lop.html">Áo khoác gió 2 lớp</a></p>
												
												
													
														<span class="price">495.000₫</span>
												<form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147573" enctype="multipart/form-data" />
													<input type="hidden" name="variantId" value="12944574" />
													<button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
												</form>
													
												
											</div>
										</div>
									</div>
								</div>
							
						</div>
					</div>
					<div class="activity-icon custom-prev-button"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                    <div class="activity-icon custom-next-button"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
				</div>
			</div>
			
			
        </div>
    </div>
</div>

<!-- Footer -->
<footer class="footer">
    <div class="container top-container">
        <div class="row">
            <div class="col-md-4 col-sm-12 info-company">
				
				<a href="{{ url('/') }}" class="logo"><img src="{{ asset('themes/hermes') }}/assets/images/logo.png" alt="logo" /></a>
				
                <p class="sub-title">Hermes Fashion - Thời trang công sở đem lại sự tự tin, trẻ trung, phong cách cho mỗi ngày</p>
                <p class="phone"><i class="fa fa-phone" aria-hidden="true"></i> 
					
					<a href="tel:01684966474">01684966474</a>
					
				</p>
				<p class="mail"><i class="fa fa-envelope-o" aria-hidden="true"></i> 
					
					<a href="mailto:manhducit93@gmail.com">manhducit93@gmail.com</a>
					
				</p>
                <p class="clock"><i class="fa fa-clock-o" aria-hidden="true"></i>8:00 - 19:00, Thứ hai - Thứ Bảy , Chủ nhật - Nghỉ </p>
				<p class="address"><i class="fa fa-map-marker " aria-hidden="true"></i>
					
					Ninh Hòa, Nha Trang, Khánh Hòa
					
				</p>
            </div>
            <div class="col-md-8 col-sm-12 link-column">
                <div class="row">
					
							
							<div class="col-md-4 col-sm-12 link-column-item">
								<h4 class="footer-column-text">Thông tin<i class="fa fa-minus hidden-md hidden-lg" aria-hidden="true"></i><i class="fa fa-plus hidden-md hidden-lg" aria-hidden="true"></i></h4>
								<ul>
									
										<li><a href="./lien-he.html">Về chúng tôi</a></li>
									
										<li><a href="./index.html">Điều kiện sử dụng</a></li>
									
										<li><a href="./index.html">Giao hàng và hoàn trả</a></li>
									
										<li><a href="./index.html">Thông tin chi tiết</a></li>
									
										<li><a href="./index.html">Link tùy chọn</a></li>
									
										<li><a href="./index.html">Link tùy chọn</a></li>
									
								</ul>
							</div>
							
							
							<div class="col-md-4 col-sm-12 link-column-item">
								<h4 class="footer-column-text">Dịch vụ<i class="fa fa-minus hidden-md hidden-lg" aria-hidden="true"></i><i class="fa fa-plus hidden-md hidden-lg" aria-hidden="true"></i></h4>
								<ul>
									
										<li><a href="./index.html">Câu hỏi thường gặp</a></li>
									
										<li><a href="./index.html">Điều khoản sử dụng</a></li>
									
										<li><a href="./index.html">RSS</a></li>
									
										<li><a href="./index.html">Trung tâm hỗ trợ</a></li>
									
										<li><a href="./collections/all.html">Tất cả sản phẩm</a></li>
									
										<li><a href="./collections.html">Danh mục</a></li>
									
								</ul>
							</div>
							

							<div class="col-md-4 col-sm-12 link-column-item">
								<h4 class="footer-column-text">Tài khoản của bạn<i class="fa fa-minus hidden-md hidden-lg" aria-hidden="true"></i><i class="fa fa-plus hidden-md hidden-lg" aria-hidden="true"></i></h4>
								<ul>
									
										<li><a href="./index.html">Đơn hàng</a></li>
									
										<li><a href="./index.html">Yêu thích</a></li>
									
										<li><a href="./index.html">Đăng nhập</a></li>
									
										<li><a href="./index.html">Đăng xuất</a></li>
									
										<li><a href="./index.html">Tìm kiếm</a></li>
									
										<li><a href="./index.html">Hỗ trợ</a></li>
									
								</ul>
							</div>
							
						
						
					
					
                </div>
            </div>
        </div>
        <div class="copyright-mark row">
			<span class="copyright">Copyright © 2017 by <b>NPA Team</b> <br class="visible-xs hidden-sm hidden-md hidden-lg" />Cung cấp bởi <a href="http://nguyenmanhduc.esy.es" rel="nofollow">Đức Nguyễn</a></span>
        </div>
    </div>
    <hr class="line" />
    <div class="last-footer">
        <div class="container">
            <div class="row">
				
				
				
                <span class="send-mail">Gửi email</span>
				<form action="#" method="post" id="mc-embedded-subscribe-form" class="subscribe-form" name="mc-embedded-subscribe-form" target="_blank" />
					<input type="text" placeholder="Email của bạn" name="EMAIL" id="mail" value="" aria-label="" />
					<button class="general-button subscribe" name="subscribe">Gửi ngay</button>
				</form>
				<ul>
                    <li><a href="./index.html"><img src="{{ asset('themes/hermes') }}/assets/images/paypal.png" alt="paypal" /></a></li>
                    <li><a href="./index.html"><img src="{{ asset('themes/hermes') }}/assets/images/visa.png" alt="visa" /></a></li>
                    <li><a href="./index.html"><img src="{{ asset('themes/hermes') }}/assets/images/cirrus.png" alt="cirrus" /></a></li>
                    <li><a href="./index.html"><img src="{{ asset('themes/hermes') }}/assets/images/discover.png" alt="discover" /></a></li>
                    <li><a href="./index.html"><img src="{{ asset('themes/hermes') }}/assets/images/ebay.png" alt="ebay" /></a></li>
                </ul>
            </div>
        </div>
    </div>
<a href="#" id="btn-to-top" class=""><i class="fa fa-long-arrow-up" aria-hidden="true"></i></a>
</footer>
		<script src='{{ asset('themes/hermes') }}/assets/js/jquery-2.2.3.min.js' type='text/javascript'></script> 	
		{!! Theme::footer() !!}
		<script src='{{ asset('themes/hermes') }}/assets/js/cs.script.js' type='text/javascript'></script>				
		
		<!-- Main JS -->	
		<script src='{{ asset('themes/hermes') }}/assets/js/main.js' type='text/javascript'></script>
		<script src='{{ asset('themes/hermes') }}/assets/js/jquery.slimscroll.min.js' type='text/javascript'></script>
		<!-- Product detail JS,CSS -->
		
		<script src='{{ asset('themes/hermes') }}/assets/js/owl.carousel.min.js' type='text/javascript'></script>	
		<script src='{{ asset('themes/hermes') }}/assets/js/swiper.min.js' type='text/javascript'></script>
		<script src='{{ asset('themes/hermes') }}/assets/js/default.js' type='text/javascript'></script>
		<script src='{{ asset('themes/hermes') }}/assets/js/hermes.js' type='text/javascript'></script>
		
	</body>
</html>