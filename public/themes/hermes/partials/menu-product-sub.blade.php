@foreach ($menu_nodes as $key => $row)
	<li class="nav-item">
		<a class="nav-link" href="{{ $row->getRelated(true)->url }}">{{ $row->getRelated(true)->name }}</a>
		@if($row->hasChild())
			<span class="icon">
				<span class="plus"><i class="fa fa-plus" aria-hidden="true"></i></span>
				<span class="minus"><i class="fa fa-minus" aria-hidden="true"></i></span>
			</span>
			<ul class="sub-menu-2 sub-menu">
				{!!
	            	Menu::generateMenu([
	                    'slug' => $menu->slug,
	                    'view' => 'menu-product-sub-2',
	                    'parent_id' => $row->id
	                ]);
	            !!}
			</ul>
		@endif
	</li>
@endforeach
