<ul class="sub-ul">
	@foreach ($menu_nodes as $key => $row)
	<li class="link3">
		<a href="{{ $row->getRelated(true)->url }}">{{ $row->getRelated(true)->name }}</a>
	</li>
	@endforeach
</ul>