<div class="daily-deal">
        <div class="container">
            <div class="col-lg-6 col-md-7 col-md-push-5 col-lg-push-6 daily-deal-content">
                <h2 class="title">Deal nổi bật</h2>
                <span class="sub-title">Giảm giá <b>35%</b> trên tất cả sản phẩm duy nhất hôm nay</span>
                <ul class="time-countdown">
                    <li><span><b class="days">365</b><br />Ngày</span></li>
                    <li><span><b class="hours">24</b><br />Giờ</span></li>
                    <li><span><b class="minutes">59</b><br />Phút</span></li>
                    <li><span><b class="seconds">59</b><br />Giây</span></li>
                </ul>
                <div class="swiper-block product-swiper">
                    <div class="owl-carousel owl-theme">
						

<div class="item">
	<div class="product-detail index">
		<div class="image-detail">
			<a href="./bouble-cotton-blazer.html" title="Vest Cotton Blazer">
				<img src="{{ asset('themes/hermes') }}/assets/images/products/full/13_1e1a21f.jpg" alt="Vest Cotton Blazer" />	
			</a>
		</div>
		
			<span class="sale-tag tag">Sale</span>
		
		<h3><a class="product-name" href="./bouble-cotton-blazer.html">Vest Cotton Blazer</a></h3>
		
			
				<span class="price">1.200.000₫</span> <span class="old-price">1.500.000₫</span>
				<hr />
			<form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147581" enctype="multipart/form-data" />
				<!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
				<input type="hidden" name="variantId" value="13235604" />
				<button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
				<div class="DucN-product-reviews-badge" data-id="8147581"></div>
			</form>
			
		
	</div>
</div>	

<div class="item">
	<div class="product-detail index">
		<div class="image-detail">
			<a href="./winter-coat.html" title="Áo khoác lông vũ">
				<img src="{{ asset('themes/hermes') }}/assets/images/products/full/11_6ad79329.jpg" alt="Áo khoác lông vũ" />	
			</a>
		</div>
		
			<span class="sale-tag tag">Sale</span>
		<h3><a class="product-name" href="./winter-coat.html">Áo khoác lông vũ</a></h3>
		
			<span class="price">Hết hàng</span>
			<hr />
		
	</div>
</div>	

<div class="item">
	<div class="product-detail index">
		<div class="image-detail">
			<a href="./mang-to-nam-2-lop.html" title="Măng tô nam 2 lớp">
				<img src="{{ asset('themes/hermes') }}/assets/images/products/full/27_6ad79f8f.jpg" alt="Măng tô nam 2 lớp" />	
			</a>
		</div>
		
			<span class="sale-tag tag">Sale</span>
		<h3><a class="product-name" href="./mang-to-nam-2-lop.html">Măng tô nam 2 lớp</a></h3>
		
			
				<span class="price">2.100.000₫</span> <span class="old-price">2.500.000₫</span>
				<hr />
			<form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147575" enctype="multipart/form-data" />
				<!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
				<input type="hidden" name="variantId" value="13235640" />
				<button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
				<div class="DucN-product-reviews-badge" data-id="8147575"></div>
			</form>
			
		
	</div>
</div>	

<div class="item">
	<div class="product-detail index">
		<div class="image-detail">
			<a href="./ao-thun-dai-tay.html" title="Áo thun dài tay">
				<img src="{{ asset('themes/hermes') }}/assets/images/products/full/22_96e89cf7.jpg" alt="Áo thun dài tay" />	
			</a>
		</div>
		
			<span class="sale-tag tag">Sale</span>
		<h3><a class="product-name" href="./ao-thun-dai-tay.html">Áo thun dài tay</a></h3>
		
			<span class="price">Hết hàng</span>
			<hr />
		
	</div>
</div>	
					</div>
                </div>
            </div>
        </div>
</div>