<div class="banner">
        <div class="sliders">
			<div class="owl-carousel owl-theme">
						<div class="item">
							<div class="slide-content">
								<a href="./nam.html">
									<img src="{{ asset('themes/hermes') }}/assets/images/section_slider_1_image.png" alt="nam" data-max-width="1920" />
								</a>
							</div>
						</div>
						<div class="item">
							<div class="slide-content">
								<a href="./nu.html">
									<img src="{{ asset('themes/hermes') }}/assets/images/section_slider_2_image.png" alt="Nữ" data-max-width="1920" />
								</a>
							</div>
						</div>
						<div class="item">
							<div class="slide-content">
								<a href="./phu-kien.html">
									<img src="{{ asset('themes/hermes') }}/assets/images/section_slider_3_image.png" alt="phụ kiện" data-max-width="1920" />
								</a>
							</div>
						</div>
			</div>
        </div>
        <div id="clear" style="clear:both;"></div>
    </div>