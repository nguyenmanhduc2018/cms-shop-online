<?php

namespace Theme\Hermes\Http\Controllers;

use Illuminate\Routing\Controller;
use Theme;

class HermesController extends Controller
{

    /**
     * @return \Response
     */
    public function test()
    {
        return Theme::scope('test')->render();
    }
}