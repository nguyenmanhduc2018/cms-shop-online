<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="anyflexbox boxshadow display-table">
<head>
	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Hermes - Thanh toán đơn hàng" />

    <title>Hermes - Thanh toán đơn hàng</title>
    
    <link rel="shortcut icon" href="{{ url(theme_option('favicon', '/themes/hermes/favicon.png')) }}" type="image/x-icon" type='text/css'/>
    <link rel="stylesheet" href="{{ asset('themes/hermes') }}/assets/css/bootstrap.min.css" type='text/css'/>
	<link rel="stylesheet" href="{{ asset('themes/hermes') }}/assets/css/font-awesome.min.css" type='text/css'/>
	<link rel='stylesheet'  href='{{ asset('themes/hermes') }}/assets/css/checkout.css' type='text/css' />


</head>
<body class="body--custom-background-color ">
    <div class="banner" data-header="">
        <div class="wrap">
            <div class="shop logo logo--left ">
		        <h1 class="shop__name">
		            <a href="{{ url('/') }}">Hermes</a>
		        </h1>
			</div>
    	</div>
    </div>
    {!! Theme::content() !!}
</body>
</html>