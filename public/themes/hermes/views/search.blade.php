<section class="bread-crumb">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home">
						<a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>						
						<span><i class="fa fa-angle-right"></i></span>
					</li>
					<li><strong>Kết quả tìm kiếm với từ khóa "{{ Request::get('q') }}"</strong></li>
					
				</ul>
			</div>
		</div>
	</div>
</section> 
<section class="signup search-main">
    <div class="container">
        <h1 class="hidden">Tìm kiếm - Hermes</h1>
        <div class="row search-block">
            <form action="{{ route('public.search') }}" method="get" />
            <div class="col-md-9">
                <input type="text" name="q" placeholder="Tìm kiếm" autocomplete="off" />
            </div>
            <div class="col-md-3">
                <button class="general-button"><i class="fa fa-search" aria-hidden="true"></i> Tìm kiếm</button>
            </div>
            </form>
        </div>
        <div class="row">


            <div class="col-xs-12">
                <h2 class="title-head">Có 18 kết quả tìm kiếm phù hợp</h2>

            </div>

            <div class="products-view-grid products product-swiper">
                <div class="row-gutter-14">

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./ao-len-co-tron.html" title="Áo len cổ tròn">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/1a_219c2873.jpg" alt="Áo len cổ tròn" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>

                                <a href="./ao-len-co-tron.html" data-handle="ao-len-co-tron" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./ao-len-co-tron.html">Áo len cổ tròn</a></h3>

                                <span class="price">Hết hàng</span>
                                <hr />

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./bouble-cotton-blazer.html" title="Vest Cotton Blazer">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/13_1e1a21f.jpg" alt="Vest Cotton Blazer" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>

                                <a href="./bouble-cotton-blazer.html" data-handle="bouble-cotton-blazer" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./bouble-cotton-blazer.html">Vest Cotton Blazer</a></h3>


                                <span class="price">1.200.000₫</span> <span class="old-price">1.500.000₫</span>
                                <hr />
                                <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147581" enctype="multipart/form-data" />
                                <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                <input type="hidden" name="variantId" value="13235604" />
                                <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                <div class="bizweb-product-reviews-badge" data-id="8147581"></div>
                                </form>


                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./winter-coat.html" title="Áo khoác lông vũ">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/11_f57565f9.jpg" alt="Áo khoác lông vũ" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>

                                <a href="./winter-coat.html" data-handle="winter-coat" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./winter-coat.html">Áo khoác lông vũ</a></h3>

                                <span class="price">Hết hàng</span>
                                <hr />

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./sweeter-black.html" title="Áo len nam body">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/14_432e5336.jpg" alt="Áo len nam body" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>

                                <a href="./sweeter-black.html" data-handle="sweeter-black" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./sweeter-black.html">Áo len nam body</a></h3>


                                <span class="price">180.000₫</span> <span class="old-price">200.000₫</span>
                                <hr />
                                <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147578" enctype="multipart/form-data" />
                                <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                <input type="hidden" name="variantId" value="13235658" />
                                <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                <div class="bizweb-product-reviews-badge" data-id="8147578"></div>
                                </form>


                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./new-blazer.html" title="Áo len khóa kéo">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/6_63a1ee93.jpg" alt="Áo len khóa kéo" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>

                                <a href="./new-blazer.html" data-handle="new-blazer" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./new-blazer.html">Áo len khóa kéo</a></h3>


                                <span class="price">Liên hệ</span>
                                <hr />


                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./gillet.html" title="Áo thun cố tròn">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/24_69a359ed.jpg" alt="Áo thun cố tròn" />
                                    </a>
                                </div>

                                <a href="./gillet.html" data-handle="gillet" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./gillet.html">Áo thun cố tròn</a></h3>

                                <span class="price">Hết hàng</span>
                                <hr />

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./mang-to-nam-2-lop.html" title="Măng tô nam 2 lớp">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/27_6ad79f8f.jpg" alt="Măng tô nam 2 lớp" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>

                                <a href="./mang-to-nam-2-lop.html" data-handle="mang-to-nam-2-lop" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./mang-to-nam-2-lop.html">Măng tô nam 2 lớp</a></h3>


                                <span class="price">2.100.000₫</span> <span class="old-price">2.500.000₫</span>
                                <hr />
                                <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147575" enctype="multipart/form-data" />
                                <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                <input type="hidden" name="variantId" value="13235640" />
                                <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                <div class="bizweb-product-reviews-badge" data-id="8147575"></div>
                                </form>


                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./hoodie-nam-bo-ong.html" title="Hoodie Nam bo ống">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/17_4e00d45d.jpg" alt="Hoodie Nam bo ống" />
                                    </a>
                                </div>

                                <a href="./hoodie-nam-bo-ong.html" data-handle="hoodie-nam-bo-ong" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./hoodie-nam-bo-ong.html">Hoodie Nam bo ống</a></h3>

                                <span class="price">Hết hàng</span>
                                <hr />

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./ao-khoac-gio-2-lop.html" title="Áo khoác gió 2 lớp">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/12_fddf659b.jpg" alt="Áo khoác gió 2 lớp" />
                                    </a>
                                </div>

                                <a href="./ao-khoac-gio-2-lop.html" data-handle="ao-khoac-gio-2-lop" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./ao-khoac-gio-2-lop.html">Áo khoác gió 2 lớp</a></h3>


                                <span class="price">495.000₫</span>
                                <hr />
                                <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147573" enctype="multipart/form-data" />
                                <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                <input type="hidden" name="variantId" value="12944574" />
                                <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                <div class="bizweb-product-reviews-badge" data-id="8147573"></div>
                                </form>


                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./ao-len-hoa-tiet.html" title="Áo len họa tiết">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/20_5c6c3740.jpg" alt="Áo len họa tiết" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>

                                <a href="./ao-len-hoa-tiet.html" data-handle="ao-len-hoa-tiet" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./ao-len-hoa-tiet.html">Áo len họa tiết</a></h3>


                                <span class="price">325.000₫</span> <span class="old-price">350.000₫</span>
                                <hr />
                                <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147572" enctype="multipart/form-data" />
                                <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                <input type="hidden" name="variantId" value="12944573" />
                                <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                <div class="bizweb-product-reviews-badge" data-id="8147572"></div>
                                </form>


                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./ao-thun-dai-tay.html" title="Áo thun dài tay">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/22_96e89cf7.jpg" alt="Áo thun dài tay" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>

                                <a href="./ao-thun-dai-tay.html" data-handle="ao-thun-dai-tay" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./ao-thun-dai-tay.html">Áo thun dài tay</a></h3>

                                <span class="price">Hết hàng</span>
                                <hr />

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">





                        <div class="item">
                            <div class="product-detail search">
                                <div class="image-detail">
                                    <a href="./quan-jeans-nu.html" title="Quần jeans nữ">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/6bj17s001sj1670_4ccc99ba.jpg" alt="Quần jeans nữ" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>

                                <a href="./quan-jeans-nu.html" data-handle="quan-jeans-nu" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                <h3><a class="product-name" href="./quan-jeans-nu.html">Quần jeans nữ</a></h3>


                                <span class="price">450.000₫</span> <span class="old-price">480.000₫</span>
                                <hr />
                                <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147570" enctype="multipart/form-data" />
                                <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                <input type="hidden" name="variantId" value="12944571" />
                                <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                <div class="bizweb-product-reviews-badge" data-id="8147570"></div>
                                </form>


                            </div>
                        </div>
                    </div>

                </div>
            </div>



            <div class="col-xs-12 text-xs-center">
                <nav>
                    <ul class="pagination clearfix">

			            <li class="page-item disabled"><a class="page-link" href="#">«</a>
			            </li>





			            <li class="active page-item disabled"><a class="page-link" href="javascript:;">1</a>
			            </li>




			            <li class="page-item"><a class="page-link" onclick="doSearch(2)" href="javascript:;">2</a>
			            </li>




			            <li class="page-item"><a class="page-link" onclick="doSearch(2)" href="javascript:;">»</a>
			            </li>

			        </ul>

                </nav>
            </div>
        </div>
    </div>
</section>