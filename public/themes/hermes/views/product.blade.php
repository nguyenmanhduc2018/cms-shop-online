<section class="bread-crumb">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home">
						<a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>						
						<span><i class="fa fa-angle-right"></i></span>
					</li>
					<li><strong><span itemprop="title"> Tất cả sản phẩm</span></strong></li>
				</ul>
			</div>
		</div>
	</div>
</section> 
<div class="container">
    <div class="row">
        <h1 class="hidden">Tất cả sản phẩm</h1>
        <section class="main_container collection col-xs-12 col-sm-12 col-md-12 col-lg-9 col-lg-push-3">
            <div class="banner-collection">
                <img src="{{ asset('themes/hermes') }}/assets/images/gridproduct-banner.jpg" alt="banner-collection" />
            </div>
            <h1 class="hidden title-head margin-top-0">Tất cả sản phẩm</h1>
            <div class="category-products products product-swiper">

                <div class="sortPagiBar">
                    <div class="row">
                        <div class="col-xs-5 col-sm-6 col-md-3">
                            <div class="view-mode">
                                <span class="hidden-xs">Hiển thị</span>
                                <a href="./products.html?view=grid&page=1" class="grid display-type active">
                                    <i class="fa fa-th" aria-hidden="true"></i>
                                </a>
                                <a href="./products.html?view=list&page=1" class="list display-type ">
                                    <i class="fa fa-th-list" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-7 col-sm-6 col-md-9 text-xs-left text-sm-right">
                            <div id="sort-by">
                                <label class="left hidden-xs">Sắp xếp </label>
                                <select class="form-control sort-by-script">
                                    <option value="default" />Mặc định
                                    <option value="price-asc" />Giá tăng dần
                                    <option value="price-desc" />Giá giảm dần
                                    <option value="alpha-asc" />Từ A-Z
                                    <option value="alpha-desc" />Từ Z-A
                                    <option value="created-asc" />Cũ đến mới
                                    <option value="created-desc" />Mới đến cũ
                                </select>
                                <!-- <ul>
					<li>
						<span>Thứ tự</span>
						<i class="fa fa-chevron-down" aria-hidden="true"></i>
						<ul>                    
							<li><a href="javascript:;" onclick="sortby('default')">Mặc định</a></li>								
							<li><a href="javascript:;" onclick="sortby('alpha-asc')">A &rarr; Z</a></li>
							<li><a href="javascript:;" onclick="sortby('alpha-desc')">Z &rarr; A</a></li>
							<li><a href="javascript:;" onclick="sortby('price-asc')">Giá tăng dần</a></li>
							<li><a href="javascript:;" onclick="sortby('price-desc')">Giá giảm dần</a></li>
							<li><a href="javascript:;" onclick="sortby('created-desc')">Hàng mới nhất</a></li>
							<li><a href="javascript:;" onclick="sortby('created-asc')">Hàng cũ nhất</a></li>
						</ul>
					</li>
				</ul> -->
                            </div>
                        </div>
                    </div>
                </div>

                <section class="products-view products-view-grid">

                    <div class="row">

                        <div class="col-xs-12 col-sm-6 col-md-4">

                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../ao-len-co-tron.html" title="Áo len cổ tròn">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/1a_219c2873.jpg" alt="Áo len cổ tròn" />
                                        </a>
                                    </div>

                                    <span class="sale-tag tag">Sale</span>

                                    <a href="../ao-len-co-tron.html" data-handle="ao-len-co-tron" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../ao-len-co-tron.html">Áo len cổ tròn</a></h3>

                                    <span class="price">Hết hàng</span>
                                    <hr />

                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">

                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../bouble-cotton-blazer.html" title="Vest Cotton Blazer">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/13_1e1a21f.jpg" alt="Vest Cotton Blazer" />
                                        </a>
                                    </div>

                                    <span class="sale-tag tag">Sale</span>

                                    <a href="../bouble-cotton-blazer.html" data-handle="bouble-cotton-blazer" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../bouble-cotton-blazer.html">Vest Cotton Blazer</a></h3>


                                    <span class="price">1.200.000₫</span> <span class="old-price">1.500.000₫</span>
                                    <hr />
                                    <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147581" enctype="multipart/form-data" />
                                    <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                    <input type="hidden" name="variantId" value="13235604" />
                                    <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                    <div class="bizweb-product-reviews-badge" data-id="8147581"></div>
                                    </form>


                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">





                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../winter-coat.html" title="Áo khoác lông vũ">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/11_6ad79329.jpg" alt="Áo khoác lông vũ" />
                                        </a>
                                    </div>

                                    <span class="sale-tag tag">Sale</span>

                                    <a href="../winter-coat.html" data-handle="winter-coat" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../winter-coat.html">Áo khoác lông vũ</a></h3>

                                    <span class="price">Hết hàng</span>
                                    <hr />

                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">





                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../sweeter-black.html" title="Áo len nam body">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/14_432e5336.jpg" alt="Áo len nam body" />
                                        </a>
                                    </div>

                                    <span class="sale-tag tag">Sale</span>

                                    <a href="../sweeter-black.html" data-handle="sweeter-black" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../sweeter-black.html">Áo len nam body</a></h3>


                                    <span class="price">180.000₫</span> <span class="old-price">200.000₫</span>
                                    <hr />
                                    <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147578" enctype="multipart/form-data" />
                                    <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                    <input type="hidden" name="variantId" value="13235658" />
                                    <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                    <div class="bizweb-product-reviews-badge" data-id="8147578"></div>
                                    </form>


                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">





                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../new-blazer.html" title="Áo len khóa kéo">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/6_63a1ee93.jpg" alt="Áo len khóa kéo" />
                                        </a>
                                    </div>

                                    <span class="sale-tag tag">Sale</span>

                                    <a href="../new-blazer.html" data-handle="new-blazer" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../new-blazer.html">Áo len khóa kéo</a></h3>


                                    <span class="price">Liên hệ</span>
                                    <hr />


                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">





                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../gillet.html" title="Áo thun cố tròn">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/24_69a359ed.jpg" alt="Áo thun cố tròn" />
                                        </a>
                                    </div>

                                    <a href="../gillet.html" data-handle="gillet" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../gillet.html">Áo thun cố tròn</a></h3>

                                    <span class="price">Hết hàng</span>
                                    <hr />

                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">





                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../mang-to-nam-2-lop.html" title="Măng tô nam 2 lớp">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/27_6ad79f8f.jpg" alt="Măng tô nam 2 lớp" />
                                        </a>
                                    </div>

                                    <span class="sale-tag tag">Sale</span>

                                    <a href="../mang-to-nam-2-lop.html" data-handle="mang-to-nam-2-lop" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../mang-to-nam-2-lop.html">Măng tô nam 2 lớp</a></h3>


                                    <span class="price">2.100.000₫</span> <span class="old-price">2.500.000₫</span>
                                    <hr />
                                    <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147575" enctype="multipart/form-data" />
                                    <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                    <input type="hidden" name="variantId" value="13235640" />
                                    <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                    <div class="bizweb-product-reviews-badge" data-id="8147575"></div>
                                    </form>


                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">





                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../hoodie-nam-bo-ong.html" title="Hoodie Nam bo ống">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/17_4e00d45d.jpg" alt="Hoodie Nam bo ống" />
                                        </a>
                                    </div>

                                    <a href="../hoodie-nam-bo-ong.html" data-handle="hoodie-nam-bo-ong" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../hoodie-nam-bo-ong.html">Hoodie Nam bo ống</a></h3>

                                    <span class="price">Hết hàng</span>
                                    <hr />

                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">





                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../ao-khoac-gio-2-lop.html" title="Áo khoác gió 2 lớp">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/12_fddf659b.jpg" alt="Áo khoác gió 2 lớp" />
                                        </a>
                                    </div>

                                    <a href="../ao-khoac-gio-2-lop.html" data-handle="ao-khoac-gio-2-lop" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../ao-khoac-gio-2-lop.html">Áo khoác gió 2 lớp</a></h3>


                                    <span class="price">495.000₫</span>
                                    <hr />
                                    <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147573" enctype="multipart/form-data" />
                                    <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                    <input type="hidden" name="variantId" value="12944574" />
                                    <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                    <div class="bizweb-product-reviews-badge" data-id="8147573"></div>
                                    </form>


                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">





                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../ao-len-hoa-tiet.html" title="Áo len họa tiết">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/20_5c6c3740.jpg" alt="Áo len họa tiết" />
                                        </a>
                                    </div>

                                    <span class="sale-tag tag">Sale</span>

                                    <a href="../ao-len-hoa-tiet.html" data-handle="ao-len-hoa-tiet" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../ao-len-hoa-tiet.html">Áo len họa tiết</a></h3>


                                    <span class="price">325.000₫</span> <span class="old-price">350.000₫</span>
                                    <hr />
                                    <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147572" enctype="multipart/form-data" />
                                    <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                    <input type="hidden" name="variantId" value="12944573" />
                                    <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                    <div class="bizweb-product-reviews-badge" data-id="8147572"></div>
                                    </form>


                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">





                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../ao-thun-dai-tay.html" title="Áo thun dài tay">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/22_96e89cf7.jpg" alt="Áo thun dài tay" />
                                        </a>
                                    </div>

                                    <span class="sale-tag tag">Sale</span>

                                    <a href="../ao-thun-dai-tay.html" data-handle="ao-thun-dai-tay" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../ao-thun-dai-tay.html">Áo thun dài tay</a></h3>

                                    <span class="price">Hết hàng</span>
                                    <hr />

                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">





                            <div class="item">
                                <div class="product-detail collection">
                                    <div class="image-detail">
                                        <a href="../quan-jeans-nu.html" title="Quần jeans nữ">
                                            <img src="{{ asset('themes/hermes') }}/assets/images/products/grande/6bj17s001sj1670_4ccc99ba.jpg" alt="Quần jeans nữ" />
                                        </a>
                                    </div>

                                    <span class="sale-tag tag">Sale</span>

                                    <a href="../quan-jeans-nu.html" data-handle="quan-jeans-nu" class="general-button quick-view hidden-md hidden-sm hidden-xs">Xem nhanh</a>
                                    <h3><a class="product-name" href="../quan-jeans-nu.html">Quần jeans nữ</a></h3>


                                    <span class="price">450.000₫</span> <span class="old-price">480.000₫</span>
                                    <hr />
                                    <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147570" enctype="multipart/form-data" />
                                    <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                    <input type="hidden" name="variantId" value="12944571" />
                                    <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                    <div class="bizweb-product-reviews-badge" data-id="8147570"></div>
                                    </form>


                                </div>
                            </div>
                        </div>
					</div>
<div class="text-xs-right">
    <nav>
        <ul class="pagination clearfix">

            <li class="page-item disabled"><a class="page-link" href="#">«</a>
            </li>





            <li class="active page-item disabled"><a class="page-link" href="javascript:;">1</a>
            </li>




            <li class="page-item"><a class="page-link" onclick="doSearch(2)" href="javascript:;">2</a>
            </li>




            <li class="page-item"><a class="page-link" onclick="doSearch(2)" href="javascript:;">»</a>
            </li>

        </ul>
    </nav>

</div>



</section>

</div>
</section>
<aside class="sidebar left left-content col-lg-3 col-xs-12 col-sm-12 col-md-12 col-lg-pull-9">

    <aside class="aside-item collection-category">
        <div class="aside-title">
            <h2 class="title-head margin-top-0 line-bottom"><span>Danh mục</span></h2>
        </div>
        <div class="aside-content">
            <nav class="nav-category navbar-toggleable-md">
                <ul class="nav navbar-pills">
		            {!!
		            	Menu::generateMenu([
		                    'slug' => 'main-menu',
		                    'view' => 'menu-product'
		                ]);
		            !!}
                </ul>
            </nav>
        </div>
    </aside>
    <div class="aside-filter">
        <div class="aside-title">
            <h2 class="title-head margin-top-0 line-bottom"><span>Bộ lọc</span></h2>
        </div>

        <aside class="aside-item filter-vendor">
            <div class="aside-title">
                <p class="title"><span>Thương hiệu</span>
                </p>
            </div>
            <div class="aside-content filter-group">
                <ul>


                    <li class="filter-item filter-item--check-box filter-item--green ">
                        <span>
						<label for="filter-bitis">
							<input type="checkbox" id="filter-bitis" onchange="toggleFilter(this)" data-group="Hãng" data-field="vendor" data-text="Bitis" value="(Bitis)" data-operator="OR" />
							<i class="fa"></i>
							Bitis
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green ">
                        <span>
						<label for="filter-canifa">
							<input type="checkbox" id="filter-canifa" onchange="toggleFilter(this)" data-group="Hãng" data-field="vendor" data-text="Canifa" value="(Canifa)" data-operator="OR" />
							<i class="fa"></i>
							Canifa
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green ">
                        <span>
						<label for="filter-khatoco">
							<input type="checkbox" id="filter-khatoco" onchange="toggleFilter(this)" data-group="Hãng" data-field="vendor" data-text="Khatoco" value="(Khatoco)" data-operator="OR" />
							<i class="fa"></i>
							Khatoco
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green ">
                        <span>
						<label for="filter-owen">
							<input type="checkbox" id="filter-owen" onchange="toggleFilter(this)" data-group="Hãng" data-field="vendor" data-text="Owen" value="(Owen)" data-operator="OR" />
							<i class="fa"></i>
							Owen
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green ">
                        <span>
						<label for="filter-simple-carry">
							<input type="checkbox" id="filter-simple-carry" onchange="toggleFilter(this)" data-group="Hãng" data-field="vendor" data-text="Simple Carry" value="(Simple Carry)" data-operator="OR" />
							<i class="fa"></i>
							Simple Carry
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green ">
                        <span>
						<label for="filter-viettien">
							<input type="checkbox" id="filter-viettien" onchange="toggleFilter(this)" data-group="Hãng" data-field="vendor" data-text="Viettien" value="(Viettien)" data-operator="OR" />
							<i class="fa"></i>
							Viettien
						</label>
					</span>
                    </li>


                </ul>
            </div>
        </aside>


        <aside class="aside-item filter-price">
            <div class="aside-title">
                <p class="title margin-top-0"><span>Giá sản phẩm</span>
                </p>
            </div>
            <div class="aside-content filter-group">
                <div id='slider'></div>
                <div id='start'>
                    <input value="0" />
                </div>
                <div id='stop'>
                    <input value="10000000" />
                </div>
                <a id="old-value" href="javascript:;"></a>
                <a id="filter-value" class="btn btn-gray" href="javascript:;" onclick="_toggleFilterdqdt(this);" data-value="(>-1 AND <10000001)">Lọc giá</a>
            </div>
        </aside>





        <aside class="aside-item filter-type">
            <div class="aside-title">
                <p class="title margin-top-0"><span>Loại</span>
                </p>
            </div>
            <div class="aside-content filter-group">
                <ul>


                    <li class="filter-item filter-item--check-box filter-item--green">
                        <span>
						<label for="filter-ao-khoac">
							<input type="checkbox" id="filter-ao-khoac" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Áo Khoác" value="(Áo Khoác)" data-operator="OR" />
							<i class="fa"></i>
							Áo Khoác
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green">
                        <span>
						<label for="filter-ao-len">
							<input type="checkbox" id="filter-ao-len" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Áo len" value="(Áo len)" data-operator="OR" />
							<i class="fa"></i>
							Áo len
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green">
                        <span>
						<label for="filter-ao-tap">
							<input type="checkbox" id="filter-ao-tap" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Áo tập" value="(Áo tập)" data-operator="OR" />
							<i class="fa"></i>
							Áo tập
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green">
                        <span>
						<label for="filter-ao-thun">
							<input type="checkbox" id="filter-ao-thun" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Áo thun" value="(Áo thun)" data-operator="OR" />
							<i class="fa"></i>
							Áo thun
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green">
                        <span>
						<label for="filter-balo">
							<input type="checkbox" id="filter-balo" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Balo" value="(Balo)" data-operator="OR" />
							<i class="fa"></i>
							Balo
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green">
                        <span>
						<label for="filter-blazer">
							<input type="checkbox" id="filter-blazer" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Blazer" value="(Blazer)" data-operator="OR" />
							<i class="fa"></i>
							Blazer
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green">
                        <span>
						<label for="filter-chan-vay">
							<input type="checkbox" id="filter-chan-vay" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Chân váy" value="(Chân váy)" data-operator="OR" />
							<i class="fa"></i>
							Chân váy
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green">
                        <span>
						<label for="filter-dep">
							<input type="checkbox" id="filter-dep" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Dép" value="(Dép)" data-operator="OR" />
							<i class="fa"></i>
							Dép
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green">
                        <span>
						<label for="filter-hoodie">
							<input type="checkbox" id="filter-hoodie" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Hoodie" value="(Hoodie)" data-operator="OR" />
							<i class="fa"></i>
							Hoodie
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green">
                        <span>
						<label for="filter-khau-trang">
							<input type="checkbox" id="filter-khau-trang" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Khẩu trang" value="(Khẩu trang)" data-operator="OR" />
							<i class="fa"></i>
							Khẩu trang
						</label>
					</span>
                    </li>



                    <li class="filter-item filter-item--check-box filter-item--green">
                        <span>
						<label for="filter-quan-jeans">
							<input type="checkbox" id="filter-quan-jeans" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Quần Jeans" value="(Quần Jeans)" data-operator="OR" />
							<i class="fa"></i>
							Quần Jeans
						</label>
					</span>
                    </li>


                </ul>
            </div>
        </aside>
	    </div>
	</aside>
	</div>
</div>