{!! dynamic_sidebar('slide_banner_home') !!}
{!! dynamic_sidebar('deal_hot') !!}
<div class="all-product">
    <div class="container">
        <h3>Tất cả sản phẩm</h3>
        <p class="sub-title">Tất cả sản phẩm của chúng tôi là những mẫu mới nhất được nhiều khách hàng quan tâm</p>
        <ul class="tab-header">

            <li><a href="#" data-tab="tab-2" class="active">Sản phẩm mới</a>
            </li>
            <li><a href="#" data-tab="tab-3">Nổi bật</a>
            </li>
            <li><a href="#" data-tab="tab-4">Khuyến mãi</a>
            </li>
            <li><a href="#" data-tab="tab-5">Phụ kiện</a>
            </li>
        </ul>
        <div class="tab-content-product col-xs-12">
            <div class="tab tab-2 active">

                <div class="swiper-block product-swiper">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="product-detail index">
                                <div class="image-detail">
                                    <a href="./bouble-cotton-blazer.html" title="Vest Cotton Blazer">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/full/13_1e1a21f.jpg" alt="Vest Cotton Blazer" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>
                                <h3><a class="product-name" href="./bouble-cotton-blazer.html">Vest Cotton Blazer</a></h3>


                                <span class="price">1.200.000₫</span> <span class="old-price">1.500.000₫</span>
                                <hr />
                                <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147581" enctype="multipart/form-data" />
                                <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                <input type="hidden" name="variantId" value="13235604" />
                                <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                <div class="DucN-product-reviews-badge" data-id="8147581"></div>
                                </form>


                            </div>
                        </div>





                        <div class="item">
                            <div class="product-detail index">
                                <div class="image-detail">
                                    <a href="./winter-coat.html" title="Áo khoác lông vũ">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/full/11_6ad79329.jpg" alt="Áo khoác lông vũ" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>
                                <h3><a class="product-name" href="./winter-coat.html">Áo khoác lông vũ</a></h3>

                                <span class="price">Hết hàng</span>
                                <hr />

                            </div>
                        </div>





                        <div class="item">
                            <div class="product-detail index">
                                <div class="image-detail">
                                    <a href="./mang-to-nam-2-lop.html" title="Măng tô nam 2 lớp">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/full/27_6ad79f8f.jpg" alt="Măng tô nam 2 lớp" />
                                    </a>
                                </div>

                                <span class="sale-tag tag">Sale</span>
                                <h3><a class="product-name" href="./mang-to-nam-2-lop.html">Măng tô nam 2 lớp</a></h3>


                                <span class="price">2.100.000₫</span> <span class="old-price">2.500.000₫</span>
                                <hr />
                                <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147575" enctype="multipart/form-data" />
                                <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                <input type="hidden" name="variantId" value="13235640" />
                                <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                <div class="DucN-product-reviews-badge" data-id="8147575"></div>
                                </form>


                            </div>
                        </div>





                        <div class="item">
                            <div class="product-detail index">
                                <div class="image-detail">
                                    <a href="./hoodie-nam-bo-ong.html" title="Hoodie Nam bo ống">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/full/17_4e00d45d.jpg" alt="Hoodie Nam bo ống" />
                                    </a>
                                </div>
                                <h3><a class="product-name" href="./hoodie-nam-bo-ong.html">Hoodie Nam bo ống</a></h3>

                                <span class="price">Hết hàng</span>
                                <hr />

                            </div>
                        </div>





                        <div class="item">
                            <div class="product-detail index">
                                <div class="image-detail">
                                    <a href="./ao-khoac-gio-2-lop.html" title="Áo khoác gió 2 lớp">
                                        <img src="{{ asset('themes/hermes') }}/assets/images/products/full/12_fddf659b.jpg" alt="Áo khoác gió 2 lớp" />
                                    </a>
                                </div>
                                <h3><a class="product-name" href="./ao-khoac-gio-2-lop.html">Áo khoác gió 2 lớp</a></h3>


                                <span class="price">495.000₫</span>
                                <hr />
                                <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8147573" enctype="multipart/form-data" />
                                <!-- <a href="javascript:void(0)" onclick="form.submit();" class="add-to-cart add-cart"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</a> -->
                                <input type="hidden" name="variantId" value="12944574" />
                                <button class="add-cart add-to-cart add_to_cart" title="Cho vào giỏ hàng"><i class="fa fa-plus" aria-hidden="true"></i> THÊM VÀO GIỎ</button>
                                <div class="DucN-product-reviews-badge" data-id="8147573"></div>
                                </form>


                            </div>
                        </div>
			        </div>
			    </div>
			</div>
    	</div>
    </div>
</div>