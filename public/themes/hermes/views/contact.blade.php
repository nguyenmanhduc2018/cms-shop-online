<section class="bread-crumb">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home">
						<a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>						
						<span><i class="fa fa-angle-right"></i></span>
					</li>
					
					<li><strong itemprop="title">Liên hệ</strong></li>
					
				</ul>
			</div>
		</div>
	</div>
</section>
<div class="container contact">
	<div class="row">
		<div class="col-lg-6 col-sm-12 col-md-12 col-xs-12">
			<div class="page-login">
				<div id="login">
					<h1 class="title-head">Liên hệ với chúng tôi</h1>
					<span>Bạn hãy điền nội dung tin nhắn vào form dưới đây và gửi cho chúng tôi. Chúng tôi sẽ trả lời bạn sau khi nhận được.</span>
					<form action='/contact' id='contact' method='post' />
					<div class="form-signup clearfix">
						<fieldset class="form-group">
							<input type="text" placeholder="Tên" name="contact[name]" id="name" class="form-control  form-control-lg" required="" />
						</fieldset>
						<fieldset class="form-group">
							<input type="email" placeholder="Email" name="contact[email]" id="email" class="form-control form-control-lg" required="" />
						</fieldset>
						<fieldset class="form-group">
							<textarea name="contact[body]" placeholder="Nội dung" id="comment" class="form-control form-control-lg" rows="5" required=""></textarea>
						</fieldset>
						<div class="pull-xs-left" style="margin-top:20px;">
							<button tyle="summit" class="btn btn-lg btn-style btn-style-active btn-dark">Gửi tin nhắn</button>
						</div> 
					</div>
					</form>
				</div>
			</div>
		</div>

		<div class="col-lg-6 col-sm-12 col-md-12 col-xs-12">
			
			<div class="widget-item info-contact">
				<!-- End .widget-title -->
				<ul class="widget-menu">
					<li><i class="fa fa-map-marker color-x" aria-hidden="true"></i> <p>
						
						Ninh Hòa, Nha Trang, Khánh Hòa
						
						</p></li>
					<li><i class="fa fa-phone color-x" aria-hidden="true"></i> <p><a href="tel:01684966474">01684966474</a></p></li>
					<li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:manhducit93@gmail.com"> manhducit93@gmail.com</a> </li>
				</ul>
				<!-- End .widget-menu -->
				<div class="box-maps">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d249525.13026515392!2d109.10641292898251!3d12.259770139102164!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3170677811cc886f%3A0x5c4bbc0aa81edcb9!2zVHAuIE5oYSBUcmFuZywgS2jDoW5oIEjDsmEsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1514189638794" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
			
		</div>


	</div>
</div>