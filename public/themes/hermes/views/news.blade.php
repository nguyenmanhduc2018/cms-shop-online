<section class="bread-crumb">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home">
						<a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>						
						<span><i class="fa fa-angle-right"></i></span>
					</li>
					
					<li><strong itemprop="title">Tin tức</strong></li>
					
				</ul>
			</div>
		</div>
	</div>
</section>
<div class="container">
    <div class="row">
        <section itemscope="" itemtype="http://schema.org/Blog" class="right-content blog-page-content col-xs-12 col-lg-9 col-lg-push-3">
            <meta itemprop="name" content="Thời trang nam" />
            <meta itemprop="description" content="Thời trang nam 2017" />
            <div class="row">
                <div class="box-heading">
                    <h1 class="title-head">Thời trang nam</h1>
                </div>

                <section class="list-blogs blog-main news-blog">

                    <div class="blog-detail col-md-6 col-sm-6 col-xs-12">
                        <div class="image-detail">
                            <a href="./vest-xuan-he-2017.html">

                                <img src="{{ asset('themes/hermes') }}/assets/images/products/articles/new-2_d0fb7894.jpg" alt="Những bộ sưu tập Vest xuân hè nổi bật nhất 2017" />

                            </a>
                            <a href="./vest-xuan-he-2017.html" class=" hidden">
                                <div class="mask"></div>
                            </a>
                        </div>

                        <p class="author">Viết bởi <span>Nguyễn Mạnh Đức</span>
                        </p>
                        <a href="./vest-xuan-he-2017.html" class="blog-title"><h3>Những bộ sưu tập Vest xuân hè nổi bật nhất 2017</h3></a>
                        <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 9/16/2017 10:31:00 PM</p>
                        <p>165 nhà triển lãm và hàng ngàn nhà báo quốc tế, những người mẫu nổi tiếng&nbsp;cùng những người mua sẽ tràn ngập trên các con phố huyền thoại của nước Ý trong khi hội chợ thương mại thời trang diễn ra.&nbsp;</p>
                        <a href="./vest-xuan-he-2017.html" class="see-more hidden-md hidden-sm hidden-xs">Xem thêm</a>
                    </div>

                    <div class="blog-detail col-md-6 col-sm-6 col-xs-12">
                        <div class="image-detail">
                            <a href="./6-xu-huong-thoi-trang-nam-mua-xuan-he-2018.html">

                                <img src="{{ asset('themes/hermes') }}/assets/images/products/articles/new-1_5e40f890.jpg" alt="6 xu hướng thời trang nam mùa xuân hè 2018" />

                            </a>
                            <a href="./6-xu-huong-thoi-trang-nam-mua-xuan-he-2018.html" class=" hidden">
                                <div class="mask"></div>
                            </a>
                        </div>

                        <p class="author">Viết bởi <span>Nguyễn Mạnh Đức</span>
                        </p>
                        <a href="./6-xu-huong-thoi-trang-nam-mua-xuan-he-2018.html" class="blog-title"><h3>6 xu hướng thời trang nam mùa xuân hè 2018</h3></a>
                        <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 9/16/2017 11:35:00 AM</p>
                        <p>Đến với tuần lễ thời trang nam 2018, giới mộ điệu được chứng kiến những xu hướng nổi bật như phong cách Mexico thập niên 40, hoạ tiết 3D in nổi... với những&nbsp;thiết kế vô cùng&nbsp;cầu kỳ, độc đáo.</p>
                        <a href="./6-xu-huong-thoi-trang-nam-mua-xuan-he-2018.html" class="see-more hidden-md hidden-sm hidden-xs">Xem thêm</a>
                    </div>

                    <div class="blog-detail col-md-6 col-sm-6 col-xs-12">
                        <div class="image-detail">
                            <a href="./so-mi-nam-2017.html">

                                <img src="{{ asset('themes/hermes') }}/assets/images/products/articles/new-4_bc6861c7.jpg" alt="Đa phong cách với áo sơ mi nam kẻ caro" />

                            </a>
                            <a href="./so-mi-nam-2017.html" class=" hidden">
                                <div class="mask"></div>
                            </a>
                        </div>

                        <p class="author">Viết bởi <span>Nguyễn Mạnh Đức</span>
                        </p>
                        <a href="./so-mi-nam-2017.html" class="blog-title"><h3>Đa phong cách với áo sơ mi nam kẻ caro</h3></a>
                        <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 9/7/2017 9:07:00 AM</p>
                        <p>Bộ sưu tập áo sơ mi nam thanh lịch&nbsp;tại Hermes Fashion&nbsp;- Trang phục sơ mi là item không thể thiếu đối với các chàng trai, nó mang lại nét đẹp thanh lịch, trẻ trung nhưng cũng&nbsp;không kém phần cá tính.</p>
                        <a href="./so-mi-nam-2017.html" class="see-more hidden-md hidden-sm hidden-xs">Xem thêm</a>
                    </div>

                    <div class="blog-detail col-md-6 col-sm-6 col-xs-12">
                        <div class="image-detail">
                            <a href="./du-doan-5-xu-huong-thoi-trang-nam-xuan-he-2017.html">

                                <img src="{{ asset('themes/hermes') }}/assets/images/products/articles/new-3_f4d1c6da.jpg" alt="Dự đoán 5 xu hướng thời trang nam Xuân - Hè 2017" />

                            </a>
                            <a href="./du-doan-5-xu-huong-thoi-trang-nam-xuan-he-2017.html" class=" hidden">
                                <div class="mask"></div>
                            </a>
                        </div>

                        <p class="author">Viết bởi <span>Nguyễn Mạnh Đức</span>
                        </p>
                        <a href="./du-doan-5-xu-huong-thoi-trang-nam-xuan-he-2017.html" class="blog-title"><h3>Dự đoán 5 xu hướng thời trang nam Xuân - Hè 2017</h3></a>
                        <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 9/3/2017 4:27:00 PM</p>
                        <p>Dù là một người đàn ông hiện đại, việc nắm bắt và ứng dụng các xu hướng thời trang dường như chưa bao giờ thực sự dễ dàng, bởi có quá nhiều thứ để phải chọn: Lifestyle, hình thể, râu-tóc. Ấn tượng...</p>
                        <a href="./du-doan-5-xu-huong-thoi-trang-nam-xuan-he-2017.html" class="see-more hidden-md hidden-sm hidden-xs">Xem thêm</a>
                    </div>

                    <div class="text-xs-left col-xs-12">


                    </div>
                </section>

            </div>
        </section>

        <aside class="left left-content col-lg-3 col-xs-12 col-lg-pull-9">

            <aside class="aside-item collection-category blog-category">
                <div class="heading">
                    <h2 class="blog-article-heading line-bottom"><span>Danh mục bài viết</span></h2>
                </div>
                <div class="aside-content">
                    <ul>

                        <li>
                            <a href="./bst-thu-dong.html">
                                <i class="fa fa-caret-right" aria-hidden="true"></i> BST Thu Đông
                                <span>(0)</span>
                            </a>
                        </li>

                        <li>
                            <a href="./bst-xuan-he.html">
                                <i class="fa fa-caret-right" aria-hidden="true"></i> BST Xuân Hè
                                <span>(0)</span>
                            </a>
                        </li>

                        <li>
                            <a href="./so-mi-nam.html">
                                <i class="fa fa-caret-right" aria-hidden="true"></i> Sơ mi nam
                                <span>(0)</span>
                            </a>
                        </li>

                        <li>
                            <a href="./thoi-trang-nam.html">
                                <i class="fa fa-caret-right" aria-hidden="true"></i> Thời trang nam
                                <span>(4)</span>
                            </a>
                        </li>

                    </ul>

                </div>
            </aside>
        </aside>
    </div>
</div>