<section class="bread-crumb">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home">
						<a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>						
						<span><i class="fa fa-angle-right"></i></span>
					</li>
					
					<li><strong itemprop="title">Giỏ hàng</strong></li>
					
				</ul>
			</div>
		</div>
	</div>
</section>
<div class="container container-cart">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="cart">
				
				<!-- <p>
					Không có sản phẩm nào trong giỏ hàng. Quay lại <a href="products.html">cửa hàng</a> để tiếp tục mua sắm.
				</p> -->
				<form method="post" action="/cart">
					<div class="shopping-cart">
                        <p class="line-bottom">Giỏ hàng của bạn</p>
                        <table cellspacing="0" celpadding="0" border="0" class="hidden-xs">
                            <thead>
                                <tr>
                                    <td width="15%">Hình ảnh</td>
                                    <td width="30%">Tên sản phẩm</td>
                                    <td width="15%">Đơn giá</td>
                                    <td width="15%">Số lượng</td>
                                    <td width="15%">Thành tiền</td>
                                    <td width="10%">Xóa</td>
                                </tr>
                            </thead>
                            <tbody>
								
									<tr>
										<td class="product-image">
											
											<a class="img-product-cart" href="/bouble-cotton-blazer?variantid=13235604" title="Vest Cotton Blazer">
												<img alt="Vest Cotton Blazer - M / Đen" src="{{ asset('themes/hermes') }}/assets/images/products/13_1e1a21f.jpg" alt="Vest Cotton Blazer">
											</a>
											
										</td>
										<td class="product-name">
											Vest Cotton Blazer
											
											<span>M / Đen</span>
											
										</td>
										<td class="price">1.200.000₫</td>
										
										<td class="quantity">
											<div class="quantity-detail">
												<input class="variantID" type="hidden" name="variantId" value="13235604">
												<input type="number" title="Số lượng" class="inp-number" value="2" id="updates_13235604" min="1" />
												<button class="quantity-top-plus quantity-button" type="button"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
												<button class="quantity-bottom-minus quantity-button" type="button"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>
											</div>
										</td>
										<td class="total-price-item price-13235604">2.400.000₫</td>
										<td class=""><a class="button remove-item" title="Xóa" href="/cart/change?line=1&amp;quantity=0" data-id="13235604">X</a></td>
									</tr>
								
                                
                            </tbody>
                        </table>
						
						<ul class="hidden-sm hidden-md hidden-lg cart-ul-mobile">
							<li class="product-image">
								
								<a class="img-product-cart" href="/bouble-cotton-blazer?variantid=13235604" title="Vest Cotton Blazer">
									<img alt="Vest Cotton Blazer - M / Đen" src="{{ asset('themes/hermes') }}/assets/images/products/13_1e1a21f.jpg" alt="Vest Cotton Blazer">
								</a>
								
							</li>
							<li>
								<p class="product-name">
									Vest Cotton Blazer
									
									<span>M / Đen</span>
									
								</p>
								<p class="price">1.200.000₫</p>
							</li>
							<li class="quantity">
								<div class="quantity-detail">
									<input class="variantID" type="hidden" name="variantId" value="13235604">
									<input type="number" title="Số lượng" class="inp-number" value="2" id="updates_13235604" min="1" />
									<button class="quantity-top-plus quantity-button" type="button"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
									<button class="quantity-bottom-minus quantity-button" type="button"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>
								</div>
								<a class="button remove-item" title="Xóa" href="/cart/change?line=1&amp;quantity=0" data-id="13235604">X</a>
							</li>
						</ul>
						
                        <div class="summarize">
                            <a href="/collections/all" class="continue-buying">Tiếp tục mua hàng</a>
                            <div class="checkout">
                                <p class="line-bottom">Tổng tiền thanh toán</p>
                                <ul>
                                    <li>Tổng cộng<span class="price main-color total-money">2.400.000₫</span></li>
                                </ul>
                                <a href="{{ route('public.checkout') }}" class="general-button">Thanh toán</a>
                            </div>
                        </div>
                    </div>
				</form>
				
				
			</div>
		</div>
	</div>
</div>