<section class="bread-crumb">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home">
						<a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>						
						<span><i class="fa fa-angle-right"></i></span>
					</li>
					
					<li><strong itemprop="title">Giới thiệu</strong></li>
					
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title category-title">
					<h1 class="title-head"><a href="javascript:;">Giới thiệu</a></h1>
				</div>
				<div class="content-page rte">
					<p>Ngày càng nhiều người Việt Nam sử dụng hàng cao cấp đắt tiền, không chỉ vì cái đẹp tinh tế, phong cách khác biệt, mà còn ở bản thân giá trị của thương hiệu đó. Đặt một món đồ hiệu kế bên đồ phổ thông, sẽ rất dễ dàng để nhận ra sự khác biệt đẳng cấp, từ những điểm nhỏ nhặt nhất. Đôi khi chỉ là một logo hết sức đơn giản như dấu Swoosh của Nike chẳng hạn, đơn giản đến mức hoàn hảo.</p>
					<p>&nbsp;</p>
					<p>Với các tín đồ thời trang, mặc hàng hiệu để trải nghiệm chất lượng chỉ mới là điều kiện “cần”, hàng hiệu có phù hợp với phong cách của mình cũng như có bật lên được cá tính của mình không mới là điều kiện “đủ”. Nhiều năm kinh nghiệm trong ngành thời trang, chúng tôi&nbsp; mang đến những thương hiệu thời trang hàng đầu tại Mỹ như: Affliction, Rebel Spirit, Sinful, True Religion, Ed Hardy, Christian Audigier, Roar, Xzavier, Rawyalty, Konflic, Xtreme Couture, Throwdown, Robin's Jean, Crash &amp; Burn...</p>
					<p>&nbsp;</p>
					<p>Trong đó, Affliction, Rebel Spirit, True Religion, Sinful, Roar là những thương hiệu thời trang mới mẻ đối với người Việt Nam, nhưng trên thế giới mà đặc biệt là ở Mỹ, đây là những thương hiệu đậm màu sắc cá tính nhất, luôn được dân sành điệu săn lùng. Những chiếc quần jeans&nbsp;<strong>True Religion</strong>&nbsp;phủi bụi, chiếc áo thun mang phong cách rock nổi loạn&nbsp;<strong>Affliction&nbsp;</strong>hay những chiếc áo sơ-mi&nbsp;<strong>Roar</strong>&nbsp;thiết kế và phối màu độc lạ mang lại một phong cách vừa ấn tượng táo bạo, vừa trẻ trung cá tính cho người mặc.</p>
					<p>&nbsp;</p>
					<p>Những thương hiệu này thổi một làn gió hoàn toàn mới vào những tín đồ của thời trang thích sự mới mẻ, lạ lẫm và thể hiện chất riêng của mình.</p>
					<p>Đến với&nbsp;<strong>Hermes Fashion</strong>, bạn không chỉ mua được những sản phẩm hàng hiệu chính gốc mà còn sở hữu chúng với những mức giá bất ngờ dựa vào chương trình bán hàng hấp dẫn theo từng đợt của chúng tôi.</p>
					<p>&nbsp;</p>
					<p>Hãy đến với <strong>Hermes Fashion&nbsp;</strong>để trải nghiệm thời trang hàng hiệu theo cách chuyên nghiệp và kinh tế nhất nhé!</p>
				</div>
			</div>
		</div>
	</div>
</section>