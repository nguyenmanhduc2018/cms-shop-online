<?php

Route::group(['namespace' => 'Botble\Woocommerce\Http\Controllers', 'middleware' => 'web'], function () {

    Route::group(['prefix' => config('cms.admin_dir'), 'middleware' => 'auth'], function () {
        Route::group(['prefix' => 'woocommerce'], function () {

            Route::get('/', [
                'as' => 'woocommerce.list',
                'uses' => 'WoocommerceController@getList',
            ]);

            Route::get('/create', [
                'as' => 'woocommerce.create',
                'uses' => 'WoocommerceController@getCreate',
            ]);

            Route::post('/create', [
                'as' => 'woocommerce.create',
                'uses' => 'WoocommerceController@postCreate',
            ]);

            Route::get('/edit/{id}', [
                'as' => 'woocommerce.edit',
                'uses' => 'WoocommerceController@getEdit',
            ]);

            Route::post('/edit/{id}', [
                'as' => 'woocommerce.edit',
                'uses' => 'WoocommerceController@postEdit',
            ]);

            Route::get('/delete/{id}', [
                'as' => 'woocommerce.delete',
                'uses' => 'WoocommerceController@getDelete',
            ]);

            Route::post('/delete-many', [
                'as' => 'woocommerce.delete.many',
                'uses' => 'WoocommerceController@postDeleteMany',
                'permission' => 'woocommerce.delete',
            ]);

            Route::post('/change-status', [
                'as' => 'woocommerce.change.status',
                'uses' => 'WoocommerceController@postChangeStatus',
                'permission' => 'woocommerce.edit',
            ]);











            // Ajax POST PRODUCT

            Route::post('/load_attributes_meta', [
                'as' => 'woocommerce.attributes_meta',
                'uses' => 'WoocommerceController@getAttributesMetaAjax',
            ]);

            Route::post('/postAttributeAjax', [
                'as' => 'woocommerce.attributtes_ajax',
                'uses' => 'WoocommerceController@postAttributeAjax',
            ]);

            //END Ajax POST PRODUCT










            // CATEGORY

            Route::get('/categories', [
                'as' => 'woocommerce.categories',
                'uses' => 'CategoriesController@getList',
            ]);

            Route::get('/categories/create', [
                'as' => 'woocommerce.categories.create',
                'uses' => 'CategoriesController@getCreate',
            ]);

            Route::post('/categories/create', [
                'as' => 'woocommerce.categories.create',
                'uses' => 'CategoriesController@postCreate',
            ]);

            Route::get('/categories/edit/{id}', [
                'as' => 'woocommerce.categories.edit',
                'uses' => 'CategoriesController@getEdit',
            ]);

            Route::post('/categories/edit/{id}', [
                'as' => 'woocommerce.categories.edit',
                'uses' => 'CategoriesController@postEdit',
            ]);

            Route::get('/categories/delete/{id}', [
                'as' => 'woocommerce.categories.delete',
                'uses' => 'CategoriesController@getDelete',
            ]);

            Route::post('/categories/delete-many', [
                'as' => 'woocommerce.categories.delete.many',
                'uses' => 'CategoriesController@postDeleteMany',
                'permission' => 'woocommerce.categories.delete',
            ]);

            Route::post('/categories/change-status', [
                'as' => 'woocommerce.categories.change.status',
                'uses' => 'CategoriesController@postChangeStatus',
                'permission' => 'woocommerce.categories.edit',
            ]);

            Route::get('/categories/trash', [
                'as' => 'woocommerce.categories.trash',
                'uses' => 'CategoriesController@getTrash',
            ]);

            // 

            Route::get('/categories/delete_permanently/{id}', [
                'as' => 'woocommerce.categories.delete_permanently',
                'uses' => 'CategoriesController@getDeletePermanently',
            ]);

            Route::post('/categories/delete_permanently-many', [
                'as' => 'woocommerce.categories.delete_permanently.many',
                'uses' => 'CategoriesController@postDeletePermanentlyMany',
            ]);

            Route::get('/categories/restore/{id}', [
                'as' => 'woocommerce.categories.restore',
                'uses' => 'CategoriesController@getRestore',
            ]);

            Route::post('/categories/restore-many', [
                'as' => 'woocommerce.categories.restore.many',
                'uses' => 'CategoriesController@postRestoreMany',
            ]);

            //END CATEGORY









            // ORDER

            Route::get('/shop_order', [
                'as' => 'woocommerce.order',
                'uses' => 'WoocommerceController@getOrder',
            ]);

            //END ORDER








            // ATTRIBUTE

            Route::get('/product_attributes', [
                'as' => 'woocommerce.attributes',
                'uses' => 'AttributesController@getList',
            ]);

            Route::get('/product_attributes/create', [
                'as' => 'woocommerce.attributes.create',
                'uses' => 'AttributesController@getCreate',
            ]);

            Route::post('/product_attributes/create', [
                'as' => 'woocommerce.attributes.create',
                'uses' => 'AttributesController@postCreate',
            ]);

            Route::get('/product_attributes/edit/{id}', [
                'as' => 'woocommerce.attributes.edit',
                'uses' => 'AttributesController@getEdit',
            ]);

            Route::post('/product_attributes/edit/{id}', [
                'as' => 'woocommerce.attributes.edit',
                'uses' => 'AttributesController@postEdit',
            ]);

            Route::get('/product_attributes/delete/{id}', [
                'as' => 'woocommerce.attributes.delete',
                'uses' => 'AttributesController@getDelete',
            ]);

            Route::post('/product_attributes/delete-many', [
                'as' => 'woocommerce.attributes.delete.many',
                'uses' => 'AttributesController@postDeleteMany',
                'permission' => 'woocommerce.attributes.delete',
            ]);

            Route::post('/product_attributes/change-status', [
                'as' => 'woocommerce.attributes.change.status',
                'uses' => 'AttributesController@postChangeStatus',
                'permission' => 'woocommerce.attributes.edit',
            ]);

            Route::get('/product_attributes/trash', [
                'as' => 'woocommerce.attributes.trash',
                'uses' => 'AttributesController@getTrash',
            ]);

            //

            Route::get('/product_attributes/create/terms/{id}', [
                'as' => 'woocommerce.attributes.create.terms',
                'uses' => 'AttributesController@getCreateTerms',
            ]);

            Route::post('/product_attributes/create/terms/{id}', [
                'as' => 'woocommerce.attributes.create.terms',
                'uses' => 'AttributesController@postCreateTerms',
            ]);

            Route::get('/product_attributes/edit/terms', [
                'as' => 'woocommerce.attributes.edit.terms',
                'uses' => 'AttributesController@getEditTerms',
            ]);

            Route::post('/product_attributes/edit/terms/{id}', [
                'as' => 'woocommerce.attributes.edit.terms',
                'uses' => 'AttributesController@postEditTerms',
            ]);

            Route::get('/product_attributes/delete/terms/{id}', [
                'as' => 'woocommerce.attributes.delete.terms',
                'uses' => 'AttributesController@getDeleteTerms',
            ]);

            Route::post('/product_attributes/delete_permanently-many/terms', [
                'as' => 'woocommerce.attributes.delete_permanently.many.terms',
                'uses' => 'AttributesController@postDeletePermanentlyManyTerms',
            ]);

            // 

            Route::get('/product_attributes/delete_permanently/{id}', [
                'as' => 'woocommerce.attributes.delete_permanently',
                'uses' => 'AttributesController@getDeletePermanently',
            ]);

            Route::post('/product_attributes/delete_permanently-many', [
                'as' => 'woocommerce.attributes.delete_permanently.many',
                'uses' => 'AttributesController@postDeletePermanentlyMany',
            ]);

            Route::get('/product_attributes/restore/{id}', [
                'as' => 'woocommerce.attributes.restore',
                'uses' => 'AttributesController@getRestore',
            ]);

            Route::post('/product_attributes/restore-many', [
                'as' => 'woocommerce.attributes.restore.many',
                'uses' => 'AttributesController@postRestoreMany',
            ]);

            // END ATTRIBUTE







            //SETTING

            Route::get('/settings', [
                'as' => 'woocommerce.setting',
                'uses' => 'WoocommerceController@getSettings',
            ]);

            Route::post('/settings', [
                'as' => 'woocommerce.setting',
                'uses' => 'WoocommerceController@postSettings',
            ]);

            //END SETTING

        });
    });
    
});