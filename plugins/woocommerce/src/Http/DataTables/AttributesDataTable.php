<?php

namespace Botble\Woocommerce\Http\DataTables;

use Botble\Base\Http\DataTables\DataTableAbstract;
use Botble\Woocommerce\Repositories\Interfaces\AttributesInterface;

class AttributesDataTable extends DataTableAbstract
{
	/**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     * @author Duc Nguyen
     * @since 2.1
     */

	public function ajax()
    {
        $data = $this->datatables
            ->eloquent($this->query())
            ->editColumn('name', function ($item) {
                return anchor_link(route('woocommerce.attributes.edit', $item->id), $item->attribute_label);
            })
            ->editColumn('slug', function ($item) {
                return $item->attribute_name;
            })
            ->editColumn('type', function ($item) {
                if($item->attribute_public === 1)
                    return '<span style="text-transform: capitalize;">'.$item->attribute_type.'</span> (Public)';
                else
                    return '<span style="text-transform: capitalize;">'.$item->attribute_type.'</span>';
            })
            ->editColumn('terms', function ($item) {
            	$collection = get_terms_by_attr_id($item->id);
                if(count($collection) === 0) {
                    return '-<br><a href="#">Configure terms</a>';
                }else{
                    foreach ($collection as $value) {
                        $text[] = '<span>'.$value->name.'<span>';               
                    }
                    $text = implode(', ', $text);
                    $text = $text.'<br><a href="#">Configure terms</a>';
                    return $text;
                }
            })
            ->editColumn('checkbox', function ($item) {
                return table_checkbox($item->id);
            })
            ->removeColumn('is_default');

            return apply_filters(BASE_FILTER_GET_LIST_DATA, $data, WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME)
            ->addColumn('operations', function ($item) {
                return table_actions('woocommerce.attributes.edit', 'woocommerce.attributes.delete', $item);
            })
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     * @author Duc Nguyen
     * @since 2.1
     */
    public function query()
    {
        $model =app(AttributesInterface::class)->getModel();
        /**
         * @var \Eloquent $model
         */
        $query = $model
            ->select(['woocommerce_attribute_taxonomies.id',
            	'woocommerce_attribute_taxonomies.attribute_label', 
            	'woocommerce_attribute_taxonomies.attribute_name', 
            	'woocommerce_attribute_taxonomies.attribute_type',
                'woocommerce_attribute_taxonomies.attribute_public']);
       return $this->applyScopes(apply_filters(BASE_FILTER_DATATABLES_QUERY, $query, $model, WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME));
    }

    /**
     * @return array
     * @author Duc Nguyen
     * @since 2.1
     */
    public function columns()
    {
        return [
            // 'id' => [
            //     'name' => 'attribute.id',
            //     'title' => trans('bases::tables.id'),
            //     'width' => '20px',
            //     'class' => 'searchable searchable_id',
            // ],
            'name' => [
                'name' => 'attribute.name',
                'title' => trans('bases::tables.name'),
                'class' => 'searchable',
            ],
            'slug' => [
                'name' => 'attribute.slug',
                'title' => trans('woocommerce::attribute.slug'),
                'class' => 'text-left searchable',
            ],
            'type' => [
                'name' => 'attribute.type',
                'title' => trans('woocommerce::attribute.type'),
                'class' => 'text-left searchable',
            ],
            'terms' => [
                'name' => 'attribute.terms',
                'title' => trans('woocommerce::attribute.terms'),
                'class' => 'text-left searchable',
            ],
        ];
    }

    /**
     * @return array
     * @author Duc Nguyen
     * @since 2.1
     */
    public function buttons()
    {
        $buttons = [
            'create' => [
                'link' => route('woocommerce.attributes.create'),
                'text' => view('bases::elements.tables.actions.create')->render(),
            ],
            // 'trash' => [
            //     'link' => route('woocommerce.attributes.trash'),
            //     'text' => view('bases::elements.tables.actions.trash')->render(),
            // ],
        ];
        return apply_filters(BASE_FILTER_DATATABLES_BUTTONS, $buttons, WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME);
    }

    /**
     * @return array
     * @author Duc Nguyen
     * @since 2.1
     */
    public function actions()
    {
        return [
            'delete' => [
                'link' => route('woocommerce.attributes.delete.many'),
                'text' => view('bases::elements.tables.actions.delete')->render(),
            ],
            'activate' => [
                'link' => route('woocommerce.attributes.change.status', ['status' => 1]),
                'text' => view('bases::elements.tables.actions.activate')->render(),
            ],
            'deactivate' => [
                'link' => route('woocommerce.attributes.change.status', ['status' => 0]),
                'text' => view('bases::elements.tables.actions.deactivate')->render(),
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     * @author Duc Nguyen
     * @since 2.1
     */
    protected function filename()
    {
        return WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME;
    }
}