<?php

namespace Botble\Woocommerce\Http\DataTables;

use Botble\Base\Http\DataTables\DataTableAbstract;
use Botble\Blog\Repositories\Interfaces\CategoryInterface;

class CategoriesDataTable extends DataTableAbstract
{
	/**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     * @author Duc Nguyen
     * @since 2.1
     */

	public function ajax()
    {
        $data = $this->datatables
            ->eloquent($this->query())
            ->editColumn('image', function ($item) {
                return '<img src="'.get_object_image($item->image, 'small').'">';
            })
            ->editColumn('name', function ($item) {
                return anchor_link(route('woocommerce.categories.edit', $item->id), $item->name);
            })
            ->editColumn('checkbox', function ($item) {
                return table_checkbox($item->id);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at, 'd-m-Y');
            })
            ->editColumn('updated_at', function ($item) {
                return date_from_database($item->updated_at, 'd-m-Y');
            })
            ->editColumn('status', function ($item) {
                return table_status($item->status);
            })
            ->removeColumn('is_default');

        return apply_filters(BASE_FILTER_GET_LIST_DATA, $data, CATEGORY_MODULE_SCREEN_NAME)
            ->addColumn('operations', function ($item) {
                return table_actions('woocommerce.categories.edit', 'woocommerce.categories.delete', $item);
            })
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     * @author Duc Nguyen
     * @since 2.1
     */
    public function query()
    {
        $model =app(CategoryInterface::class)->getModel();
        /**
         * @var \Eloquent $model
         */
        $query = $model
            ->select(['categories.id', 'categories.name', 'categories.image', 'categories.status', 'categories.order', 'categories.created_at', 'categories.is_default', 'categories.parent_id'])
            ->where('categories.cate_type', '=', 'product');
        return $this->applyScopes(apply_filters(BASE_FILTER_DATATABLES_QUERY, $query, $model, CATEGORY_MODULE_SCREEN_NAME));
    }

    /**
     * @return array
     * @author Duc Nguyen
     * @since 2.1
     */
    public function columns()
    {
        return [
            // 'id' => [
            //     'name' => 'categories.id',
            //     'title' => trans('bases::tables.id'),
            //     'width' => '20px',
            //     'class' => 'searchable searchable_id',
            // ],
            'image' => [
                'name' => 'categories.image',
                'title' => '<i class="fa fa-picture-o"></i>',
            ],
            'name' => [
                'name' => 'categories.name',
                'title' => trans('bases::tables.name'),
                'class' => 'searchable',
            ],
            'created_at' => [
                'name' => 'categories.created_at',
                'title' => trans('bases::tables.created_at'),
                'class' => 'searchable',
                'width' => '100px',
            ],
            'updated_at' => [
                'name' => 'categories.updated_at',
                'title' => trans('bases::tables.updated_at'),
                'width' => '100px',
            ],
            'status' => [
                'name' => 'categories.status',
                'title' => trans('bases::tables.status'),
                'width' => '100px',
            ],
        ];
    }

    /**
     * @return array
     * @author Duc Nguyen
     * @since 2.1
     */
    public function buttons()
    {
        $buttons = [
            'create' => [
                'link' => route('woocommerce.categories.create'),
                'text' => view('bases::elements.tables.actions.create')->render(),
            ],
            'trash' => [
                'link' => route('woocommerce.categories.trash'),
                'text' => view('bases::elements.tables.actions.trash')->render(),
            ],
        ];
        return apply_filters(BASE_FILTER_DATATABLES_BUTTONS, $buttons, CATEGORY_MODULE_SCREEN_NAME);
    }

    /**
     * @return array
     * @author Duc Nguyen
     * @since 2.1
     */
    public function actions()
    {
        return [
            'delete' => [
                'link' => route('woocommerce.categories.delete.many'),
                'text' => view('bases::elements.tables.actions.delete')->render(),
            ],
            'activate' => [
                'link' => route('woocommerce.categories.change.status', ['status' => 1]),
                'text' => view('bases::elements.tables.actions.activate')->render(),
            ],
            'deactivate' => [
                'link' => route('woocommerce.categories.change.status', ['status' => 0]),
                'text' => view('bases::elements.tables.actions.deactivate')->render(),
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     * @author Duc Nguyen
     * @since 2.1
     */
    protected function filename()
    {
        return CATEGORY_MODULE_SCREEN_NAME;
    }
}