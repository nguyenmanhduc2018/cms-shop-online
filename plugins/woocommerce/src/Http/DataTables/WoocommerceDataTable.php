<?php

namespace Botble\Woocommerce\Http\DataTables;

use Botble\Base\Http\DataTables\DataTableAbstract;
use Botble\Woocommerce\Repositories\Interfaces\WoocommerceInterface;

class WoocommerceDataTable extends DataTableAbstract
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     * @author Sang Nguyen
     * @since 2.1
     */
    public function ajax()
    {
        $data = $this->datatables
            ->eloquent($this->query())
            ->editColumn('image', function ($item) {
                return '<img src="'.get_object_image($item->image, 'small').'">';
            })
            ->editColumn('name', function ($item) {
                return anchor_link(route('woocommerce.edit', $item->id), $item->name);
            })
            ->editColumn('sku', function ($item) {
                $sku = app(WoocommerceInterface::class)->getPostMetaByID($item->id);
                foreach ($sku as $value) {
                    if($value->meta_key === '_sku'){
                        if($value->meta_value === "" || $value->meta_value === null)
                            return '-';
                        else
                            return $value->meta_value;
                    }
                }
            })
            ->editColumn('price', function ($item) {
                $price = app(WoocommerceInterface::class)->getPostMetaByID($item->id);
                if(empty($price)) return '-';
                foreach ($price as $value) {
                    if($value->meta_key === 'regular_price'){
                        $priceData[$value->meta_key] = $value->meta_value;
                    }
                    if($value->meta_key === 'sale_price'){
                        $priceData[$value->meta_key] = $value->meta_value;
                    }
                }
                if($priceData['sale_price'] !== ""){
                    $html = '<div><del>'.$priceData['regular_price'].'</del></div><div>'.$priceData['sale_price'].'</div>';
                }else{
                    $html = '<span>'.$priceData['regular_price'].' </span>';
                }
                return $html;
                
            })
            ->editColumn('categories', function ($item) {
                $category = app(WoocommerceInterface::class)->getCategoryByPostID($item->id);
                if(!empty($category)){
                    $index = 0;
                    foreach ($category as $value) {
                        if($index === 0){
                            $category_name = anchor_link(route('categories.edit', $value->id), $value->name);
                        }else{
                            $category_name = $category_name . '<br> ' . anchor_link(route('categories.edit', $value->id), $value->name);
                        }
                        ++$index;
                    }
                    if(empty($category_name)){
                        $category_name = '-';
                    }
                    return $category_name;
                    //print_r($category);
                }
            })
            ->editColumn('checkbox', function ($item) {
                return table_checkbox($item->id);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at, 'd-m-Y');
            })
            ->editColumn('status', function ($item) {
                return table_status($item->status);
            });

            return apply_filters(BASE_FILTER_GET_LIST_DATA, $data, WOOCOMMERCE_MODULE_SCREEN_NAME)
            ->addColumn('operations', function ($item) {
                return table_actions('woocommerce.edit', 'woocommerce.delete', $item);
            })
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     * @author Sang Nguyen
     * @since 2.1
     */
    public function query()
    {
       $model = app(WoocommerceInterface::class)->getModel();
       /**
        * @var \Eloquent $model
        */
       //$query = $model->select(['woocommerceid', 'woocommerce.name', 'woocommerce.created_at', 'woocommerce.status']);
        $query = $model
                    ->leftJoin('post_category', 'post_category.post_id', '=', 'posts.id')
                    ->leftJoin('categories','categories.id', '=', 'post_category.category_id')
                    ->where('posts.post_type', 'product')
                    ->select(['posts.id', 
                        'posts.name', 
                        'posts.status', 
                        'posts.image'
                    ])->groupBy(
                        'posts.id', 
                        'posts.name', 
                        'posts.status', 
                        'posts.image'
                    );
        return $this->applyScopes(apply_filters(BASE_FILTER_DATATABLES_QUERY, $query, $model, WOOCOMMERCE_MODULE_SCREEN_NAME));
       
    }

    /**
     * @return array
     * @author Sang Nguyen
     * @since 2.1
     */
    public function columns()
    {
        return [
            // 'id' => [
            //     'name' => 'posts.id',
            //     'title' => trans('bases::tables.id'),
            //     'width' => '20px',
            //     'class' => 'searchable searchable_id',
            // ],
            'image' => [
                'name' => 'posts.image',
                'title' => '<i class="fa fa-picture-o"></i>'
            ],
            'name' => [
                'name' => 'posts.name',
                'title' => trans('bases::tables.name'),
                'class' => 'text-left searchable',
            ],
            'sku' => [
                'name' => 'posts.sku',
                'title' => trans('woocommerce::woocommerce.sku'),
                'class' => 'text-left searchable',
            ],
            'price' => [
                'name' => 'posts.price',
                'title' => trans('woocommerce::woocommerce.price'),
                'class' => 'text-left searchable',
            ],
            'categories' => [
                'name' => 'posts.categories',
                'title' => trans('woocommerce::woocommerce.cate'),
                'class' => 'text-left searchable',
            ],
            'status' => [
                'name' => 'posts.status',
                'title' => trans('bases::tables.status'),
                'width' => '100px',
            ],
            'created_at' => [
                'name' => 'posts.created_at',
                'title' => trans('bases::tables.created_at'),
                'width' => '100px',
                'class' => 'searchable',
            ],
        ];
    }

    /**
     * @return array
     * @author Sang Nguyen
     * @since 2.1
     */
    public function buttons()
    {
        $buttons = [
            'create' => [
                'link' => route('woocommerce.create'),
                'text' => view('bases::elements.tables.actions.create')->render(),
            ],
        ];
        return apply_filters(BASE_FILTER_DATATABLES_BUTTONS, $buttons, WOOCOMMERCE_MODULE_SCREEN_NAME);
    }

    /**
     * @return array
     * @author Sang Nguyen
     * @since 2.1
     */
    public function actions()
    {
        return [
            'delete' => [
                'link' => route('woocommerce.delete.many'),
                'text' => view('bases::elements.tables.actions.delete')->render(),
            ],
            'activate' => [
                'link' => route('woocommerce.change.status', ['status' => 1]),
                'text' => view('bases::elements.tables.actions.activate')->render(),
            ],
            'deactivate' => [
                'link' => route('woocommerce.change.status', ['status' => 0]),
                'text' => view('bases::elements.tables.actions.deactivate')->render(),
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     * @author Sang Nguyen
     * @since 2.1
     */
    protected function filename()
    {
        return WOOCOMMERCE_MODULE_SCREEN_NAME;
    }
}
