<?php

namespace Botble\Woocommerce\Http\Requests;

use Botble\Support\Http\Requests\Request;

class AttributesRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @author Sang Nguyen
     */
    public function rules()
    {
        return [
            'attribute_label' => 'required|max:120',
            'attribute_name' => 'required',
            'attribute_type' => 'required', 
        ];
    }
}
