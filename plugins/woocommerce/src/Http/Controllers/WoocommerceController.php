<?php

namespace Botble\Woocommerce\Http\Controllers;

use Assets;
use Botble\Woocommerce\Http\Requests\WoocommerceRequest;
use Botble\Woocommerce\Repositories\Interfaces\WoocommerceInterface;
use Botble\Blog\Repositories\Interfaces\PostInterface;
use Botble\Base\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use MongoDB\Driver\Exception\Exception;
use Botble\Woocommerce\Http\DataTables\WoocommerceDataTable;
use Botble\Blog\Services\StoreCategoryService;
use Botble\Blog\Services\StoreTagService;

class WoocommerceController extends BaseController
{
    /**
     * @var WoocommerceInterface
     */
    protected $woocommerceRepository;

    /**
     * @var WoocommerceInterface
     */
    protected $postRepository;

    /**
     * WoocommerceController constructor.
     * @param WoocommerceInterface $woocommerceRepository
     * @author Sang Nguyen
     */
    public function __construct(WoocommerceInterface $woocommerceRepository, PostInterface $postRepository)
    {
        $this->woocommerceRepository = $woocommerceRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * Display all woocommerce
     * @param WoocommerceDataTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Sang Nguyen
     */
    public function getList(WoocommerceDataTable $dataTable)
    {

        page_title()->setTitle(trans('woocommerce::woocommerce.list'));

        return $dataTable->renderTable(['title' => trans('woocommerce::woocommerce.list')]);
    }

    /**
     * Show create form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Sang Nguyen
     */
    public function getCreate()
    {
        page_title()->setTitle(trans('woocommerce::woocommerce.create'));
        
        Assets::addJavascript(['bootstrap-tagsinput', 'typeahead', 'are-you-sure']);
        Assets::addStylesheets(['bootstrap-tagsinput']);
        Assets::addAppModule(['tags', 'slug']);

        $categories = get_categories_product_with_children();

        $listAttribute = $this->woocommerceRepository->getAllAttribute(['attribute_id', 'attribute_name', 'attribute_label']);
        return view('woocommerce::create', compact('categories', 'listAttribute'));
    }

    /**
     * Insert new Woocommerce into database
     *
     * @param WoocommerceRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @author Sang Nguyen
     */
    public function postCreate(WoocommerceRequest $request, StoreTagService $tagService, StoreCategoryService $categoryService)
    {

        $woocommerce = $this->postRepository->createOrUpdate(array_merge($request->input(), [
            'user_id' => acl_get_current_user_id(),
            'featured' => $request->input('featured', false)
        ]));
        $post_id = $woocommerce->id;
        //$post_id = 1;
        $this->postRepository->update([
            'id' => $post_id,
            'user_id' => acl_get_current_user_id()
        ], ['post_type' => 'product']);


        $data = [
            'regular_price' => $request->input('regular_price'),
            'sale_price' => $request->input('sale_price'),
            '_sale_price_dates_from' => $request->input('_sale_price_dates_from'),
            '_sale_price_dates_to' => $request->input('_sale_price_dates_to'),
            '_sku' => $request->input('_sku'),
            '_stock_status' => $request->input('_stock_status')
        ];

        
        
        $this->woocommerceRepository->createPostMeta($post_id, $data);
    
        do_action(BASE_ACTION_AFTER_CREATE_CONTENT, POST_MODULE_SCREEN_NAME, $request, $woocommerce);

        $tagService->execute($request, $woocommerce);
        $categoryService->execute($request, $woocommerce);

        if ($request->input('submit') === 'save') {
            return redirect()->route('woocommerce.list')->with('success_msg', trans('bases::notices.create_success_message'));
        } else {
            return redirect()->route('woocommerce.edit', $woocommerce->id)->with('success_msg', trans('bases::notices.create_success_message'));
        }
    }

    /**
     * Show edit form
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Sang Nguyen
     */
    public function getEdit($id)
    {
        page_title()->setTitle(trans('woocommerce::woocommerce.edit') . ' #' . $id);

        Assets::addJavascript(['bootstrap-tagsinput', 'typeahead']);
        Assets::addStylesheets(['bootstrap-tagsinput']);
        Assets::addAppModule(['tags', 'slug']);

        $woocommerce = $this->postRepository->findById($id);
        $postMeta   = $this->woocommerceRepository->getPostMetaByID($id);
        foreach ($postMeta as $value) {
            $dataPostMeta[$value->meta_key] = $value->meta_value;
        }
        if(!empty($dataPostMeta)){
            $dataPostMeta = (object) $dataPostMeta;
        }else{
            $dataPostMeta = [];
        }

        $selected_categories = [];
        if ($woocommerce->categories != null) {
            $selected_categories = $woocommerce->categories->pluck('id')->all();
        }

        $tags = $woocommerce->tags->pluck('name')->all();
        $tags = implode(',', $tags);

        $categories = get_categories_product_with_children();

        $listAttribute = $this->woocommerceRepository->getAllAttribute();

        $getPostAttrMeta = get_post_attribute_meta_by_id($id);
        $getPostAttrMeta = json_decode($getPostAttrMeta);
    
        return view('woocommerce::edit', compact('woocommerce', 'dataPostMeta', 'selected_categories', 'categories', 'listAttribute', 'tags', 'getPostAttrMeta'));
    }

    /**
     * @param $id
     * @param WoocommerceRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @author Sang Nguyen
     */
    public function postEdit($id, WoocommerceRequest $request)
    {
        $woocommerce = $this->woocommerceRepository->findById($id);
        $woocommerce->fill($request->input());

        $this->postRepository->createOrUpdate($woocommerce);

        $data = [
            'regular_price' => $request->input('regular_price'),
            'sale_price' => $request->input('sale_price'),
            '_sale_price_dates_from' => $request->input('_sale_price_dates_from'),
            '_sale_price_dates_to' => $request->input('_sale_price_dates_to'),
            '_sku' => $request->input('_sku'),
            '_stock_status' => $request->input('_stock_status')
        ];

        $this->woocommerceRepository->updatePostMeta($id, $data);

        do_action(BASE_ACTION_AFTER_UPDATE_CONTENT, POST_MODULE_SCREEN_NAME, $request, $woocommerce);

        if ($request->input('submit') === 'save') {
            return redirect()->route('woocommerce.list')->with('success_msg', trans('bases::notices.update_success_message'));
        } else {
            return redirect()->route('woocommerce.edit', $id)->with('success_msg', trans('bases::notices.update_success_message'));
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return array
     * @author Sang Nguyen
     */
    public function getDelete(Request $request, $id)
    {
        try {
            $woocommerce = $this->woocommerceRepository->findById($id);
            $this->woocommerceRepository->delete($woocommerce);

            do_action(BASE_ACTION_AFTER_DELETE_CONTENT, WOOCOMMERCE_MODULE_SCREEN_NAME, $request, $woocommerce);

            return [
                'error' => false,
                'message' => trans('bases::notices.deleted'),
            ];
        } catch (Exception $e) {
            return [
                'error' => true,
                'message' => trans('bases::notices.cannot_delete'),
            ];
        }
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @author Sang Nguyen
     */
    public function postDeleteMany(Request $request)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return [
                'error' => true,
                'message' => trans('bases::notices.no_select'),
            ];
        }

        foreach ($ids as $id) {
            $woocommerce = $this->woocommerceRepository->findById($id);
            $this->woocommerceRepository->delete($woocommerce);
            do_action(BASE_ACTION_AFTER_DELETE_CONTENT, WOOCOMMERCE_MODULE_SCREEN_NAME, $request, $woocommerce);
        }

        return [
            'error' => false,
            'message' => trans('bases::notices.delete_success_message'),
        ];
    }

    /**
     * @param Request $request
     * @return array
     * @author Sang Nguyen
     */
    public function postChangeStatus(Request $request)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return [
                'error' => true,
                'message' => trans('bases::notices.no_select'),
            ];
        }

        foreach ($ids as $id) {
            $woocommerce = $this->woocommerceRepository->findById($id);
            $woocommerce->status = $request->input('status');
            $this->woocommerceRepository->createOrUpdate($woocommerce);

            do_action(BASE_ACTION_AFTER_UPDATE_CONTENT, WOOCOMMERCE_MODULE_SCREEN_NAME, $request, $woocommerce);
        }

        return [
            'error' => false,
            'status' => $request->input('status'),
            'message' => trans('bases::notices.update_success_message'),
        ];
    }

    public function getAttributesMetaAjax(Request $request){
        $Attrid = $request->id;
        $Attrname = $request->name;
        if(empty($Attrid) && empty($Attrname)) exit;
        //$getAttr = $this->woocommerceRepository->getAttrBySlug($slug);
        //$Attrid = $getAttr->attribute_id;
        $getTermsMeta = get_terms_by_attr_id($Attrid);
        return view('woocommerce::partials.load_attribute_ajax', compact('getTermsMeta', 'Attrid', 'Attrname'));
    }

    public function getSettings(){
        page_title()->setTitle(trans('woocommerce::woocommerce.setting'));
        check_insert_default_settings();

        $getCur = get_all_currency();

        return view('woocommerce::setting', compact('getCur'));
    }

    public function postSettings(Request $request){
        if($request->submit === 'save'){
            $data = [
                '_currency' => $request->currency,
                '_currency_pos' => $request->currency_pos,
                '_price_thousand_sep' => $request->price_thousand_sep,
            ];
            $this->woocommerceRepository->updateSettings($data);
        }
        return redirect()->route('woocommerce.setting')->with('success_msg', trans('bases::notices.update_success_message'));
    }

    public function postAttributeAjax(Request $request){
        $postID = $request->post_id;
        $data = [
            'a' => $request->boxAttr,
            'v' => $request->metaValAttribute,
            'c' => $request->metaCheck
        ];
        $data = json_encode($data);
        $this->woocommerceRepository->createOrUpdatePostMetaAttribute($postID, $data);
        return [
            'error' => false,
        ];
    }
}
