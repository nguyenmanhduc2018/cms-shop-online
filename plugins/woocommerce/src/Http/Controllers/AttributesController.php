<?php

namespace Botble\Woocommerce\Http\Controllers;

use Assets;
use Botble\Woocommerce\Http\Requests\AttributesRequest;
use Botble\Woocommerce\Repositories\Interfaces\AttributesInterface;
use Botble\Woocommerce\Repositories\Interfaces\WoocommerceInterface;
use Botble\Base\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use MongoDB\Driver\Exception\Exception;
use Botble\Woocommerce\Http\DataTables\AttributesDataTable;
use Botble\Language\Repositories\Interfaces\LanguageMetaInterface;

class AttributesController extends BaseController
{
	/**
     * @var AttributesInterface
     */
    protected $attributesRepository;

    /**
     * @var WoocommerceInterface
     */
    protected $woocommerceRepository;

    /**
     * @var LanguageMetaInterface
     */
    protected $languageMetaInterface;

    /**
     * AttributesController constructor.
     * @param AttributesController $woocommerceRepository
     * @author Duc Nguyen
     */
    public function __construct(AttributesInterface $attributesRepository, WoocommerceInterface $woocommerceRepository, LanguageMetaInterface $languageMetaInterface)
    {
        $this->attributesRepository = $attributesRepository;
        $this->woocommerceRepository = $woocommerceRepository;
        $this->languageMetaInterface = $languageMetaInterface;
    }

    /**
     * Display all woocommerce
     * @param WoocommerceDataTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Duc Nguyen
     */
    public function getList(AttributesDataTable $dataTable)
    {
        page_title()->setTitle(trans('woocommerce::attribute.list'));

        return $dataTable->renderTable(['title' => trans('woocommerce::attribute.list')]);
    }

    /**
     * Show create form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Duc Nguyen
     */
    public function getCreate(){
    	page_title()->setTitle(trans('woocommerce::attribute.create'));

        Assets::addAppModule(['slug']);

        $listAttribute = $this->woocommerceRepository->getAllAttribute();

        return view('woocommerce::attribute.create', compact('listAttribute'));
    }


    /**
     * Insert new Attribute into database
     *
     * @param AttributesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @author Duc Nguyen
     */
    public function postCreate(AttributesRequest $request)
    {	

        $attribute = $this->attributesRepository->createOrUpdate($request->input());

        do_action(BASE_ACTION_AFTER_CREATE_CONTENT, WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME, $request, $attribute);

        if ($request->input('submit') === 'save') {
            return redirect()->route('woocommerce.attributes.create')->with('success_msg', trans('bases::notices.create_success_message'));
        } else {
            return redirect()->route('woocommerce.attributes.edit', $attribute->attribute_id)->with('success_msg', trans('bases::notices.create_success_message'));
        }
    }

    /**
     * Show edit form
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Duc Nguyen
     */
    public function getEdit($id)
    {

        page_title()->setTitle(trans('woocommerce::attribute.edit') . ' #' . $id);

        Assets::addAppModule(['slug']);

        $attribute = $this->attributesRepository->findById($id);

        $listAttribute = $this->woocommerceRepository->getAllAttribute();

        return view('woocommerce::attribute.edit', compact('listAttribute', 'attribute'));
    }

    /**
     * @param $id
     * @param AttributesRequest $request
     * @return \Illuminate\Http\AttributesRequest
     * @author Duc Nguyen
     */
    public function postEdit($id, AttributesRequest $request)
    {

        $attribute = $this->attributesRepository->findById($id);
        $data = $request->input();
        empty($data['attribute_public']) ? $data['attribute_public'] = 0 : $data['attribute_public'];
        $attribute->fill($data);
       
        $this->attributesRepository->createOrUpdate($attribute);

        do_action(BASE_ACTION_AFTER_UPDATE_CONTENT, WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME, $request, $attribute);

        if ($request->input('submit') === 'save') {
            // return redirect()->route('woocommerce.attributes.create')->with('success_msg', trans('bases::notices.update_success_message'));
            return redirect()->route('woocommerce.attributes.edit', $id)->with('success_msg', trans('bases::notices.update_success_message'));
        }
        
    }


    /**
     * @param $id
     * @param Request $request
     * @return array
     * @author Duc Nguyen
     */
    public function getDelete(Request $request, $id)
    {

        try {
            $attribute = $this->attributesRepository->findById($id);
            if (!$attribute->is_default) {
            	$this->languageMetaInterface->deleteBy([['content_id', $id], ['reference', 'attribute']]);
                $this->attributesRepository->delete($attribute);
                do_action(BASE_ACTION_AFTER_DELETE_CONTENT, WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME, $request, $attribute);
            }

            return [
                'error' => false,
                'message' => trans('bases::notices.deleted'),
                'link' => route('woocommerce.attributes.create'),
            ];
        } catch (Exception $ex) {
            return [
                'error' => true,
                'message' => trans('bases::notices.cannot_delete'),
            ];
        }
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @author Duc Nguyen
     */
    public function postDeleteMany(Request $request)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return [
                'error' => true,
                'message' => trans('bases::notices.no_select'),
            ];
        }

        foreach ($ids as $id) {
        	$this->languageMetaInterface->deleteBy([['content_id', $id], ['reference', 'attribute']]);
            $attribute = $this->attributesRepository->findById($id);
            if (!$attribute->is_default) {
                $this->attributesRepository->delete($attribute);

                do_action(BASE_ACTION_AFTER_DELETE_CONTENT, WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME, $request, $attribute);
            }
        }

        return [
            'error' => false,
            'message' => trans('bases::notices.delete_success_message'),
        ];
    }

    /**
     * Show create form Terms
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Duc Nguyen
     */
    public function getCreateTerms($id){
    	page_title()->setTitle(trans('woocommerce::attribute.create'));

        Assets::addAppModule(['slug']);

        $listTerms = $this->woocommerceRepository->getTermsByAttrID($id);

        return view('woocommerce::attribute.terms.create', compact('listTerms'));
    }


    /**
     * Delete many Terms
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Duc Nguyen
     */

    public function postDeletePermanentlyManyTerms(Request $request){
    	dd($request->input());
    }
}