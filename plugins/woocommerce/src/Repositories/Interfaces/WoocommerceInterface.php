<?php

namespace Botble\Woocommerce\Repositories\Interfaces;

use Botble\Support\Repositories\Interfaces\RepositoryInterface;

interface WoocommerceInterface extends RepositoryInterface
{

	/**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

	public function countUnread();

	/**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

	public function getAllAttribute($select = ['*']);

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function createPostMeta($post_id, array $collection);

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function updatePostMeta($post_id, array $collection);


     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function getAttrBySlug($slug);


     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function getTermsByAttrID($id);

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function getPostMetaByID($id);

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function getCategoryByPostID($id);

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function checkInsertDefaultSettings();

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function getAllCurrent();

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function getCurrent();

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function getUnitCurrent($cur);

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function updateSettings(array $collection);

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function createOrUpdatePostMetaAttribute($post_id, $json);

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function getPostAttributeMetaByID($post_id);

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function getAttributeNameByID($attr_id);

     /**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

     public function getTermsNameByTermID($term_id);
     
}
