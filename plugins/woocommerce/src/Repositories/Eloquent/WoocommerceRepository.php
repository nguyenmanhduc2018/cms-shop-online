<?php

namespace Botble\Woocommerce\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Woocommerce\Repositories\Interfaces\WoocommerceInterface;
use Botble\Woocommerce\Models\Order;
use Botble\Woocommerce\Models\Attribute;
use Botble\Woocommerce\Models\PostMeta;
use Botble\Woocommerce\Models\Terms;
use Botble\Woocommerce\Models\Categories;
use Botble\Woocommerce\Models\Settings;

class WoocommerceRepository extends RepositoriesAbstract implements WoocommerceInterface
{
	/**
     * 
     * @return mixed
     * @author Duc Nguyen
     */

	public function countUnread(){
		$list = Order::select('order_id')->get();
		foreach($list as $value){
			$data[] = $value->order_id;
		}
        $this->resetModel();
        return $data;
	}

	public function getAllAttribute($select = ['*']){
		$data = Attribute::select($select);
		$data = apply_filters(BASE_FILTER_BEFORE_GET_FRONT_PAGE_ITEM ,$data, new Attribute(), WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME);
		$data = $data->get();

		$this->resetModel();
        return $data;
	}

	public function createPostMeta($post_id, array $collection){
		foreach ($collection as $key => $value) {
			//if(!empty($ex[1])){
			$ins = new PostMeta;
			$ins->post_id = $post_id;
			$ins->meta_key = $key;
			$ins->meta_value = $value;
			$ins->save();
			//}
		}
	}

	public function updatePostMeta($post_id, array $collection){
		foreach ($collection as $key => $value) {
			PostMeta::where('post_id', $post_id)->where('meta_key', $key)->update(['meta_value' => $value]);
		}
	}

	public function updateSettings(array $collection){
		foreach ($collection as $key => $value) {
			Settings::where('meta_key', $key)->update(['meta_value' => $value]);
		}
	}

	public function getAttrBySlug($slug){
		$data = Attribute::where('attribute_name', $slug)->first();
		return $data;
	}

	public function getTermsByAttrID($id){
		$data = Terms::where('attribute_id', $id)->get();
		return $data;
	}

	public function getPostMetaByID($id){
		$listMeta = PostMeta::select('meta_key', 'meta_value')->where('post_id', $id)->get();
		return $listMeta;
	}

	public function getCategoryByPostID($id){
        $data = Categories::where('post_category.post_id', '=', $id)
        		->join('categories', 'categories.id', '=', 'post_category.category_id')
        		->select('categories.*');
        $data = apply_filters(WOO_CATE_BASE_FILTER_BEFORE_GET_FRONT_PAGE_ITEM, $data, new Categories(), CATEGORY_MODULE_SCREEN_NAME);
        $data = $data->get();
        return $data;
	}

	public function checkInsertDefaultSettings(){
		$setting = Settings::where('meta_key', '_currency')->first();
		if(empty($setting)){
			$data = [
				[
			        'meta_key' => '_currency',
			        'meta_value' => 'VND',
			    ],
			    [
			        'meta_key' => '_currency_pos',
			        'meta_value' => 'right_space',
			    ],
			    [
			        'meta_key' => '_price_thousand_sep',
			        'meta_value' => ',',
			    ],
			];
			Settings::insert($data);
		}
	}

	public function getCurrent(){
		$cur = Settings::select('meta_value')->where('meta_key', '_currency')->first();
		if(!empty($cur))
			return $cur->meta_value;
		else
			return;
	}

	public function getAllCurrent(){
		$collection = Settings::select('meta_key', 'meta_value')->get();
		foreach ($collection as $value) {
			$data[$value->meta_key] = $value->meta_value;
 		}
		
		return (object) $data;
	}

	public function getUnitCurrent($cur){
		$data = ['AED' => 'د.إ','AFN' => '؋','ALL' => 'L','AMD' => 'AMD','ANG' => 'ƒ','AOA' => 'Kz','ARS' => '$','AUD' => '$','AWG' => 'Afl.','AZN' => 'AZN','BAM' => 'KM','BBD' => '$','BDT' => '৳','BGN' => 'лв.','BHD' => '.د.ب','BIF' => 'Fr','BMD' => '$','BND' => '$','BOB' => 'Bs.','BRL' => 'R$','BSD' => '$','BTC' => '฿','BTN' => 'Nu.','BWP' => 'P','BYR' => 'Br','BZD' => '$','CAD' => '$','CDF' => 'Fr','CHF' => 'CHF','CLP' => '$','CNY' => '¥','COP' => '$','CRC' => '₡','CUC' => '$','CUP' => '$','CVE' => '$','CZK' => 'Kč','DJF' => 'Fr','DKK' => 'DKK','DOP' => 'RD$','DZD' => 'د.ج','EGP' => 'EGP','ERN' => 'Nfk','ETB' => 'Br','EUR' => '€','FJD' => '$','FKP' => '£','GBP' => '£','GEL' => 'ლ','GGP' => '£','GHS' => '₵','GIP' => '£','GMD' => 'D','GNF' => 'Fr','GTQ' => 'Q','GYD' => '$','HKD' => '$','HNL' => 'L','HRK' => 'Kn','HTG' => 'G','HUF' => 'Ft','IDR' => 'Rp','ILS' => '₪','IMP' => '£','INR' => '₹','IQD' => 'ع.د','IRR' => '﷼','IRT' => 'تومان','ISK' => 'kr.','JEP' => '£','JMD' => '$','JOD' => 'د.ا','JPY' => '¥','KES' => 'KSh','KGS' => 'сом','KHR' => '៛','KMF' => 'Fr','KPW' => '₩','KRW' => '₩','KWD' => '.ك','KYD' => '$','KZT' => 'KZT','LAK' => '₭','LBP' => 'ل.ل','LKR' => 'රු','LRD' => '$','LSL' => 'L','LYD' => 'ل.د','MAD' => 'د.م.','MDL' => 'MDL','MGA' => 'Ar','MKD' => 'ден','MMK' => 'Ks','MNT' => '₮','MOP' => 'P','MRO' => 'UM','MUR' => '₨','MVR' => '.ރ','MWK' => 'MK','MXN' => '$','MYR' => 'RM','MZN' => 'MT','NAD' => '$','NGN' => '₦','NIO' => 'C$','NOK' => 'kr','NPR' => '₨','NZD' => '$','OMR' => 'ر.ع.','PAB' => 'B/.','PEN' => 'S/.','PGK' => 'K','PHP' => '₱','PKR' => '₨','PLN' => 'ł','PRB' => 'р.','PYG' => '₲','QAR' => 'ر.ق','RON' => 'lei','RSD' => 'дин.','RUB' => '₽','RWF' => 'Fr','SAR' => 'ر.س','SBD' => '$','SCR' => '₨','SDG' => 'ج.س.','SEK' => 'kr','SGD' => '$','SHP' => '£','SLL' => 'Le','SOS' => 'h','SRD' => '$','SSP' => '£','STD' => 'Db','SYP' => 'ل.س','SZL' => 'L','THB' => '฿','TJS' => 'ЅМ','TMT' => 'm','TND' => 'د.ت','TOP' => 'T$','TRY' => '₺','TTD' => '$','TWD' => 'NT$','TZS' => 'Sh','UAH' => '₴','UGX' => 'UGX','USD' => '$','UYU' => '$','UZS' => 'UZS','VEF' => 'Bs F','VND' => '₫','VUV' => 'Vt','WST' => 'T','XAF' => 'CFA','XCD' => '$','XOF' => 'CFA','XPF' => 'Fr','YER' => '﷼','ZAR' => 'R','ZMW' => 'ZK'];
		if(!empty($data[$cur]))
			return $data[$cur];
		else
			return 'N/A';
	}

	public function createOrUpdatePostMetaAttribute($post_id, $json){
		if($post_id === ''){
			$getLastID = $this->model->latest('id')->first();
			$post_id = $getLastID->id + 1;

		}
		$getKey = PostMeta::where('post_id', $post_id)->where('meta_key', '_attritube')->first();
		if(empty($getKey)){
			$ins = new PostMeta;
			$ins->post_id = $post_id;
			$ins->meta_key = '_attritube';
			$ins->meta_value = $json;
			$ins->save();
		}else{
			PostMeta::where('post_id', $post_id)->where('meta_key', '_attritube')->update(['meta_value' => $json]);
		}
	}

	public function getPostAttributeMetaByID($post_id){
		$value = PostMeta::where('post_id', $post_id)->where('meta_key', '_attritube')->first();
		if(empty($value))
			return;
		else
			return $value->meta_value;
	}

	public function getAttributeNameByID($attr_id){
		$attr_id = (int) $attr_id;
		$get = Attribute::select('attribute_label')->where('id', $attr_id)->first();
		return $get->attribute_label;
	}

	public function getTermsNameByTermID($term_id){
		$term_id = (int) $term_id;
		$get = Terms::select('name')->where('term_id', $term_id)->first();
		return $get->name;
	}
}
