<?php

namespace Botble\Woocommerce\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\Support\Services\Cache\CacheInterface;
use Botble\Woocommerce\Repositories\Interfaces\WoocommerceInterface;

class WoocommerceCacheDecorator extends CacheAbstractDecorator implements WoocommerceInterface
{
    /**
     * @var WoocommerceInterface
     */
    protected $repository;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * WoocommerceCacheDecorator constructor.
     * @param WoocommerceInterface $repository
     * @param CacheInterface $cache
     * @author Sang Nguyen
     */
    public function __construct(WoocommerceInterface $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }
}
