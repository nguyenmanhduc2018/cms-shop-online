<?php

namespace Botble\Woocommerce\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\Support\Services\Cache\CacheInterface;
use Botble\Woocommerce\Repositories\Interfaces\AttributesInterface;

class WoocommerceCacheDecorator extends CacheAbstractDecorator implements AttributesInterface
{
    /**
     * @var AttributesInterface
     */
    protected $repository;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * WoocommerceCacheDecorator constructor.
     * @param AttributesInterface $repository
     * @param CacheInterface $cache
     * @author Sang Nguyen
     */
    public function __construct(AttributesInterface $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }
}
