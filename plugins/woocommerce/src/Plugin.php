<?php

namespace Botble\Woocommerce;

use Artisan;
use Botble\Base\Supports\Commands\Permission;
use Schema;
use Botble\Base\Interfaces\PluginInterface;

class Plugin implements PluginInterface
{

    /**
     * @return array
     * @author Sang Nguyen
     */
    public static function permissions()
    {
        return [
            [
                'name' => 'Woocommerce',
                'flag' => 'woocommerce.list',
                'is_feature' => true,
            ],
            [
                'name' => 'Create',
                'flag' => 'woocommerce.create',
                'parent_flag' => 'woocommerce.list',
            ],
            [
                'name' => 'Edit',
                'flag' => 'woocommerce.edit',
                'parent_flag' => 'woocommerce.list',
            ],
            [
                'name' => 'Delete',
                'flag' => 'woocommerce.delete',
                'parent_flag' => 'woocommerce.list',
            ]
        ];
    }

    /**
     * @author Sang Nguyen
     */
    public static function activate()
    {
        Permission::registerPermission(self::permissions());
        Artisan::call('migrate', [
            '--force' => true,
            '--path' => 'plugins/woocommerce/database/migrations',
        ]);
    }

    /**
     * @author Sang Nguyen
     */
    public static function deactivate()
    {

    }

    /**
     * @author Sang Nguyen
     */
    public static function remove()
    {
        Permission::removePermission(self::permissions());
        Schema::dropIfExists('woocommerce');
    }
}