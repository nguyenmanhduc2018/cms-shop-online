<?php

namespace Botble\Woocommerce\Models;

use Eloquent;

/**
 * Botble\Woocommerce\Models\Woocommerce
 *
 * @mixin \Eloquent
 */
class Categories extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_category';

    protected $hidden = ['deleted_at'];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $fillable = [
        'id',
        'category_id',
        'post_id'
    ];
}
