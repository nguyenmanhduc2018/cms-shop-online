<?php

namespace Botble\Woocommerce\Models;

use Eloquent;

/**
 * Botble\Woocommerce\Models\Woocommerce
 *
 * @mixin \Eloquent
 */
class Attribute extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'woocommerce_attribute_taxonomies';

    protected $hidden = ['deleted_at'];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $fillable = [
        'id',
        'attribute_name',
        'attribute_label',
        'attribute_type',
        'attribute_public',
    ];
}
