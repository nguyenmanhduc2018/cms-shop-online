<?php

namespace Botble\Woocommerce\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Botble\Woocommerce\Models\Woocommerce
 *
 * @mixin \Eloquent
 */
class Order extends Eloquent
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'woocommerce_order_items';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];

    /**
     * The date fields for the model.clear
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'order_item_id',
        'order_item_name',
        'order_item_type',
        'order_id',
    ];
}
