<?php

namespace Botble\Woocommerce\Models;

use Eloquent;

/**
 * Botble\Woocommerce\Models\Woocommerce
 *
 * @mixin \Eloquent
 */
class Terms extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'woocommerce_terms';

    protected $hidden = ['deleted_at'];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $fillable = [
        'term_id',
        'name',
        'slug',
        'attribute_id'
    ];
}
