<?php

namespace Botble\Woocommerce\Models;

use Eloquent;

/**
 * Botble\Woocommerce\Models\Woocommerce
 *
 * @mixin \Eloquent
 */
class PostMeta extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_meta';

    protected $hidden = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $fillable = [
        'id',
        'post_id',
        'meta_key',
        'meta_value'
    ];
}
