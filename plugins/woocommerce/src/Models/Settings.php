<?php

namespace Botble\Woocommerce\Models;

use Eloquent;

/**
 * Botble\Woocommerce\Models\Woocommerce
 *
 * @mixin \Eloquent
 */
class Settings extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'woocommerce_settings';

    protected $hidden = ['deleted_at'];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $fillable = [
        'id',
        'meta_key',
        'meta_value'
    ];
}
