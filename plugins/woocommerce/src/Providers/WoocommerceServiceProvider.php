<?php

namespace Botble\Woocommerce\Providers;

use Botble\Base\Events\SessionStarted;
use Botble\Woocommerce\Models\Woocommerce;
use Botble\Woocommerce\Models\Attribute;
use Illuminate\Support\ServiceProvider;
use Botble\Woocommerce\Repositories\Caches\WoocommerceCacheDecorator;
use Botble\Woocommerce\Repositories\Eloquent\WoocommerceRepository;
use Botble\Woocommerce\Repositories\Interfaces\WoocommerceInterface;
use Botble\Woocommerce\Repositories\Caches\AttributesCacheDecorator;
use Botble\Woocommerce\Repositories\Eloquent\AttributesRepository;
use Botble\Woocommerce\Repositories\Interfaces\AttributesInterface;
use Botble\Support\Services\Cache\Cache;
use Botble\Blog\Providers\HookServiceProvider;
use Botble\Woocommerce\Providers\OrderServiceProvider;
use Botble\Base\Supports\Helper;
use Event;
use Language;

class WoocommerceServiceProvider extends ServiceProvider
{
    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    /**
     * @author Sang Nguyen
     */
    public function register()
    {
        if (setting('enable_cache', false)) {
            $this->app->singleton(WoocommerceInterface::class, function () {
                return new WoocommerceCacheDecorator(new WoocommerceRepository(new Woocommerce()), new Cache($this->app['cache'], WoocommerceRepository::class));
            });
            $this->app->singleton(AttributesInterface::class, function () {
                return new AttributesCacheDecorator(new AttributesRepository(new Attribute()), new Cache($this->app['cache'], AttributesRepository::class));
            });
        } else {
            $this->app->singleton(WoocommerceInterface::class, function () {
                return new WoocommerceRepository(new Woocommerce());
            });
            $this->app->singleton(AttributesInterface::class, function () {
                return new AttributesRepository(new Attribute());
            });
        }

        Helper::autoload(__DIR__ . '/../../helpers');
    }

    /**
     * @author Sang Nguyen
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'woocommerce');
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
        $this->mergeConfigFrom(__DIR__ . '/../../config/woocommerce.php', 'woocommerce');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'woocommerce');

        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

            $this->publishes([__DIR__ . '/../../resources/views' => resource_path('views/vendor/woocommerce')], 'views');
            $this->publishes([__DIR__ . '/../../resources/lang' => resource_path('lang/vendor/woocommerce')], 'lang');
            $this->publishes([__DIR__ . '/../../config/woocommerce.php' => config_path('woocommerce.php')], 'config');
        }

        $this->app->register(HookServiceProvider::class);
        $this->app->register(OrderServiceProvider::class);

        Event::listen(SessionStarted::class, function () {
            dashboard_menu()->registerItem([
                'id' => 'cms-plugins-woocommerce',
                'priority' => 7,
                'parent_id' => null,
                'name' => trans('woocommerce::woocommerce.name'),
                'icon' => 'fa fa-opencart',
                'url' => route('woocommerce.list'),
                'permissions' => ['woocommerce.list'],
            ])
            ->registerItem([
                'id' => 'cms-plugins-woocommerce-allproduct',
                'priority' => 1,
                'parent_id' => 'cms-plugins-woocommerce',
                'name' => trans('woocommerce::woocommerce.allproduct'),
                'icon' => null,
                'url' => route('woocommerce.list'),
                'permissions' => ['woocommerce.list'],
            ])
            ->registerItem([
                'id' => 'cms-plugins-woocommerce-order',
                'priority' => 2,
                'parent_id' => 'cms-plugins-woocommerce',
                'name' => trans('woocommerce::woocommerce.order'),
                'icon' => null,
                'url' => route('woocommerce.order'),
                'permissions' => ['woocommerce.order'],
            ])
            ->registerItem([
                'id' => 'cms-plugins-woocommerce-category',
                'priority' => 3,
                'parent_id' => 'cms-plugins-woocommerce',
                'name' => trans('woocommerce::woocommerce.category'),
                'icon' => null,
                'url' => route('woocommerce.categories'),
                'permissions' => ['woocommerce.categories'],
            ])
            ->registerItem([
                'id' => 'cms-plugins-woocommerce-attributes',
                'priority' => 4,
                'parent_id' => 'cms-plugins-woocommerce',
                'name' => trans('woocommerce::woocommerce.attributes'),
                'icon' => null,
                'url' => route('woocommerce.attributes'),
                'permissions' => ['woocommerce.attributes'],
            ])
            ->registerItem([
                'id' => 'cms-plugins-woocommerce-settings',
                'priority' => 5,
                'parent_id' => 'cms-plugins-woocommerce',
                'name' => trans('woocommerce::woocommerce.setting'),
                'icon' => null,
                'url' => route('woocommerce.setting'),
                'permissions' => ['woocommerce.setting'],
            ]);
        });
        
        add_filter(WOO_CATE_BASE_FILTER_BEFORE_GET_FRONT_PAGE_ITEM, [$this, 'checkItemLanguageBeforeShowCATE'], 50, 3);
        
        add_filter(WOO_FILTER_SCRIPT_CONTENT_PAGE, [$this, 'contentScriptFilterWoocom'], 120);
        add_filter(WOO_FILTER_LOAD_ATTRIBUTE_PAGE, [$this, 'contentLoadAttribute'], 120);
    }

    public function checkItemLanguageBeforeShowCATE($data, $model, $screen)
    {
        $table = $model->getTable();
        return $data->join('language_meta', 'language_meta.content_id', $table . '.category_id')
            ->where('language_meta.reference', '=', $screen)
            ->where('language_meta.code', '=', Language::getCurrentLocaleCode());
    }

    public function contentScriptFilterWoocom($options){
        return $options . view('woocommerce::partials.script')->render();
    }

    public function contentLoadAttribute($id){
        $getPostAttrMeta = get_post_attribute_meta_by_id($id);
        $getPostAttrMeta = json_decode($getPostAttrMeta);
        return view('woocommerce::partials.load_attribute', compact('getPostAttrMeta'))->render();
    }

}

