<?php

namespace Botble\Woocommerce\Providers;

use Illuminate\Support\ServiceProvider;
use Botble\Woocommerce\Repositories\Interfaces\WoocommerceInterface;

class OrderServiceProvider extends ServiceProvider
{
	/**
     * Boot the service provider.
     * @author Sang Nguyen
     */
    public function boot()
    {
        add_filter(BASE_FILTER_TOP_HEADER_LAYOUT_ORDER, [$this, 'orderTopNotification'], 120);
    }

    public function orderTopNotification($options){

    	if (acl_get_current_user()->hasPermission('woocommerce.edit')) {
            $orders = app(WoocommerceInterface::class)->countUnread();
    		$orders = array_unique($orders);
            return $options . view('woocommerce::partials.notification', compact('orders'))->render();
        }
        return null;
    }
}