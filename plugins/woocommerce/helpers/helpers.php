<?php

use Botble\Base\Supports\SortItemsWithChildrenHelper;
use Botble\Woocommerce\Repositories\Interfaces\WoocommerceInterface;

if (!function_exists('check_insert_default_settings')) {
    /**
     * @param $limit
     * @return mixed
     * @author Duc Nguyen
     */
    function check_insert_default_settings()
    {
        return app(WoocommerceInterface::class)->checkInsertDefaultSettings();
    }
}

if (!function_exists('get_all_currency')) {
    /**
     * @param $limit
     * @return mixed
     * @author Duc Nguyen
     */
    function get_all_currency()
    {
        return app(WoocommerceInterface::class)->getAllCurrent();
    }
}

if (!function_exists('get_currency')) {
    /**
     * @param $limit
     * @return mixed
     * @author Duc Nguyen
     */
    function get_currency()
    {
        return app(WoocommerceInterface::class)->getCurrent();
    }
}

if (!function_exists('get_unitcurrency')) {
    /**
     * @param $limit
     * @return mixed
     * @author Duc Nguyen
     */
    function get_unitcurrency($cur)
    {
        return app(WoocommerceInterface::class)->getUnitCurrent($cur);
    }
}

if (!function_exists('get_post_attribute_meta_by_id')) {
    /**
     * @param $limit
     * @return mixed
     * @author Duc Nguyen
     */
    function get_post_attribute_meta_by_id($id)
    {
        return app(WoocommerceInterface::class)->getPostAttributeMetaByID($id);
    }
}

if (!function_exists('get_attribute_name_by_attr_id')) {
    /**
     * @param $limit
     * @return mixed
     * @author Duc Nguyen
     */
    function get_attribute_name_by_attr_id($attr_id)
    {
        return app(WoocommerceInterface::class)->getAttributeNameByID($attr_id);
    }
}

if (!function_exists('get_terms_name_by_term_id')) {
    /**
     * @param $limit
     * @return mixed
     * @author Duc Nguyen
     */
    function get_terms_name_by_term_id($term_id)
    {
        return app(WoocommerceInterface::class)->getTermsNameByTermID($term_id);
    }
}

if (!function_exists('get_terms_by_attr_id')) {
    /**
     * @param $limit
     * @return mixed
     * @author Duc Nguyen
     */
    function get_terms_by_attr_id($attr_id)
    {
        return app(WoocommerceInterface::class)->getTermsByAttrID($attr_id);
    }
}


if (!function_exists('get_terms_by_attr_id')) {
    /**
     * @param $limit
     * @return mixed
     * @author Duc Nguyen
     */
    function get_terms_by_attr_id($attr_id)
    {
        return app(WoocommerceInterface::class)->getTermsByAttrID($attr_id);
    }
}

