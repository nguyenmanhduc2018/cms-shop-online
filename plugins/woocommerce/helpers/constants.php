<?php

if (!defined('WOOCOMMERCE_MODULE_SCREEN_NAME')) {
    define('WOOCOMMERCE_MODULE_SCREEN_NAME', 'woocommerce');
}

if (!defined('WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME')) {
    define('WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME', 'attribute');
}

if (!defined('BASE_FILTER_TOP_HEADER_LAYOUT_ORDER')) {
    define('BASE_FILTER_TOP_HEADER_LAYOUT_ORDER', 'woocommerce_order_notifi');
}

if (!defined('WOO_CATE_BASE_FILTER_BEFORE_GET_FRONT_PAGE_ITEM')) {
    define('WOO_CATE_BASE_FILTER_BEFORE_GET_FRONT_PAGE_ITEM', 'woocommerce_get_cate_item');
}

if (!defined('WOO_FILTER_SCRIPT_CONTENT_PAGE')) {
    define('WOO_FILTER_SCRIPT_CONTENT_PAGE', 'woocommerce_content_script');
}

if (!defined('WOO_FILTER_LOAD_ATTRIBUTE_PAGE')) {
    define('WOO_FILTER_LOAD_ATTRIBUTE_PAGE', 'woocommerce_content_load_attribute');
}




