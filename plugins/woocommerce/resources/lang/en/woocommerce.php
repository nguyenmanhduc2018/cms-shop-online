<?php

return [
    'name' => 'WooCommerce',
    'create' => 'New Product',
    'edit' => 'Edit Product',
    'list' => 'List Product',
    'allproduct' => 'All Products',
    'order' => 'Orders',
    'attributes' => 'Attributes',
    'category' => 'Categories',
    'sku' => 'Sku',
    'cate' => 'Category',
    'price' => 'Price',
    'setting' => 'Settings',
    'form' => [
    	'name_placeholder' => 'Product\'s name (Maximum 120 characters)',
    	'regular_price' => 'Regular price',
    	'sale_price' => 'Sale price',
    	'sale_price_date' => 'Sale price dates',
    	'_sku' => 'SKU',
    	'_stock_status' => 'Stock status',
        'currency' => 'Currency',
        'currency_pos' => 'Currency position',
        'price_thousand_sep' => 'Thousand separator'
   	],
    'restore' => 'Restore successfully.',
    'deleted' => 'Deleted successfully.',
];
