<?php
return [
	'create' => 'Create new attributes',
    'edit' => 'Edit attributes',
	'list' => 'List attributes',
	'slug' => 'Slug',
	'type' => 'Type',
	'terms' => 'Terms',
];