<li class="dropdown">
    <a href="{{ route('woocommerce.order') }}" style="padding-right: 10px;padding-left: 15px;" class="dropdown-toggle dropdown-header-name">
        <i class="fa fa-shopping-basket"></i>
        <span class="badge badge-default" style="background-color: #d23939;">{{ count($orders) }}</span>
    </a>
</li>