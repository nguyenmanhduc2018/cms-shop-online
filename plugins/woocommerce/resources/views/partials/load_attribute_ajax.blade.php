<div class="widget-title box_attribute box_{{$Attrid}}">
    <div class="pull-right">
        <a href="javascript:;" class="remove_row delete_{{$Attrid}}">Remove</a>
    </div>
    <div class="name_attribute_{{$Attrid}}" data-id="{{$Attrid}}">
        <div class="left">
            <strong class="attribute_name"><i class="fa fa-file-text-o"></i> {{ $Attrname }}</strong>
        </div>
    </div>
</div>
<div class="metabox-content show_{{$Attrid}}" style="display: none;">
    <table class="attribute" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td class="attribute_name">
                    <label>Name:</label>
                    <strong>{{ $Attrname }}</strong>
                </td>
                <td rowspan="3">
                    <label>Value(s):</label>
                    <select class="form-control js-example-basic-multiple-{{$Attrid}}" multiple="multiple" name="attribute_terms" style="width: 436px;">
                    </select>
                    <div>
                        <button type="button" onclick="selectAllAttr({{$Attrid}}, dataAttribute_{{$Attrid}});" class="form-control button plus select_all_attributes_{{$Attrid}}">Select all</button>
                        <button type="button" class="form-control button minus select_no_attributes_{{$Attrid}}">Select none</button>
                    {{-- <button type="button" class="form-control button fr plus tawcvs_add_new_attribute" data-type="color">Add new</button> --}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="mt-checkbox mt-checkbox-outline ">
                        <input type="checkbox" class="checkbox" checked="checked" name="attribute_visibility">
                        <span></span>
                        <span class="text">Visible on the product page</span>
                    </label>
                </td>
            </tr>
            {{-- <tr>
                <td>
                    <div style="display: none;">
                        <label class="mt-checkbox mt-checkbox-outline ">
                            <input type="checkbox" class="checkbox" checked="checked" name="attribute_variation[0]" value="1"> 
                            <span></span>
                            <span class="text">Used for variations</span>
                        </label>
                    </div>
                </td>
            </tr> --}}
        </tbody>
    </table>
    <script type="text/javascript">
        onloadClick({{$Attrid}});
        var dataAttribute_{{$Attrid}} = [
                @foreach ($getTermsMeta as $value)
                    {
                    id: '{{ $value->term_id }}',
                    text: '{{ $value->name }}'
                    },
                @endforeach
                ];
        $('select.js-example-basic-multiple-{{$Attrid}}').select2({
          placeholder: 'Select terms',
          allowClear: true,
          data: dataAttribute_{{$Attrid}}
        });

    </script>
</div>