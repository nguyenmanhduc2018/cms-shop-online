@for($i=0;$i<count($getPostAttrMeta->a); $i++)
<div class="widget-title box_attribute box_{{ $getPostAttrMeta->a[$i] }}">
    <div class="pull-right">
        <a href="javascript:;" class="remove_row delete_{{ $getPostAttrMeta->a[$i] }}">Remove</a>
    </div>
    <div class="name_attribute_{{ $getPostAttrMeta->a[$i] }}" data-id="{{ $getPostAttrMeta->a[$i] }}">
        <div class="left">
            <strong class="attribute_name"><i class="fa fa-file-text-o"></i> {{ get_attribute_name_by_attr_id($getPostAttrMeta->a[$i]) }}</strong>
        </div>
    </div>
</div>
<div class="metabox-content show_{{ $getPostAttrMeta->a[$i] }}" style="display: none;">
    <table class="attribute" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td class="attribute_name">
                    <label>Name:</label>
                    <strong>{{ get_attribute_name_by_attr_id($getPostAttrMeta->a[$i]) }}</strong>
                </td>
                <td rowspan="3">
                    <label>Value(s):</label>
                    <select class="form-control js-example-basic-multiple-{{ $getPostAttrMeta->a[$i] }}" multiple="multiple" name="attribute_terms" style="width: 436px;">
                    </select>
                    <div>
                        <button type="button" onclick="selectAllAttr({{ $getPostAttrMeta->a[$i] }}, dataAttribute_{{ $getPostAttrMeta->a[$i] }});" class="form-control button plus select_all_attributes_{{ $getPostAttrMeta->a[$i] }}">Select all</button>
                        <button type="button" class="form-control button minus select_no_attributes_{{ $getPostAttrMeta->a[$i] }}">Select none</button>
                    {{-- <button type="button" class="form-control button fr plus tawcvs_add_new_attribute" data-type="color">Add new</button> --}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="mt-checkbox mt-checkbox-outline ">
                        @if($getPostAttrMeta->c[$i] === 'true')
                        <input type="checkbox" class="checkbox" checked="checked" name="attribute_visibility">
                        @else
                        <input type="checkbox" class="checkbox" name="attribute_visibility">
                        @endif
                        <span></span>
                        <span class="text">Visible on the product page</span>
                    </label>
                </td>
            </tr>
            {{-- <tr>
                <td>
                    <div style="display: none;">
                        <label class="mt-checkbox mt-checkbox-outline ">
                            <input type="checkbox" class="checkbox" checked="checked" name="attribute_variation[0]" value="1"> 
                            <span></span>
                            <span class="text">Used for variations</span>
                        </label>
                    </div>
                </td>
            </tr> --}}
        </tbody>
    </table>
    <script type="text/javascript">
        $(document).ready(function() {
            onloadClick({{ $getPostAttrMeta->a[$i] }});
            dataAttribute_{{ $getPostAttrMeta->a[$i] }} = [
                    @foreach(get_terms_by_attr_id($getPostAttrMeta->a[$i]) as $value)
                        {
                        id: '{{ $value->term_id }}',
                        text: '{{ $value->name }}'
                        },
                    @endforeach
                    ];
            var data_{{ $getPostAttrMeta->a[$i] }} = [
                @for ($v = 0; $v < count($getPostAttrMeta->v[$i]); $v++)
                    '{{ $getPostAttrMeta->v[$i][$v] }}',
                @endfor
            ];
            var reloadSelect = $('select.js-example-basic-multiple-{{ $getPostAttrMeta->a[$i] }}').select2({
              placeholder: 'Select terms',
              allowClear: true,
              data: dataAttribute_{{ $getPostAttrMeta->a[$i] }}
            });
            reloadSelect.val(data_{{ $getPostAttrMeta->a[$i] }}).trigger('change');
        });
    </script>
</div>
@endfor