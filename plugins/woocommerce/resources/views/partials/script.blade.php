<style type="text/css">
    .attribute_name{
            width: 200px;
            text-align: left;
            padding: 0 6px 1em 0;
            vertical-align: top;
            border: 0;
    }
    .remove_row[class*="delete"]{
        color: #F44336;
    }
    .expand-close{
        font-style: italic;
    }
    .attribute_name label{
        font-weight: normal;
    }
    .save_attributes{
        width: inherit;
        margin-top: 25px;
    }
    table.attribute{
        margin-top: 20px;
    }
    table.attribute button{

        width: inherit;
        display: inline-block;
    }
    .loadingPage{
        display: none;
        background-color: rgba(255, 255, 255, 0.88);
        position: fixed;
        top: 0;
        z-index: 9999;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .loadingPage img{
        width: 150px;
        display: block;
        margin: 0 auto;
        margin-top: 20%;
    }
</style>
<div class="loadingPage">
    <img src="/vendor/media/images/loading.gif">
</div>
<script src="/vendor/core/packages/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $('.sale_schedule').click(function() {
        $(this).hide();
        $('.sale_price_dates_fields').fadeIn(100);
    });
    $('.cancel_sale_schedule').click(function() {
        $('.sale_schedule').fadeIn(100);
        $('.sale_price_dates_fields').fadeOut(1);
    });
    $('.expand_all').click(function(){
        $('.metabox-content').fadeIn(150);
    });
    $('.close_all').click(function(){
        $('.metabox-content').fadeOut(150);
    });

    function onloadClick(id){
        $('.name_attribute_'+id).click(function(){
            var id = $(this).attr('data-id');
            $(this).toggleClass('open');
            if($(this).hasClass('open')){
                $('.metabox-content.show_'+id).fadeIn(150);
            }else{
                $('.metabox-content.show_'+id).fadeOut(150);
            }
        });

        $('.remove_row.delete_'+id).click(function() {
            var _this = $(this);
            var name = _this.attr('data-name');
            var r = confirm("Remove this attribute.");
            if (r == true) {
                $('.loadingPage').fadeIn(100);
                $('.box_attribute.box_'+id).remove();
                $('.metabox-content.show_'+id).remove();
                $('select.attribute_taxonomy').find('option[data-id='+id+']').removeAttr('disabled');
                $('.loadingPage').fadeOut(500);
            }
        });

        $('.select_no_attributes_'+id).on('click',function() {
           $('select.js-example-basic-multiple-'+id).val(null).trigger("change");
        });
    }

    function selectAllAttr(id, data){
        var arr = [];
        data.forEach(function(name) {
            if (name.id) {
                arr.push(name.id);
            }
        });
        $('select.js-example-basic-multiple-'+id).select2({placeholder: 'Select terms',allowClear: true}).val(arr).trigger('change');
    }

    // Add attribute
    $('.add_attribute').click(function() {
        var select = $('select.attribute_taxonomy option:selected');
        var id = select.attr('data-id');
        var name = select.text();
        var checkDis = $('select.attribute_taxonomy').find('option:selected[disabled=disabled]');
        if(checkDis.length != 1){
            if(id != undefined){
                $('.loadingPage').fadeIn(100);
                select.attr('disabled', 'disabled');
                var text = select.text();
                $.ajax({
                    url: '{{ route('woocommerce.attributes_meta') }}',
                    type: 'POST',
                    data: {id: id, name: name},
                    success: function(e){
                        $('.loadAttribute').append(e);
                        $('.loadingPage').fadeOut(500);
                    }
                })
            } 
        }      
    });

    var metaValAttribute = [];
    var metaCheck = [];
    var boxAttr = [];

    $('.save_attributes').click(function() {
        $('[class*="name_attribute_"').each(function(){
            boxAttr.push($(this).attr('data-id'));
        });
        $('select[name="attribute_terms"]').each(function(){
            metaValAttribute.push($(this).val());
        });
        $('input[name="attribute_visibility"]').each(function(){
            metaCheck.push($(this).is(':checked'));
        });
        var postID = $('.postId').attr('tab-post-id');
        if(postID == undefined){
            postID = '';
        }

        $('.loadingPage').fadeIn(100);
        $.ajax({
             url: '{{ route('woocommerce.attributtes_ajax') }}',
             type: 'POST',
             dataType: 'json',
             data: {post_id: postID, boxAttr: boxAttr, metaValAttribute:metaValAttribute, metaCheck:metaCheck},
             success: function(e){
                if(e.error == false){
                    metaValAttribute = [];
                    metaCheck = [];
                    boxAttr = [];
                    $('.loadingPage').fadeOut(500);
                }   
             }
         }); 
           
    });

    //end Add attribute
    $('.input-daterange').datepicker({
        format: 'yyyy-mm-dd',
    });
</script>