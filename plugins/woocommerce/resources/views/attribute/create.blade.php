@extends('bases::layouts.master')
@section('content')
    {!! Form::open() !!}
    @php do_action(BASE_ACTION_CREATE_CONTENT_NOTIFICATION, WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME, request(), null) @endphp
    <div class="row">
        <div class="col-md-5">
            <div class="main-form">
                <div class="form-body">
	            	<div class="form-group @if ($errors->has('attribute_label')) has-error @endif">
	                    <label for="attribute_label" class="control-label required">{{ trans('bases::forms.name') }}</label>
	                    {!! Form::text('attribute_label', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Name for the attribute (shown on the front-end).', 'data-counter' => 120]) !!}
	                    {!! Form::error('attribute_label', $errors) !!}
	                </div>
	                <div class="form-group">
	                    {!! Form::permalink('attribute_name', old('attribute_name'), null, route('categories.create.slug'), route('public.single.detail', config('cms.slug.pattern')), url('/')) !!}
	                    {!! Form::error('attribute_name', $errors) !!}
	                </div>
	                <div class="form-group">
	                	<label class="mt-checkbox mt-checkbox-outline ">
	                        <input type="checkbox" class="checkbox" name="attribute_public" value="1">
	                        <span></span>
	                        <span class="text">Enable Archives?</span>
	                    </label>
	                    <br>
	                    <i>Enable this if you want this attribute to have product archives in your store.</i>
	                </div>
	                <div class="form-group @if ($errors->has('attribute_type')) has-error @endif">
	                    <label for="attribute_type" class="control-label">Type</label>
	                    <select class="form-control" id="attribute_type" name="attribute_type">
	                    	<option value="select" disabled="disabled">Select</option>
	                    	<option value="text" selected="selected">Text</option>
	                    	<option value="color" disabled="disabled">Color</option>
	                    	<option value="image" disabled="disabled">Image</option>
	                    	<option value="label" disabled="disabled">Label</option>
	                    </select>
	                    {!! Form::error('attribute_type', $errors) !!}
	                    <br>
	                    <i>Determines how you select attributes for products. <b>Text</b> allows manual entry whereas select allows pre-configured terms in a drop-down list.</i>
	                </div>
	                @php do_action(BASE_ACTION_META_BOXES, CATEGORY_MODULE_SCREEN_NAME, 'top') @endphp
                	@php do_action(BASE_ACTION_META_BOXES, CATEGORY_MODULE_SCREEN_NAME, 'side') @endphp
	                <div class="form-group">
	                	<div class="toolbar">
	                        <button type="submit" name="submit" class="form-control button add_new_attribute btn btn-primary" value="save">Add attribute</button>
	                    </div>
	                </div>
            	</div>
        	</div>
    	</div>
    	<div class="col-md-7">
        	<div class="main-form">
            	<div class="form-body">
            		<table class="table table-striped">
					    <thead>
					      <tr>
					        <th>Name</th>
					        <th>Slug</th>
					        <th>Type</th>
					        <th>Terms</th>
					      </tr>
					    </thead>
					    <tbody>
					   	@foreach($listAttribute as $item)
					      <tr class="alternate">
					        <td>
					        	{!! anchor_link(route('woocommerce.attributes.edit', $item->content_id), $item->attribute_label) !!}
					        	<div class="clearfix"></div>
					        	<div class="row-actions">
					        		<span class="edit">
					        			<a href="{{ route('woocommerce.attributes.edit', $item->attribute_id) }}">Edit</a> | 
					        		</span>
					        		<span class="delete">
					        			<a href="javascript:;" data-href="{{ route('woocommerce.attributes.delete', $item->content_id) }}">Delete</a>
					        		</span>
					        	</div>
					        </td>
					        <td>{{ $item->attribute_name }}</td>
					        @if($item->attribute_public === 1)
					        <td><span style="text-transform: capitalize;">{{ $item->attribute_type }}</span> (Public)</td>
					        @else
					        <td><span style="text-transform: capitalize;">{{ $item->attribute_type }}</span></td>
					        @endif
					        <td>
					        	@php
					        		$listTerms = get_terms_by_attr_id($item->content_id);
					        	@endphp
					        	@if(count($listTerms) === 0)
					        		-
					        	@else
					        		@foreach($listTerms as $value) @php $text[] = $value->name; @endphp @endforeach
						        	{{ implode(', ', $text) }}
						        	@php $text = []; @endphp
					        	@endif
					        	<p><a href="{{ route('woocommerce.attributes.create.terms', $item->content_id) }}">Configure terms</a></p>
					        </td>
					      </tr>
					    @endforeach
					    </tbody>
					</table>
            	</div>
            </div>
        </div>
	</div>
    {!! Form::close() !!}
@stop
<style type="text/css">
	.row-actions{
		display: none;
		font-size: 13px;
	}
	.row-actions .delete a{
		color: #ff0000;
	}
	tr.alternate:hover .row-actions{
		display: block;
	}
</style>