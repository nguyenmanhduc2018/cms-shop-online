@extends('bases::layouts.master')
@section('content')
    {!! Form::open() !!}
    @php do_action(BASE_ACTION_CREATE_CONTENT_NOTIFICATION, WOOCOMMERCE_ATTRIBUTE_MODULE_SCREEN_NAME, request(), null) @endphp
    <div class="row">
        <div class="col-md-5">
            <div class="main-form">
                <div class="form-body">
	            	<div class="form-group @if ($errors->has('name')) has-error @endif">
	                    <label for="name" class="control-label required">{{ trans('bases::forms.name') }}</label>
	                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'The name is how it appears on your site.', 'data-counter' => 120]) !!}
	                    {!! Form::error('name', $errors) !!}
	                </div>
	                <div class="form-group">
	                    {!! Form::permalink('slug', old('slug'), null, route('categories.create.slug'), route('public.single.detail', config('cms.slug.pattern')), url('/')) !!}
	                    {!! Form::error('slug', $errors) !!}
	                </div>
	                <div class="form-group @if ($errors->has('description')) has-error @endif">
                        <label for="description" class="control-label required">{{ trans('blog::posts.form.description') }}</label>
                        {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'rows' => 4, 'id' => 'description', 'placeholder' => trans('blog::posts.form.description_placeholder'), 'data-counter' => 300]) !!}
                        {!! Form::error('description', $errors) !!}
                        <br>
                        <i>The description is not prominent by default; however, some themes may show it.</i>
                    </div>
	                <div class="form-group">
	                	<div class="toolbar">
	                        <button type="submit" name="submit" class="form-control button add_new_attribute btn btn-primary" value="save">Add New</button>
	                    </div>
	                </div>
            	</div>
        	</div>
    	</div>
    	<div class="col-md-7">
        	<div class="main-form">
            	<div class="table-responsive form-inline">
            		<div class="form-group pull-right">
            			<select class="form-control" name="action" id="bulk-action-selector-top">
							<option value="-1">Actions</option>
							<option value="delete">Delete</option>
						</select>
						<button type="button" class="btn btn-success" id="apply">Apply</button>
            		</div>
					<div class="clearfix"></div>
            		<table id="dataTableBuilderTerms" class="table table-striped table-bordered">
					    <thead>
					      <tr>
					      	<th width="10px" class="text-left no-sort">
					      		<div class="checkbox checkbox-primary">
			            			<div class="checker">
			            				<span>
			            					<input type="checkbox" class="groupCheckableTerms">
			            				</span>
			            			</div>
			            		</div>
					      	</th>
					      	<th></th>
					        <th>Name</th>
					        <th>Description</th>
					        <th>Slug</th>
					      </tr>
					    </thead>
					    <tbody>
					   	@foreach($listTerms as $item)
					    <tr class="alternate" style=" height: 55px;">
					    	<td width="10px" class="text-left no-sort">
					            <div class="checkbox checkbox-primary">
			            			<div class="checker">
			            				<span>
			            					<input type="checkbox" class="group-checkable" value="{{ $item->term_id }}" />
			            				</span>
			            			</div>
			            		</div>
					        </td>
					    	<td width="60px"></td>
					        <td width="25%">
					        	{!! anchor_link(route('woocommerce.attributes.edit', $item->term_id), $item->name) !!}
					        	<div class="clearfix"></div>
					        	<div class="row-actions">
					        		<span class="edit">
					        			<a href="{{ route('woocommerce.attributes.edit.terms', $item->term_id) }}">Edit</a> | 
					        		</span>
					        		<span class="delete">
					        			<a href="javascript:;" data-href="{{ route('woocommerce.attributes.delete.terms', $item->term_id) }}">Delete</a>
					        		</span>
					        	</div>
					        </td>
					        <td>{{ $item->description }}</td>
					        <td width="25%">{{ $item->slug }}</td>
					      </tr>
					    @endforeach
					    </tbody>
					</table>
            	</div>
            </div>
        </div>
	</div>
    {!! Form::close() !!}
@stop
<style type="text/css">
	.row-actions{
		display: none;
		font-size: 13px;
	}
	.row-actions .delete a{
		color: #ff0000;
	}
	tr.alternate:hover .row-actions{
		display: block;
	}
</style>
@section('javascript')
{!! HTML::style('vendor/core/packages/datatables/extensions/Buttons/css/buttons.bootstrap.min.css') !!}
{!! HTML::style('vendor/core/packages/datatables/extensions/ColReorder/css/colReorder.bootstrap.min.css') !!}
{!! HTML::style('vendor/core/packages/datatables/media/css/dataTables.bootstrap.min.css') !!}

{!! HTML::script('vendor/core/packages/datatables/media/js/jquery.dataTables.min.js') !!}
{!! HTML::script('vendor/core/packages/datatables/extensions/Buttons/js/dataTables.buttons.min.js') !!}
{!! HTML::script('vendor/core/packages/datatables/extensions/Buttons/js/dataTables.buttons.min.js') !!}
{!! HTML::script('vendor/core/packages/datatables/media/js/dataTables.bootstrap.min.js') !!}
<script type="text/javascript">
	$(document).ready(function() {

		$('#dataTableBuilderTerms').DataTable();

		var checkList = $('.checker span input[type="checkbox"].group-checkable');

		$('.groupCheckableTerms').click(function() {
			$(this).toggleClass('selector');
			if($(this).hasClass('selector')){
				$('.checker span').addClass('checked');
				checkList.each(function() {
					$(this).click();
				});
			}else{
				$('.checker span').removeClass('checked');
				checkList.each(function() {
					$(this).click();
				});
			}	
		});

		$('.group-checkable').change(function() {
			if($(this).is(':checked')){
				$(this).parent().addClass('checked');
			}else{
				$(this).parent().removeClass('checked');
			}
		});

		$('#apply').click(function() {
			var ListID = [];
			var select = $('#bulk-action-selector-top');
			if(select.find(":selected").val() == 'delete'){
				checkList.each(function() {
					if($(this).is(':checked')){
						ListID.push($(this).val());
					}
				});
				$.ajax({
					url: '{{ route('woocommerce.attributes.delete_permanently.many.terms') }}',
					type: 'POST',
					dataType: 'json',
					data: {data: ListID},
					success: function(e){
						
					}
				});
			}
		});

	});

</script>
@stop