<a class="btn btn-icon btn-sm btn-info restoreDialog tip" data-toggle="modal" data-section="{{ route('woocommerce.categories.restore', $item->id) }}" role="button" data-original-title="{{ trans('bases::tables.restore') }}" >
    <i class="fa fa-undo"></i>
</a>

@if (!$item->is_default)
    <a class="btn btn-icon btn-danger deletePermanentlyDialog tip" data-toggle="modal" data-section="{{ route('woocommerce.categories.delete_permanently', $item->id) }}" role="button" data-original-title="{{ trans('bases::tables.delete_permanently') }}" >
        <i class="fa fa-trash-o"></i>
    </a>
@endif