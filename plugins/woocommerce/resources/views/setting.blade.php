@extends('bases::layouts.master')
@section('content')
    {!! Form::open(['route' => ['woocommerce.setting']]) !!}
    <div class="tabbable-custom tabbable-tabdrop">
        <ul class="nav nav-tabs" id="settings-tab">
             <li class="active">
                <a data-toggle="tab" href="#General">General</a>
             </li>
        </ul>
        <div class="tab-content" style="overflow: inherit">
            <div class="tab-pane active" id="General">
                <div class="col-md-6">
                    <div class="form-group @if ($errors->has('currency')) has-error @endif">
                        <label for="currency" class="control-label">{{ trans('woocommerce::woocommerce.form.currency') }}</label>
                        {!! Form::select('currency', [
                            'AED' => 'United Arab Emirates dirham (د.إ)',
                            'AFN' => 'Afghan afghani (؋)',
                            'ALL' => 'Albanian lek (L)',
                            'AMD' => 'Armenian dram (AMD)',
                            'ANG' => 'Netherlands Antillean guilder (ƒ)',
                            'AOA' => 'Angolan kwanza (Kz)',
                            'ARS' => 'Argentine peso ($)',
                            'AUD' => 'Australian dollar ($)',
                            'AWG' => 'Aruban florin (Afl.)',
                            'AZN' => 'Azerbaijani manat (AZN)',
                            'BAM' => 'Bosnia and Herzegovina convertible mark (KM)',
                            'BBD' => 'Barbadian dollar ($)',
                            'BDT' => 'Bangladeshi taka (৳)',
                            'BGN' => 'Bulgarian lev (лв.)',
                            'BHD' => 'Bahraini dinar (.د.ب)',
                            'BIF' => 'Burundian franc (Fr)',
                            'BMD' => 'Bermudian dollar ($)',
                            'BND' => 'Brunei dollar ($)',
                            'BOB' => 'Bolivian boliviano (Bs.)',
                            'BRL' => 'Brazilian real (R$)',
                            'BSD' => 'Bahamian dollar ($)',
                            'BTC' => 'Bitcoin (฿)',
                            'BTN' => 'Bhutanese ngultrum (Nu.)',
                            'BWP' => 'Botswana pula (P)',
                            'BYR' => 'Belarusian ruble (Br)',
                            'BZD' => 'Belize dollar ($)',
                            'CAD' => 'Canadian dollar ($)',
                            'CDF' => 'Congolese franc (Fr)',
                            'CHF' => 'Swiss franc (CHF)',
                            'CLP' => 'Chilean peso ($)',
                            'CNY' => 'Chinese yuan (¥)',
                            'COP' => 'Colombian peso ($)',
                            'CRC' => 'Costa Rican colón (₡)',
                            'CUC' => 'Cuban convertible peso ($)',
                            'CUP' => 'Cuban peso ($)',
                            'CVE' => 'Cape Verdean escudo ($)',
                            'CZK' => 'Czech koruna (Kč)',
                            'DJF' => 'Djiboutian franc (Fr)',
                            'DKK' => 'Danish krone (DKK)',
                            'DOP' => 'Dominican peso (RD$)',
                            'DZD' => 'Algerian dinar (د.ج)',
                            'EGP' => 'Egyptian pound (EGP)',
                            'ERN' => 'Eritrean nakfa (Nfk)',
                            'ETB' => 'Ethiopian birr (Br)',
                            'EUR' => 'Euro (€)',
                            'FJD' => 'Fijian dollar ($)',
                            'FKP' => 'Falkland Islands pound (£)',
                            'GBP' => 'Pound sterling (£)',
                            'GEL' => 'Georgian lari (ლ)',
                            'GGP' => 'Guernsey pound (£)',
                            'GHS' => 'Ghana cedi (₵)',
                            'GIP' => 'Gibraltar pound (£)',
                            'GMD' => 'Gambian dalasi (D)',
                            'GNF' => 'Guinean franc (Fr)',
                            'GTQ' => 'Guatemalan quetzal (Q)',
                            'GYD' => 'Guyanese dollar ($)',
                            'HKD' => 'Hong Kong dollar ($)',
                            'HNL' => 'Honduran lempira (L)',
                            'HRK' => 'Croatian kuna (Kn)',
                            'HTG' => 'Haitian gourde (G)',
                            'HUF' => 'Hungarian forint (Ft)',
                            'IDR' => 'Indonesian rupiah (Rp)',
                            'ILS' => 'Israeli new shekel (₪)',
                            'IMP' => 'Manx pound (£)',
                            'INR' => 'Indian rupee (₹)',
                            'IQD' => 'Iraqi dinar (ع.د)',
                            'IRR' => 'Iranian rial (﷼)',
                            'IRT' => 'Iranian toman (تومان)',
                            'ISK' => 'Icelandic króna (kr.)',
                            'JEP' => 'Jersey pound (£)',
                            'JMD' => 'Jamaican dollar ($)',
                            'JOD' => 'Jordanian dinar (د.ا)',
                            'JPY' => 'Japanese yen (¥)',
                            'KES' => 'Kenyan shilling (KSh)',
                            'KGS' => 'Kyrgyzstani som (сом)',
                            'KHR' => 'Cambodian riel (៛)',
                            'KMF' => 'Comorian franc (Fr)',
                            'KPW' => 'North Korean won (₩)',
                            'KRW' => 'South Korean won (₩)',
                            'KWD' => 'Kuwaiti dinar (د.ك)',
                            'KYD' => 'Cayman Islands dollar ($)',
                            'KZT' => 'Kazakhstani tenge (KZT)',
                            'LAK' => 'Lao kip (₭)',
                            'LBP' => 'Lebanese pound (ل.ل)',
                            'LKR' => 'Sri Lankan rupee (රු)',
                            'LRD' => 'Liberian dollar ($)',
                            'LSL' => 'Lesotho loti (L)',
                            'LYD' => 'Libyan dinar (ل.د)',
                            'MAD' => 'Moroccan dirham (د.م.)',
                            'MDL' => 'Moldovan leu (MDL)',
                            'MGA' => 'Malagasy ariary (Ar)',
                            'MKD' => 'Macedonian denar (ден)',
                            'MMK' => 'Burmese kyat (Ks)',
                            'MNT' => 'Mongolian tögrög (₮)',
                            'MOP' => 'Macanese pataca (P)',
                            'MRO' => 'Mauritanian ouguiya (UM)',
                            'MUR' => 'Mauritian rupee (₨)',
                            'MVR' => 'Maldivian rufiyaa (.ރ)',
                            'MWK' => 'Malawian kwacha (MK)',
                            'MXN' => 'Mexican peso ($)',
                            'MYR' => 'Malaysian ringgit (RM)',
                            'MZN' => 'Mozambican metical (MT)',
                            'NAD' => 'Namibian dollar ($)',
                            'NGN' => 'Nigerian naira (₦)',
                            'NIO' => 'Nicaraguan córdoba (C$)',
                            'NOK' => 'Norwegian krone (kr)',
                            'NPR' => 'Nepalese rupee (₨)',
                            'NZD' => 'New Zealand dollar ($)',
                            'OMR' => 'Omani rial (ر.ع.)',
                            'PAB' => 'Panamanian balboa (B/.)',
                            'PEN' => 'Peruvian nuevo sol (S/.)',
                            'PGK' => 'Papua New Guinean kina (K)',
                            'PHP' => 'Philippine peso (₱)',
                            'PKR' => 'Pakistani rupee (₨)',
                            'PLN' => 'Polish złoty (zł)',
                            'PRB' => 'Transnistrian ruble (р.)',
                            'PYG' => 'Paraguayan guaraní (₲)',
                            'QAR' => 'Qatari riyal (ر.ق)',
                            'RON' => 'Romanian leu (lei)',
                            'RSD' => 'Serbian dinar (дин.)',
                            'RUB' => 'Russian ruble (₽)',
                            'RWF' => 'Rwandan franc (Fr)',
                            'SAR' => 'Saudi riyal (ر.س)',
                            'SBD' => 'Solomon Islands dollar ($)',
                            'SCR' => 'Seychellois rupee (₨)',
                            'SDG' => 'Sudanese pound (ج.س.)',
                            'SEK' => 'Swedish krona (kr)',
                            'SGD' => 'Singapore dollar ($)',
                            'SHP' => 'Saint Helena pound (£)',
                            'SLL' => 'Sierra Leonean leone (Le)',
                            'SOS' => 'Somali shilling (Sh)',
                            'SRD' => 'Surinamese dollar ($)',
                            'SSP' => 'South Sudanese pound (£)',
                            'STD' => 'São Tomé and Príncipe dobra (Db)',
                            'SYP' => 'Syrian pound (ل.س)',
                            'SZL' => 'Swazi lilangeni (L)',
                            'THB' => 'Thai baht (฿)',
                            'TJS' => 'Tajikistani somoni (ЅМ)',
                            'TMT' => 'Turkmenistan manat (m)',
                            'TND' => 'Tunisian dinar (د.ت)',
                            'TOP' => 'Tongan paʻanga (T$)',
                            'TRY' => 'Turkish lira (₺)',
                            'TTD' => 'Trinidad and Tobago dollar ($)',
                            'TWD' => 'New Taiwan dollar (NT$)',
                            'TZS' => 'Tanzanian shilling (Sh)',
                            'UAH' => 'Ukrainian hryvnia (₴)',
                            'UGX' => 'Ugandan shilling (UGX)',
                            'USD' => 'United States dollar ($)',
                            'UYU' => 'Uruguayan peso ($)',
                            'UZS' => 'Uzbekistani som (UZS)',
                            'VEF' => 'Venezuelan bolívar (Bs F)',
                            'VND' => 'Vietnamese đồng (₫)',
                            'VUV' => 'Vanuatu vatu (Vt)',
                            'WST' => 'Samoan tālā (T)',
                            'XAF' => 'Central African CFA franc (CFA)',
                            'XCD' => 'East Caribbean dollar ($)',
                            'XOF' => 'West African CFA franc (CFA)',
                            'XPF' => 'CFP franc (Fr)',
                            'YER' => 'Yemeni rial (﷼)',
                            'ZAR' => 'South African rand (R)',
                            'ZMW' => 'Zambian kwacha (ZK)',
                            ], @$getCur->_currency,  ['class' => 'form-control', 'id' => 'currency']) 
                        !!}
                        {!! Form::error('currency', $errors) !!}
                    </div>
                    <div class="form-group @if ($errors->has('price_thousand_sep')) has-error @endif">
                        <label for="price_thousand_sep" class="control-label required">{{ trans('woocommerce::woocommerce.form.price_thousand_sep') }}</label>
                        {!! Form::text('price_thousand_sep', @$getCur->_price_thousand_sep, ['class' => 'form-control', 'id' => 'price_thousand_sep', 'data-counter' => 1]) !!}
                        {!! Form::error('price_thousand_sep', $errors) !!}
                    </div>
                </div>
                <div class="col-md-6">
                        <div class="form-group @if ($errors->has('currency_pos')) has-error @endif">
                            <label for="currency_pos" class="control-label">{{ trans('woocommerce::woocommerce.form.currency_pos') }}</label>
                            {!! Form::select('currency_pos', [
                                'left' => 'Left (₫99.99)',
                                'right' => 'Right (99.99₫)',
                                'left_space' => 'Left with space (₫ 99.99)',
                                'right_space' => 'Right with space (99.99 ₫)',
                                ], @$getCur->_currency_pos,  ['class' => 'form-control', 'id' => 'currency_pos']) 
                            !!}
                            {!! Form::error('currency_pos', $errors) !!}
                        </div>
                    <div class="clearfix"></div>
                </div>
            <div class="clearfix"></div>
            <div class="text-center">
                <button type="submit" name="submit" value="save" class="btn btn-info">
                    <i class="fa fa-save"></i> {{ trans('bases::forms.save') }}
                </button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop
