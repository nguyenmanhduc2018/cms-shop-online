@extends('bases::layouts.master')
@section('content')
    {!! Form::open(['route' => ['woocommerce.edit', $woocommerce->id]]) !!}
        @php do_action(BASE_ACTION_EDIT_CONTENT_NOTIFICATION, WOOCOMMERCE_MODULE_SCREEN_NAME, request(), $woocommerce) @endphp
        <div class="row">
            <div class="col-md-9">
                <div class="tabbable-custom tabbable-tabdrop">
                     <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_detail" data-toggle="tab">{{ trans('bases::tabs.detail') }}</a>
                        </li>
                    </ul>
                    <div class="tab-content postId" tab-post-id="{{ $woocommerce->id }}">
                        <div class="tab-pane active" id="tab_detail">
                            <div class="form-body">
                                <div class="form-group @if ($errors->has('name')) has-error @endif">
                                    <label for="name" class="control-label required">{{ trans('bases::forms.name') }}</label>
                                    {!! Form::text('name', $woocommerce->name, ['class' => 'form-control', 'id' => 'name', 'placeholder' => trans('bases::forms.name_placeholder'), 'data-counter' => 120]) !!}
                                    {!! Form::error('name', $errors) !!}
                                </div>
                                <div class="form-group @if ($errors->has('slug')) has-error @endif">
                                    {!! Form::permalink('slug', $woocommerce->slug, $woocommerce->id, route('posts.create.slug'), route('public.single.detail', config('cms.slug.pattern')), url('/')) !!}
                                    {!! Form::error('slug', $errors) !!}
                                </div>
                                <div class="form-group @if ($errors->has('description')) has-error @endif">
                                    <label for="description" class="control-label required">{{ trans('blog::posts.form.description') }}</label>
                                    {!! Form::textarea('description', $woocommerce->description, ['class' => 'form-control', 'rows' => 4, 'id' => 'description', 'placeholder' => trans('blog::posts.form.description_placeholder'), 'data-counter' => 300]) !!}
                                    {!! Form::error('description', $errors) !!}
                                </div>
                                <div class="form-group @if ($errors->has('content')) has-error @endif">
                                    <label class="control-label required">{{ trans('blog::posts.form.content') }}</label>
                                    <a class="btn_gallery" data-mode="attach" data-result="content" data-action="image_post"
                                       data-backdrop="static" data-keyboard="false" data-toggle="modal"
                                       data-target=".media_modal">{{ trans('media::media.add') }}</a>
                                    {!! render_editor('content', $woocommerce->content, true) !!}
                                    {!! Form::error('content', $errors) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget meta-boxes">
                    <div class="widget-title">
                        <h4><span>Product Data</span></h4>
                    </div>
                    <div class="tabbable-custom tabbable-tabdrop">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#general_product_data" data-toggle="tab"><i class="fa fa-wrench"></i> General</a>
                            </li>
                            <li>
                                <a href="#inventory_product_data" data-toggle="tab"><i class="fa fa-ticket"></i> Inventory</a>
                            </li>
                            <li>
                                <a href="#product_attributes" data-toggle="tab"><i class="fa fa-list-alt"></i> Attributes</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="general_product_data">
                                <div class="form-body">
                                    @php
                                        $cur = get_unitcurrency(get_currency());
                                    @endphp
                                    @if($cur !== 'N/A')
                                    <div class="form-group @if ($errors->has('regular_price')) has-error @endif">
                                        <label for="regular_price" class="control-label">{{ trans('woocommerce::woocommerce.form.regular_price') }} ({{ $cur }})</label>
                                        {!! Form::text('regular_price', @$dataPostMeta->regular_price, ['class' => 'form-control', 'id' => 'regular_price']) !!}
                                        {!! Form::error('regular_price', $errors) !!}
                                    </div>
                                    <div class="form-group @if ($errors->has('sale_price')) has-error @endif">
                                        <label for="regular_price" class="control-label">{{ trans('woocommerce::woocommerce.form.sale_price') }} ({{ $cur }})</label>
                                        {!! Form::text('sale_price', @$dataPostMeta->sale_price, ['class' => 'form-control', 'id' => 'sale_price']) !!}
                                        {!! Form::error('sale_price', $errors) !!}
                                        <span class="description">
                                            <a href="javascript:;" class="sale_schedule" style="display: inline;">Schedule</a>
                                            </span>
                                    </div>
                                    <div class="form-group sale_price_dates_fields" style="display: none;">
                                        <label for="_sale_price_dates_from">{{ trans('woocommerce::woocommerce.form.sale_price_date') }}</label>
                                        <div class="input-group input-daterange">
                                        {!! Form::text('_sale_price_dates_from', @$dataPostMeta->_sale_price_dates_from, ['class' => 'form-control datepicker_from', 'id' => '_sale_price_dates_from', 'placeholder' => 'From… YYYY-MM-DD', 'maxlength' => '10', 'pattern' => '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])']) !!}
                                        <div class="input-group-addon">to</div>
                                         {!! Form::text('_sale_price_dates_to', @$dataPostMeta->_sale_price_dates_to, ['class' => 'form-control datepicker_to', 'id' => '_sale_price_dates_to', 'placeholder' => 'To… YYYY-MM-DD', 'maxlength' => '10', 'pattern' => '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])']) !!}
                                        </div>
                                        <a href="javascript:;" class="description cancel_sale_schedule">Cancel</a>
                                    </div>
                                    @else
                                        <div class="alert alert-warning">
                                          Attention: Current settings to use the price. <a href="{{ route('woocommerce.setting') }}">Settings</a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="tab-pane" id="inventory_product_data">
                                <div class="form-body">
                                    <div class="form-group @if ($errors->has('_sku')) has-error @endif">
                                        <label for="_sku" class="control-label">{{ trans('woocommerce::woocommerce.form._sku') }}</label>
                                        {!! Form::text('_sku', @$dataPostMeta->_sku, ['class' => 'form-control', 'id' => '_sku']) !!}
                                        {!! Form::error('_sku', $errors) !!}
                                    </div>
                                    <div class="form-group @if ($errors->has('_stock_status')) has-error @endif">
                                        <label for="_stock_status" class="control-label">{{ trans('woocommerce::woocommerce.form._stock_status') }}</label>
                                        {!! Form::select('_stock_status', ['instock' => 'In stock', 'outofstock' => 'Out of stock'], @$dataPostMeta->_stock_status,  ['class' => 'form-control', 'id' => '_stock_status']) !!}
                                        {!! Form::error('_stock_status', $errors) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="product_attributes">
                                <div class="form-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="attribute_taxonomy" class="attribute_taxonomy form-control">
                                                <option value="">Custom product attribute</option>
                                                @php
                                                    $i = -1;
                                                @endphp
                                                 @foreach ($listAttribute as $value)
                                                    @php
                                                        ++$i;
                                                    @endphp
                                                    @if(!empty($getPostAttrMeta))
                                                        @if( (int)$getPostAttrMeta->a[$i] === $value->attribute_id)
                                                        <option value="{{ $value->attribute_name }}" data-id="{{ $value->attribute_id }}" disabled="disabled">{{ $value->attribute_label }}</option>
                                                        @else
                                                        <option value="{{ $value->attribute_name }}" data-id="{{ $value->attribute_id }}">{{ $value->attribute_label }}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{ $value->attribute_name }}" data-id="{{ $value->attribute_id }}">{{ $value->attribute_label }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="pull-left">
                                            <button type="button" class="form-control button add_attribute">Add</button>
                                        </div>
                                        <div class="pull-right">
                                            <span class="expand-close">
                                                <a href="javascript:;" class="expand_all">Expand</a> / <a href="javascript:;" class="close_all">Close</a>
                                            </span>
                                        </div>  
                                    </div>
                                    <div class="col-md-12 loadAttribute">
                                        @if(!empty($getPostAttrMeta))
                                         {!! apply_filters(WOO_FILTER_LOAD_ATTRIBUTE_PAGE, $woocommerce->id) !!}
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <div class="toolbar">
                                            <button type="button" class="form-control button save_attributes btn btn-primary">Save attributes</button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @php do_action(BASE_ACTION_META_BOXES, POST_MODULE_SCREEN_NAME, 'advanced', $woocommerce) @endphp
            </div>
            <div class="col-md-3 right-sidebar">
                @include('bases::elements.form-actions')
                @php do_action(BASE_ACTION_META_BOXES, POST_MODULE_SCREEN_NAME, 'top', $woocommerce) @endphp
                @include('bases::elements.forms.status', ['selected' => $woocommerce->status])

                <div class="widget meta-boxes @if ($errors->has('featured')) has-error @endif">
                    <div class="widget-title">
                        <h4><span class="required">{{ trans('blog::posts.form.text-color') }}</span></h4>
                    </div>
                    <div class="widget-body">
                        {!! Form::onOff('featured', old('featured', null)) !!}
                        <label for="featured">{{ trans('blog::posts.form.is-light-color') }}</label>
                        {!! Form::error('featured', $errors) !!}
                    </div>
                </div>

                @include('blog::categories.partials.categories-multi', [
                    'name' => 'categories[]',
                    'title' => trans('blog::posts.form.categories'),
                    'value' => $selected_categories,
                    'categories' => $categories,
                    'object' => $woocommerce
                ])

                <div class="widget meta-boxes">
                    <div class="widget-title">
                        <h4><span class="required">{{ trans('bases::forms.image') }}</span></h4>
                    </div>
                    <div class="widget-body">
                        {!! Form::mediaImage('image', $woocommerce->image) !!}
                        {!! Form::error('image', $errors) !!}
                    </div>
                </div>

                <div class="widget meta-boxes">
                    <div class="widget-title">
                        <h4><span>{{ trans('blog::posts.form.tags') }}</span></h4>
                    </div>
                    <div class="widget-body">
                        <div class="form-group @if ($errors->has('tag')) has-error @endif">
                            {!! Form::text('tag', $tags, ['class' => 'form-control', 'id' => 'tags', 'data-role' => 'tagsinput', 'placeholder' => trans('blog::posts.form.tags_placeholder')]) !!}
                            {!! Form::error('tag', $errors) !!}
                        </div>
                        <div data-tag-route="{{ route('tags.all') }}"></div>
                    </div>
                </div>

                @php do_action(BASE_ACTION_META_BOXES, POST_MODULE_SCREEN_NAME, 'side', $woocommerce) @endphp
            </div>
        </div>
    {!! Form::close() !!}
    {!! apply_filters(WOO_FILTER_SCRIPT_CONTENT_PAGE, null) !!}
@stop
