<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWoocommerceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('woocommerce_order_items', function (Blueprint $table) {
            $table->increments('order_item_id');
            $table->text('order_item_name');
            $table->string('order_item_type');
            $table->integer('order_id');
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('woocommerce_order_item_meta', function (Blueprint $table) {
            $table->increments('meta_id');
            $table->integer('order_item_id');
            $table->string('meta_key')->nullable();
            $table->longText('meta_value')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('woocommerce_attribute_taxonomies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attribute_name');
            $table->string('attribute_label')->nullable();
            $table->string('attribute_type');
            $table->integer('attribute_public')->default(0);
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('woocommerce_terms', function (Blueprint $table) {
            $table->increments('term_id');
            $table->string('name');
            $table->string('slug');
            $table->string('description', 400)->nullable();
            $table->integer('attribute_id');
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('woocommerce_term_meta', function (Blueprint $table) {
            $table->increments('meta_id');
            $table->integer('term_id');
            $table->string('meta_key')->nullable();
            $table->longText('meta_value')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('woocommerce_payment', function (Blueprint $table) {
            $table->increments('pay_id');
            $table->integer('user_id')->unsigned()->index()->references('id')->on('users');
            $table->integer('order_id');
            $table->string('pay_type')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('woocommerce_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('meta_key')->nullable();
            $table->longText('meta_value')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('post_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id');
            $table->string('meta_key')->nullable();
            $table->longText('meta_value')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content');
            $table->integer('user_id')->unsigned()->index()->references('id')->on('users');
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('user_role', function (Blueprint $table) {
            $table->increments('user_id');
            $table->integer('role_id');
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('permission', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 120);
            $table->string('name', 120);
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('role_permission', function (Blueprint $table) {
            $table->increments('role_id');
            $table->integer('permission_id');
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        // Add
        Schema::table('posts', function (Blueprint $table) {
            $table->string('post_type', 10)->nullable();
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->string('cate_type', 10)->nullable();
            $table->string('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('woocommerce_order_items');
        //Schema::dropIfExists('woocommerce_order_item_meta');
        //Schema::dropIfExists('woocommerce_attribute_taxonomies');
        //Schema::dropIfExists('woocommerce_terms');
        //Schema::dropIfExists('woocommerce_term_meta');
        //Schema::dropIfExists('woocommerce_payment');
        //Schema::dropIfExists('post_meta');
        //Schema::dropIfExists('comments');
        //Schema::dropIfExists('user_role');
        //Schema::dropIfExists('permission');
        //Schema::dropIfExists('role_permission');
        //Schema::table('posts', function (Blueprint $table) {
        //    $table->dropColumn('post_type');
        //});
    }
}
