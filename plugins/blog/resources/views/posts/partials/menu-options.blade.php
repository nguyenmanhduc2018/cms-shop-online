@if (!empty($posts))
    <div class="widget panel">
        <div class="widget-heading">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapsePosts">
                <h4 class="widget-title">
                    <i class="box_img_sale"></i>
                    <span>{{ trans('blog::posts.menu') }}</span>
                    <i class="fa fa-angle-down narrow-icon"></i>
                </h4>
            </a>
        </div>
        <div id="collapsePosts" class="panel-collapse collapse">
            <div class="widget-body">
                <div class="box-links-for-menu">
                    <div class="the-box">
                    	<div class="list-item">
                    		<ul class="hidden" >
                		        <li class="li_get_post_menu">
                		            {!! Form::checkbox('menu_id', null, null, ['class' => 'styled hidden', 'id' => '']) !!}
                		            <label for="menu_id_post" class="menu_id_post" data-title="" data-related-id=""  data-type="{{ $posts->getTable() }}"></label>
                		        </li>
                    		</ul>
                    		
                    		{!! Form::select('menu_id_select', array(), null, [
                            'class' => 'form-control get-data-post-ajax',
                            'data-href' => route('posts.get.post.ajax'),
                            'data-placeholder' => trans('blog::posts.form.select-posts')
                            ]) !!}
                    	</div>
                        
                        <div class="text-right">
                            <div class="btn-group btn-group-devided">
                                <a href="#" class="btn-add-to-menu btn btn-primary">
                                    <span class="text"><i class="fa fa-plus"></i> {{ trans('menu::menu.add_to_menu') }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@push('footer')
    <script type="text/javascript">
    	var getDataPostAjax = "get-data-post-ajax";
    	$('.' + getDataPostAjax).select2({
    	    placeholder: $('.' + getDataPostAjax).data('placeholder'),
    	    minimumInputLength: 1,
    	    width: '100%',
    	    ajax: {
    	        url: $('.' + getDataPostAjax).data('href'),
    	        dataType: 'json',
    	        type: 'post',
    	        delay: 250,
    	        data: function(params){
    	            return {
    	                q: $.trim(params.term)
    	            };
    	        },
    	        processResults: function(data){
    	        	$('.li_get_post_menu').addClass('active');
    	            return {
    	                results: data
    	            };

    	        },
    	        cache: true
    	    },
    	    templateResult: formatState,
    	    templateSelection: formatStateSelection
    	});
    	function formatState (state) {
    	  if (!state.id) { return state.text; }
    	  if(state.image){
    	    var $state = $('<span><img src="'+ state.image +'" class="img-flag" style="height: 35px;" /> ' + state.text + '</span>'
    	    );
    	  }
    	  return $state;
    	};
    	function formatStateSelection (state) {
    	  if (!state.id) { return state.text; }
    	  if(state.image){
    	    var $state = $('<span><img src="'+ state.image +'" class="img-flag" style="height: 22px;" /> ' + state.text + '</span>'
    	    );
    	  }
    	  return $state;
    	};
    	$('.' + getDataPostAjax).on("select2:select", function(event){
    		var label = 'menu_id_post';
    		
    		$('.'+label).attr('data-related-id', $(this).val());
            console.log($(this).text());
    		$('.'+label).attr('data-title', $.trim($("select.get-data-post-ajax").children("option:selected").text()) );
    	});
    </script>
@endpush

@endif