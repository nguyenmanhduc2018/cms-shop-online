<?php

namespace Botble\Language\Http\Requests;

use Botble\Support\Http\Requests\Request;

class LanguageRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @author Sang Nguyen
     */
    public function rules()
    {
        return [
            'name' => 'required|max:30|min:2',
            'code' => 'required|max:10|min:2',
            'locale' => 'required|max:10|min:2',
            'flag' => 'required',
            'is_rtl' => 'required',
            'order' => 'required|numeric',
        ];
    }
}
